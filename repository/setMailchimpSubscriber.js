import axios from 'axios'

/**
 * Add new subscriber 
 * Mailchimp API is not accessible from frontend (due to CORS), so we go via WordPress and do it in the backend 
 */
export default async function setMailchimpSubscriber(email, tags=[], coupon='') {

	let response
	try {
		response = await fetch(process.env.WORDPRESS_API + 'subscribe', {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json',
			},
			body: JSON.stringify({
				'email' : email,
				'tags' : tags,
				'coupon' : coupon
			})
		})
	} catch(err) {
		console.log(err)
		throw { success: false, errorMessage: "Failed to connect to server." }
	}

	if(response.status === 200) {
		return response.json()
	} else {
		const data = await response.json()
		const errorMessage = data.errorMessage || data.message // if url invalid return error message from default wordpress
		throw { success: false, errorMessage }
	}
}

export async function updateMailchimpSubscriber(email, tags=[], coupon='') {

	const result = { success: false, message: null }

	try {

		await axios.post(`${process.env.WORDPRESS_API}update`, { email, tags, coupon })
		result.success = true

	} catch(err) {

		result.success = false
		if(err.response) {
			const data = err.response.data
			result.message = data.message
		}
		return result

	}

	return result
	
}

export const checkMemberNotExists = async email => {

	const result = { success: false, message: null }

	try {

		await axios.get(`${process.env.WORDPRESS_API}hasmember`, { params: { email } })
		result.success = true

	} catch(err) {

		result.success = false
		if(err.response) {
			const data = err.response.data
			result.message = data.message
		}
		return result

	}

	return result

}

export const checkMemberNotUsedCoupon = async email => {

	const result = { success: false, message: null }

	try {

		await axios.get(`${process.env.WORDPRESS_API}member_has_coupon`, { params: { email } })
		result.success = true

	} catch(err) {

		result.success = false
		if(err.response) {
			const data = err.response.data
			result.message = data.message
		}
		return result

	}

	return result

}

export const generateMailchimpCoupon = async email => {

	const result = { success: false, message: null, coupon: null }

	try {

		const response = await axios.get(`${process.env.WORDPRESS_API}generate_mailchimp_coupon`, { params: { email } })
		if(response.data && response.data.length === 1) {
			result.success = true
			result.coupon = response.data[0]
		} else {
			throw 'Could not generate coupon'
		}
		
	} catch(err) {

		result.success = false
		if(err.response) {
			const data = err.response.data
			result.message = data.message
		} else {
			result.message = err
		}
		return result

	}

	return result

}