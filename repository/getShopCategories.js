import { shopClient } from '../api/public'

/**
 * Get all shop categories for the current store 
 */
 export default async function getShopCategories(audience=false) {
	const query = `
	{
        categories( filters: { parent_id: { eq: "${process.env.SHOP_CATEGORY_ID}" } } ) {
            items {
                url_key
                children {
                    url_key
                }
            }
        }
    }
	`

	try {
		const response = await shopClient.request(query)
		return await mapCategories(response.categories.items, audience)
	} catch(e) {
		console.error('Categories could not be retrieved')
	}
}

export async function mapCategories(categories, audience) {
	return categories.map(cat => ({ 
		slug: cat.url_key,
		subcategories: !!cat.children ? cat.children.map(child => ({
			slug: child.url_key,
		})) : false
	}))
}
