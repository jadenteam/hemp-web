import { shopClient } from '../api/public'
import getManufacturers from './getManufacturers'
import getAudiences from './getAudiences'
import { getAudienceLabel, getManufacturerLabel, mapUpsellProductsAsColours, mapPrices, mapOptions } from './productHelpers'

/**
 * Get a single grouped product
 */
export default async function getGroupedProduct(slug) {
    const query = `
    {
		products(filter: { url_key: { eq: "${slug}" } }) {
			items { 
				__typename 
				id
				name
				sku
				description {
					html
				}
				... on GroupedProduct {
					items {
						qty
						position
						product {
							id
							sku
							name
							url_key
							short_description {
								html
							}
							manufacturer 
							audience
							featured_product
							categories {
								id
								name
								level
							}
							price_range {
								minimum_price {
									regular_price {
										value
										currency
									}
									final_price {
										value
										currency
									}
									discount {
										amount_off
										percent_off
									}
								}
								maximum_price {
									regular_price {
										value
										currency
									}
									final_price {
										value
										currency
									}
									discount {
										amount_off
										percent_off
									}
								}
							}
							stock_status
							only_x_left_in_stock
							image {
								url
								label
							}
							small_image {
								url
								label
							}
							... on ConfigurableProduct {
								configurable_options {
									id
									attribute_code
									label
									attribute_id_v2
									values {
										value_index
										label
									}
								}
								variants {
									attributes {
										uid
										label
										code
										value_index
									}
								}
							}
						}
					}
				}
			}
		}
	}
    `
	const response = await shopClient.request(query)
    return await !!response.products.items.length ? mapGroupedProduct(response.products.items[0]) : false
}

export async function mapGroupedProduct(group) {
    const manufacturers = await getManufacturers()
	const audiences = await getAudiences()

	return {
		...group,
		description: group.description.html,
		
		products: group.items.map(p => ({
			...p.product,
			inStock: p.product.stock_status === 'IN_STOCK',
 
			// replace manufacturer ID with object containing ID and label
			manufacturer: getManufacturerLabel(p.product.manufacturer, manufacturers),

			// replace audience ID with label object
			audience: getAudienceLabel(p.product.audience, audiences),

			// map price_range object into an easier to use object 
			price: mapPrices(p.product.price_range, p.product.__typename),

			// get uid's of each option variation
			options: mapOptions(p.product.configurable_options, p.product.variants)
		}))
	}
}