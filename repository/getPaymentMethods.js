import { shopClient } from '../api/public'

/**
 * Get the available payment method options 
 */
export default async function getPaymentMethods(id) {
    const query = `
    {
        cart(cart_id: "${id}") {
            available_payment_methods {
                code
                title
            }
        }
    }
    `
    const response = await shopClient.request(query)
    return response.data.cart.available_payment_methods
}