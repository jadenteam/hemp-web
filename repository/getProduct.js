import stripHtml from 'string-strip-html'

import { shopClient } from '../api/public'
import getManufacturers from './getManufacturers'
import getAudiences from './getAudiences'
import getColours from './getColours'
import { getAudienceLabel, getManufacturerLabel, mapUpsellProductsAsColours, mapPrices, mapOptions, mapImage } from './productHelpers'

/**
 * Get the products for a specific shop  
 */
export default async function getProduct(slug) {
    const query = `
		{
			products(filter: { url_key: { eq: "${slug}" } }) {
				items {
					__typename
					id
					sku
					uid
					name
					url_key
					short_description {
						html
					}
					description {
						html
					}
					manufacturer 
					audience
					colour
					categories {
						id
						name
						level
					}
					upsell_products {
						url_key
						colour 
					}
					related_products {
						id
						sku
						name
						url_key
						manufacturer 
						categories {
							id
							name
							level
						}
						price_range {
							minimum_price {
								regular_price {
									value
									currency
								}
								final_price {
									value
									currency
								}
								discount {
									amount_off
									percent_off
								}
							}
							maximum_price {
								regular_price {
									value
									currency
								}
								final_price {
									value
									currency
								}
								discount {
									amount_off
									percent_off
								}
							}
						}
						stock_status
						only_x_left_in_stock
						image {
							url
							label
						}
						small_image {
							url
							label
						}
					}
					price_range {
						minimum_price {
							regular_price {
								value
								currency
							}
							final_price {
								value
								currency
							}
							discount {
								amount_off
								percent_off
							}
						}
						maximum_price {
							regular_price {
								value
								currency
							}
							final_price {
								value
								currency
							}
							discount {
								amount_off
								percent_off
							}
						}
					}
					stock_status
					only_x_left_in_stock
					image {
						url
						label
					}
					small_image {
						url
						label
					}
					media_gallery {
						url 
						label
						position
					}
					reviews {
						items {
							average_rating
							created_at
							nickname
							summary
							text
						}
					}
					rating_summary
					review_count
					... on ConfigurableProduct {
						configurable_options {
							id
							attribute_code
							label
							attribute_id_v2
							values {
								value_index
								label
							}
						}
						variants {
							product {
								id
								stock_status
								only_x_left_in_stock
								image {
									url
									label
								}
								small_image {
									url
									label
								}
								media_gallery {
									url 
									label
									position
								}
							}
							attributes {
								uid
								label
								code
								value_index
							}
						}
					}
				}
			}
		}
    `

	let res;
	try {
		res = await shopClient.request(query);
	} catch(err) {
		throw "Failed to get product"
	}

	if(res.products.items.length > 0) {
		// Product items is an array of products, so find exact match on url_key and use to mapProduct
		const productToMap = res.products.items.find(product => product.url_key === slug)
		return await mapProduct(productToMap)
	} else {
		return false
	}

}

export async function mapProduct(item) {
    const manufacturers = await getManufacturers()
	const audiences = await getAudiences()
	const colours = await getColours()

	const configurableMediaGallery = item.variants ? item.variants.reduce((acc, variant) => acc.concat(variant.product.media_gallery), []) : []

	return {
		...item,
		inStock: item.stock_status === 'IN_STOCK',

		// strip html tags and convert from redundant object to string
		short_description: stripHtml(item.short_description.html).result,
		description: item.description.html,

		// replace manufacturer ID with object containing ID and label
		manufacturer: getManufacturerLabel(item.manufacturer, manufacturers),

		// replace audience ID with label object
		audience: getAudienceLabel(item.audience, audiences),

		// map upsell products to be used as colour variants 
		colours: mapUpsellProductsAsColours(item, colours),

		related_products: item.related_products.map(related => ({
			...related,
			inStock: related.stock_status === 'IN_STOCK',
			manufacturer: getManufacturerLabel(related.manufacturer, manufacturers),
			price: mapPrices(related.price_range, related.__typename),
		})),

		// map price_range object into an easier to use object 
		price: mapPrices(item.price_range, item.__typename),
		
		// map main image
		image: mapImage(item.image),

		// merge images and configurable gallery, map, then sort media gallery photos by position
		media_gallery: [...item.media_gallery, ...configurableMediaGallery]
			.map(obj => mapImage(obj))
			.sort((a, b) => (a.position > b.position) ? 1 : -1),
		// media_gallery: item.media_gallery.map(obj => mapImage(obj)).sort((a, b) => (a.position > b.position) ? 1 : -1),

		// map review array into an easier to use array 
		reviews: {
			...item.reviews,
			average: item.reviews.items.reduce((acc, val) => acc + (val.average_rating / item.reviews.items.length), 0),
			count: item.reviews.items.length,
		},

		// get uid's of each option variation
		options: mapOptions(item.configurable_options, item.variants),

		rawProduct: item
	}
}