import { AUDIENCE } from '../contants/audience'

export const getAudienceAssociationByAudienceNumber = (audience) => {
    const { MEN, WOMEN, UNISEX, KIDS } = AUDIENCE
    const _audience = audience && Number(audience)
    switch (_audience) {
        case MEN: return [MEN, UNISEX]
        case WOMEN: return [WOMEN, UNISEX]
        case UNISEX: return UNISEX
        case KIDS: return KIDS
        default: return undefined
    }
}

export const getAudienceAssociationByAudienceName = (audienceName) => {
    const { MEN, WOMEN, UNISEX, KIDS } = AUDIENCE
    switch (audienceName) {
        case 'men': return [MEN, UNISEX]
        case 'women': return [WOMEN, UNISEX]
        case 'unisex': return UNISEX
        case 'kids': return KIDS
        default: return undefined
    }
}