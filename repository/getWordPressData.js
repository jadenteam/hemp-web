/**
 * Load data from WordPress endpoint as promise
 * @param {String} endpoint 		The WordPress endpoint to call
 * @param {Object} params 			The params to pass as GET params
 */
export default async function getWordPressData(endpoint, params={}) {
	const urlParams = new URLSearchParams(params).toString()

	let response;

	try {
		response = await fetch(process.env.WORDPRESS_API + endpoint + '?' + urlParams)
	} catch(err) {
		throw "Failed to get wordpress data";
	} 

	if(!response.ok) {
		throw "Failed to get wordpress data";
	}

	return await response.json()
}