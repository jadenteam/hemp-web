import stripHtml from 'string-strip-html'

import { shopClient } from '../api/public'
import getManufacturers from './getManufacturers'
import { getManufacturerLabel, mapPrices } from './productHelpers'

/**
 * Get the product gallery data from Magento
 */
export default async function getProductShowcase() {
	const filter = {
		'showcase_product' : 1, // show all showcase products 
		...process.env.SHOP_CATEGORY_ID && { 'category_id': process.env.SHOP_CATEGORY_ID }, // if SHOP_CATEGORY_ID is set, only show products in that category
	}
	const filterString = Object.entries(filter).map(param => `${param[0]}: { eq: "${param[1]}" }`)

    const query = `
		{
			products(filter: { ${filterString.join(',')} }) {
				items {
					__typename
					id
					sku
					name
					url_key
					short_description {
						html
					}
					categories {
						id
						name
						level
					}
					manufacturer 
					price_range {
						minimum_price {
							regular_price {
								value
								currency
							}
							final_price {
								value
								currency
							}
							discount {
								amount_off
								percent_off
							}
						}
						maximum_price {
							regular_price {
								value
								currency
							}
							final_price {
								value
								currency
							}
							discount {
								amount_off
								percent_off
							}
						}
					}
					stock_status
					image {
						url
						label
					}
					small_image {
						url
						label
					}
					related_products {
						id
						sku
						name
						url_key
						manufacturer
						price_range {
							minimum_price {
								regular_price {
									value
									currency
								}
								final_price {
									value
									currency
								}
								discount {
									amount_off
									percent_off
								}
							}
							maximum_price {
								regular_price {
									value
									currency
								}
								final_price {
									value
									currency
								}
								discount {
									amount_off
									percent_off
								}
							}
						}
						image {
							url
							label
						}
						small_image {
							url
							label
						}
					}
				}
			}
		}
	`

    const response = await shopClient.request(query)
    return await mapShowcase(response.products.items)
}

export async function mapShowcase(items) {
	const manufacturers = await getManufacturers()

	return items.flatMap(item => {
		// remove all grouped products from products array 
		// we're using grouped products as promo collections (linked to directly via campaigns)
		if (item.__typename === 'GroupedProduct') {
			return []
		}
		
		return {
			...item,

			// replace manufacturer ID with object containing ID and label
			manufacturer: getManufacturerLabel(item.manufacturer, manufacturers),

			// strip html tags and convert from redundant object to string
			short_description: stripHtml(item.short_description.html).result,

			// map price_range object into an easier to use object 
			price: mapPrices(item.price_range),

			// limit related products to 4 and replace manufacturer ID with label 
			related_products: item.related_products.slice(0,4).map(function(product) {
				return {
					...product,
					manufacturer: getManufacturerLabel(product.manufacturer, this),
					price: mapPrices(product.price_range),
				}
			}, manufacturers)
		}
	})
}