import { shopClient } from '../api/public'

/**
 * Get the available product manufacturer options 
 */
export default async function getAudiences() {
    const query = `
    {
        customAttributeMetadata(
            attributes: [
                {
                attribute_code: "audience"
                entity_type: "catalog_product"
                }
            ]
        ) 
        {
            items {
                attribute_options {
                    value
                    label
                }
            }
        }
    }
    `
    const response = await shopClient.request(query)
    return response.customAttributeMetadata.items[0].attribute_options
}