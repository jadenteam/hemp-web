import { shopClient } from '../api/public'
import getManufacturers from './getManufacturers'
import getAudiences from './getAudiences'
import { getAudienceLabel, getManufacturerLabel, mapPrices, mapOptions } from './productHelpers'
import getShopCategoryID from './getShopCategoryID'
import { getAudienceAssociationByAudienceNumber } from '../repository/getAudienceAssociations'

/**
 * Get the products for a specific shop  
 */
export default async function getProducts(params = {}, currentPage = 1, pageSize = 99999, getAll = false) {

	const defaultCategoryID = process.env.SHOP_CATEGORY_ID && !getAll ? process.env.SHOP_CATEGORY_ID : [3,4,5,6,7,8] // if no category id set, get products from all shops
	const categorySlugResponse = await processCategorySlug(params.category, params.parentCategoryKey)

	let filter = {
		category_id: categorySlugResponse.category_id.length > 0 ? categorySlugResponse.category_id : defaultCategoryID
	}
	if (categorySlugResponse.audience) {
		filter.audience = getAudienceAssociationByAudienceNumber(categorySlugResponse.audience)
	}
	if (params.manufacturer) {
		filter.manufacturer = params.manufacturer
	}

	const filterString = Object.entries(filter).map(param => {
		if (Array.isArray(param[1])) {
			return `${param[0]}: { in: [${param[1].map(num => `"${num}"`)}] }`
		} else {
			return `${param[0]}: { eq: "${param[1]}" }`
		}
	})

	const query = `
	{
		products(filter: { ${filterString.join(',')} }, pageSize: ${pageSize}) {
			items {
				__typename
				id
				sku
				gtin
				name
				url_key
				short_description {
					html
				}
				description {
					html
				}
				manufacturer 
				audience
				featured_product
				categories {
					id
					name
					level
				}
				price_range {
					minimum_price {
						regular_price {
							value
							currency
						}
						final_price {
							value
							currency
						}
						discount {
							amount_off
							percent_off
						}
					}
					maximum_price {
						regular_price {
							value
							currency
						}
						final_price {
							value
							currency
						}
						discount {
							amount_off
							percent_off
						}
					}
				}
				stock_status
				only_x_left_in_stock
				image {
					url
					label
				}
				small_image {
					url
					label
				}
				media_gallery {
					url
					label
				}
				... on SimpleProduct {
					shipping_weight: weight
				}
				... on ConfigurableProduct {
					configurable_options {
						id
						attribute_code
						label
						attribute_id_v2
						values {
							value_index
							label
						}
					}
					variants {
						attributes {
							uid
							label
							code
							value_index
						}
						product {
							id
							url_key
							name
							image {
								url
								label
							}
							stock_status
							... on PhysicalProductInterface {
								shipping_weight: weight
							}
						}
					}
				}
			}
		}
	}
    `

	let res
	try {
		res = await shopClient.request(query)
	} catch (err) {
		throw 'Failed to get products'
	}

	// Keep only those products which are in stock
	const filteredProducts = filterProductsByStockLevel(res.products)

	// We have passed in some filter parameter
	if (params.filter) {

		// This exists because if we ever need to add more filters on we can easily just tack it onto this switch statement
		switch (params.filter.filterBy) {
			case 'featured': {
				// Keep only those products which are 'featured'
				const featuredProducts = filteredProducts.filter(product => product.featured_product)

				// We most likely will have passed in a count in which case we want to slice by this number, otherwise don't
				const maybeReducedProducts = params.filter.count ? featuredProducts.slice(0, params.filter.count) : featuredProducts

				return await mapProducts(maybeReducedProducts) 
			}
		}
	}

	return await mapProducts(filteredProducts)
}


const filterProductsByStockLevel = (products) => products.items.filter(product => product.stock_status === 'IN_STOCK')

export async function mapProducts(products) {
	const manufacturers = await getManufacturers()
	const audiences = await getAudiences()

	return {
		...products,
		items: products.flatMap(item => {
			// remove all grouped products from products array 
			// we're using grouped products as promo collections (linked to directly via campaigns)
			if (item.__typename === 'GroupedProduct') {
				return []
			}

			return {
				...item,
				inStock: item.stock_status === 'IN_STOCK',

				// replace manufacturer ID with object containing ID and label
				manufacturer: getManufacturerLabel(item.manufacturer, manufacturers),

				// replace audience ID with label object
				audience: getAudienceLabel(item.audience, audiences),

				// map price_range object into an easier to use object 
				price: mapPrices(item.price_range),

				// get uid's of each option variation
				options: mapOptions(item.configurable_options, item.variants),

			}
		}),
	}
}

/**
 * Transform the category slug(s) into magento query compatible IDs
 * @param {String} slug 	The category slug
 * @returns 
 */
async function processCategorySlug(slug, parentSlug) {
	let output = {}

	// if category is men/women, add audience filter to current site category
	// men/women audience filters are exclusive to bodycare/clothing 
	if(slug === 'men' || slug === 'women' || parentSlug === 'men' || parentSlug === 'women') {
		output.audience = slug === 'men' || parentSlug === 'men' ? '4' : '5'
		output.category_id = process.env.SHOP_CATEGORY_ID
	}

	// for everything else, attempt to magento category ID
	try {
		output.category_id = await getShopCategoryID(slug)
	} catch (err) {
		// failed to get a category ID
		// if not found, don't do any filtering
	}

	return output
}