import axios from 'axios'

export const getSalableQuantity = async (sku, stockId = 1) => {
    try {
        const response = await axios.get(`${process.env.MAGENTO_REST_API}inventory/get-product-salable-quantity/${sku}/${stockId}`, {
            headers: {
                'Authorization': `Bearer ${process.env.MAGENTO_REST_FRONTEND_TOKEN}`
            }
        })

        return response.data
    } catch (error) {
        console.log({ error })
    }
}