import { shopClient } from '../api/public'

/**
 * Get the available product manufacturer options 
 */
export default async function getCountries() {
    const query = `
    {
        countries {
            id
            two_letter_abbreviation
            three_letter_abbreviation
            full_name_locale
            full_name_english
            available_regions {
                id
                code
                name
            }
        }
    }
    `
    const response = await shopClient.request(query)
    return response.countries
}