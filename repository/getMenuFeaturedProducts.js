import stripHtml from 'string-strip-html'

import { shopClient } from '../api/public'
import getManufacturers from './getManufacturers'
import { getManufacturerLabel, mapPrices, mapImage } from './productHelpers'
import storeIndex from './getStores'

/**
 * Return array of 1 featured product from each store. Returns an empty object if none is selected.
 */
export default async function getMenuFeaturedProducts() {

    for (let i = 0; i < storeIndex.length; i++) {

        let store = storeIndex[i]

        const filter = {
            'featured_product': 1,
            'category_id': store.id
        }

        const filterString = Object.entries(filter).map(param => `${param[0]}: { eq: "${param[1]}" }`)
        // TODO: Optimize/Convert to single query
        const query = `
            {
                products(filter: { ${filterString.join(',')} }, pageSize:1) {
                    items {
                        id
                        sku
                        name
                        url_key
                        short_description {
                            html
                        }
                        manufacturer 
                        price_range {
                            minimum_price {
                                regular_price {
                                    value
                                    currency
                                }
                                final_price {
                                    value
                                    currency
                                }
                                discount {
                                    amount_off
                                    percent_off
                                }
                            }
                            maximum_price {
                                regular_price {
                                    value
                                    currency
                                }
                                final_price {
                                    value
                                    currency
                                }
                                discount {
                                    amount_off
                                    percent_off
                                }
                            }
                        }
                        image {
                            url
                            label
                        }
                    }
                }
            }
        
        `

        let product

        try {
            product = await shopClient.request(query)
        } catch (err) {
            storeIndex[i].featured = {}
            continue
        }

        storeIndex[i].featured = product?.products?.items[0] ? await mapProduct(product.products.items[0]) : {}

    }

    return storeIndex;

}

export async function mapProduct(item) {

    const manufacturers = await getManufacturers()

    return {
        ...item,
        short_description: stripHtml(item.short_description.html).result,
        manufacturer: getManufacturerLabel(item.manufacturer, manufacturers),
        price: mapPrices(item.price_range, item.__typename),
        image: mapImage(item.image)
    }

}