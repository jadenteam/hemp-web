import { shopClient } from '../api/public'

/**
 * Post a product review mutation 
 */
 export default async function setProductReview(sku, fields) {
	 const { name, rating, title, body } = fields
    const query = `
    mutation {
		createProductReview( 
			input: {
				sku: "${sku}",
				nickname: "${name}",
				summary: "${title}",
				text: "${body}",
				ratings: [
					{
					id: "${btoa('5')}", 
					value_id: "${btoa(rating)}"
					}
				]
			}
		)
		{
			review {
				average_rating
				ratings_breakdown {
					name
					value
				}
			}
		}
	}
    `

	try {
		const response = await shopClient.request(query)
		return response
	} catch(e) {
		return e.response
	}
}