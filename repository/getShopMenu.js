import { shopClient } from '../api/public'

/**
 * Search products 
 */
 export default async function getShopMenu(audience=false) {
	const query = `
	{
        categories( filters: { parent_id: { eq: "${process.env.SHOP_CATEGORY_ID}" } } ) {
            items {
                id
                level
                name
                url_key
                children {
                    id
                    level
                    name
                    url_key
                }
            }
        }
    }
	`

	try {
		const response = await shopClient.request(query)
		return await mapMenu(response.categories.items, audience)
	} catch(e) {
		console.error('Menu could not be retrieved')
	}
}

export async function mapMenu(categories, audience) {
	return categories.map(cat => ({ 
		id: cat.id,
		label: cat.name, 
		slug: cat.url_key,
		link: audience ? `/shop/${audience}/${cat.url_key}` : `/shop/${cat.url_key}`, 
		subcategories: !!cat.children ? cat.children.map(child => ({
			id: child.id,
			label: child.name,
			slug: child.url_key,
			link: audience ? `/shop/${audience}/${cat.url_key}/${child.url_key}` : `/shop/${cat.url_key}/${child.url_key}`
		})) : false
	}))
}
