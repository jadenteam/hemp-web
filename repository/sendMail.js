/**
 * Post to wordpress sendmail function
 * @param {Object} body 			The body to send
 */
 export default async function sendMail(body={}) {

	let res;

	const postData = {
		method: 'POST',
		headers: {
			'Content-Type' : 'application/x-www-form-urlencoded',
			'Accept': 'application/json'
		},
		body: new URLSearchParams(body)
	}

	try {

		res = await fetch(`${process.env.WORDPRESS_API}send`, postData)

	} catch(err) {

	}

	if(res.status === 200) {

		return res.json()
		
	} else {

		const data = await res.json();
		const err = new Error("Error in request");
		err.data = data;
		throw err;

	}

}