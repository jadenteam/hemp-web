import { shopClient } from '../api/public'

const getProducts = async () => {

    let category_id = process.env.SHOP_CATEGORY_ID

	const query = `
        {
            products(filter: { category_id: { eq: "${category_id}" } }, pageSize: 99999) {
                items {
                    __typename
                    url_key
                }
            }
        }
    `

	let res
	try {
		res = await shopClient.request(query)
	} catch (err) {
		throw 'Failed to get product slugs'
	}

    return res.products.items

}

export const getGroupedProductSlugs = async () => {

    let items = []

    try {
        items = await getProducts()
    } catch(err) {
        throw err
    }

    items = items.filter(item => item.__typename === "GroupedProduct")
    return items.map(item => item.url_key)

}

export const getProductSlugs = async () => {

	let items = []

    try {
        items = await getProducts()
    } catch(err) {
        throw err
    }

    items = items.filter(item => item.__typename !== "GroupedProduct")
    return items.map(item => item.url_key)

}