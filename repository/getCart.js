import Cookies from 'js-cookie'

import { shopClient } from '../api/public'
import { getManufacturerLabel, mapCart, mapPrices, mapOptions, mapCartOptions } from './productHelpers'

export const cartOptions = `
id
email
total_quantity
items {
	id
	quantity
	product {
		id
		sku
		name
		url_key
		manufacturer
		price_range {
			minimum_price {
				regular_price {
					value
					currency
				}
				final_price {
					value
					currency
				}
				discount {
					amount_off
					percent_off
				}
			}
			maximum_price {
				regular_price {
					value
					currency
				}
				final_price {
					value
					currency
				}
				discount {
					amount_off
					percent_off
				}
			}
		}
		stock_status
		only_x_left_in_stock
		image {
			url
			label
		}
		small_image {
			url
			label
		}
		... on ConfigurableProduct {
			configurable_options {
				id
				attribute_code
				label
				attribute_id_v2
				values {
					value_index
					label
				}
			}
			variants {
				attributes {
					uid
					label
					code
					value_index
				}
			}
		}
	}
	... on ConfigurableCartItem {
		configurable_options {
			id
			option_label
			value_id
			value_label
		}
	}
}
prices {
	discounts {
		amount {
			value
		}
		label
	}
	subtotal_excluding_tax {
		value
		currency
	}
	grand_total {
		value
		currency
	}
}
applied_coupons {
	code
}
shipping_addresses {
	firstname
	lastname
	street
	city
	region {
		region_id
		code
		label
	}
	postcode
	country {
		code 
		label
	}
	telephone
	available_shipping_methods {
		method_code
		method_title
		carrier_code
		carrier_title
		available
		amount {
			value
			currency 
		}
		price_excl_tax {
			value
			currency
		}
	}
	selected_shipping_method {
		amount {
			value
			currency
		}
		carrier_code
		carrier_title
		method_code
		method_title
	}
}
available_payment_methods {
	code
	title
}
`

/**
 * Get the current user's shopping bag 
 * If none already available, create a new one and set cookie 
 */
export default async function getCart(context) {
	const cookieName = 'hemp-forever-shopping-bag'
	const id = Cookies.get(cookieName)

	// get the existing cart if ID found
	if (!!id) {
		const cart = await getCartByID(id)
		if (cart) {
			return cart
		}
	}

	// if no existing cart found, create a new one
	const query = `
	mutation {
		createEmptyCart
	}`

	const response = await shopClient.request(query)

	// set the cart ID to cookie 
	Cookies.set(cookieName, response.createEmptyCart, { 'path': '/', 'sameSite': 'strict' })

	// get the newly created cart
	return await getCartByID(response.createEmptyCart)
}

export async function getCartByID(id) {
	const query = `{
		cart(cart_id: "${id}") {
			${cartOptions}
		}
	}`

	// if cart found, return its contents
	try {
		const response = await shopClient.request(query)
		return await mapCart(response)
	}
	// if no cart found, throw error and create a new one
	catch (error) {
		console.error(error.message)
		console.warn('Attempting to create new cart...')
	}
}

export async function addToCart(id, sku, quantity, options) {
	const formattedOptions = JSON.stringify(Object.keys(options).map(key => options[key]))
	const query = `
	mutation {
		addProductsToCart(
			cartId: "${id}"
			cartItems: [{
				quantity: ${quantity}
				sku: "${sku}",
				selected_options: ${formattedOptions}
			}]
		) {
			cart {
				${cartOptions}
			}
		}
	}`

	try {
		const response = await shopClient.request(query)
		return await mapCart(response.addProductsToCart)
	} catch (e) {
		return e.response
	}
}

export async function removeFromCart(id, productID) {
	const query = `
	mutation {
		removeItemFromCart(
			input: {
				cart_id: "${id}"
				cart_item_id: ${productID}
			}
		) {
			cart {
				${cartOptions}
			}
		}
	}`

	try {
		const response = await shopClient.request(query)
		return await mapCart(response.removeItemFromCart)
	} catch (e) {
		return e.response
	}
}

// Combine remove/add into single method
// Can't update configurable options via Magento GraphQL API, so we have to remove then add with the new selected options 
export async function updateInCart(id, productID, sku, quantity, selected_options) {
	try {
		await removeFromCart(id, productID)
	} catch (e) {
		return e.response
	}

	try {
		const mappedResponse = await addToCart(id, sku, quantity, selected_options)
		return mappedResponse
	} catch (e) {
		return e.response
	}
}

export function getProductAttributeInCart(cart, id, key) {
	if (key === 'quantity') {
		return cart.items.find(item => item.product.id === id).quantity
	} else {
		return cart.items.find(item => item.product.id === id).options
	}
}