import getShopMenu from './getShopMenu'
import { AUDIENCE } from '../contants/audience'
import { getAudienceAssociationByAudienceNumber } from './getAudienceAssociations'

/** 
 * Menu default state  
 * 	objects keys should match SITE_KEY env param 
 * 	shop menus fetched from magento
 */
export const defaultMenu = {
    'main': [
        { link: '/blog', label: 'Blog', active: false },
        { link: '/about', label: 'About', active: false },
        { link: '/contact', label: 'Contact Us', active: false },
    ],
    'bodycare': [
        { id: 'w1', link: '/shop/women', label: 'Women', slug: 'women', active: false, subcategories: [] },
        { id: 'm1', link: '/shop/men', label: 'Men', slug: 'men', active: false, subcategories: [] },
    ],
    'clothing': [
        { id: 'w1', link: '/shop/women', label: 'Women', slug: 'women', active: false, subcategories: [] },
        { id: 'm1', link: '/shop/men', label: 'Men', slug: 'men', active: false, subcategories: [] },
    ],
    'food': [],
    'homeware': [],
    'growhouse': [],
    'greenlight': [],
}

export async function getAudienceAssociatedMenu() {
    // Handling it slightly different for an Audience associated menu
    const newMenu = await Promise.all(
        defaultMenu[process.env.SITE_KEY].map(async cat => {
            let response = await getShopMenu(cat.slug)

            return {
                ...cat,
                subcategories: response
            }
        })
    )
    return newMenu
}

export async function getMenu() {
    const response = await getShopMenu()
    return response
}