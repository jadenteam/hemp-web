import { shopClient } from '../api/public'
import getManufacturers from './getManufacturers'
import getAudiences from './getAudiences'
import { getAudienceLabel, getManufacturerLabel, mapPrices, mapOptions } from './productHelpers'

/**
 * Search products 
 */
 export default async function getSearch(term) {
	const query = `
	{
		products(search: "${term}", filter: { category_id: { eq: "${process.env.SHOP_CATEGORY_ID}" } }, pageSize: 20, currentPage: 1) {
			page_info {
				current_page
				page_size
				total_pages
			}
			items {
				__typename
				id
				sku
				name
				url_key
				short_description {
					html
				}
				manufacturer 
				audience
				featured_product
				categories {
					id
					name
					level
				}
				price_range {
					minimum_price {
						regular_price {
								value
								currency
						}
						final_price {
								value
								currency
						}
						discount {
								amount_off
								percent_off
						}
					}
					maximum_price {
						regular_price {
							value
							currency
						}
						final_price {
							value
							currency
						}
						discount {
							amount_off
							percent_off
						}
					}
				}
				stock_status
				only_x_left_in_stock
				image {
					url
					label
				}
				small_image {
					url
					label
				}
				... on ConfigurableProduct {
					configurable_options {
						id
						attribute_code
						label
						attribute_id_v2
						values {
							value_index
							label
						}
					}
					variants {
						attributes {
							uid
							label
							code
							value_index
						}
					}
				}
			}
		}
	}
	`

	try {
		const response = await shopClient.request(query)
		return await mapProducts(response.products)
	} catch(e) {
		return 'No products found matching those search terms'
	}
}

export async function mapProducts(products) {
	const manufacturers = await getManufacturers()
	const audiences = await getAudiences()

	return {
		...products,
		items: products.items.flatMap(item => {
			// remove all grouped products from products array 
			// we're using grouped products as promo collections (linked to directly via campaigns)
			if (item.__typename === 'GroupedProduct') {
				return []
			}

			return {
				...item,
				inStock: item.stock_status === 'IN_STOCK',

				// replace manufacturer ID with object containing ID and label
				manufacturer: getManufacturerLabel(item.manufacturer, manufacturers),

				// replace audience ID with label object
				audience: getAudienceLabel(item.audience, audiences),

				// map price_range object into an easier to use object 
				price: mapPrices(item.price_range),

				// get uid's of each option variation
				options: mapOptions(item.configurable_options, item.variants)
			}
		})
	}
}