import stripHtml from 'string-strip-html'

import { shopClient } from '../api/public'
import getManufacturers from './getManufacturers'
import getAudiences from './getAudiences'
import getColours from './getColours'
import { getAudienceLabel, getManufacturerLabel, mapUpsellProductsAsColours, mapPrices, mapOptions, mapImage } from './productHelpers'

/**
 * Get the products for a specific shop  
 */
export default async function getProductConfigurableOptionsMeta(slug, selected) {

	const uidString = JSON.stringify(Object.values(selected))

    const query = `
	{
		products(filter: { url_key: { eq: "${slug}" } }) {
			items {
				id
				sku
				name
				url_key
				... on ConfigurableProduct {
					configurable_product_options_selection(configurableOptionValueUids: ${uidString}) {
						options_available_for_selection {
							attribute_code
							option_value_uids
						}
						variant {
							id
							uid
							sku
							name
							stock_status
							only_x_left_in_stock
							price_range {
								minimum_price {
									regular_price {
										value
										currency
									}
									final_price {
										value
										currency
									}
									discount {
										amount_off
										percent_off
									}
								}
								maximum_price {
									regular_price {
										value
										currency
									}
									final_price {
										value
										currency
									}
									discount {
										amount_off
										percent_off
									}
								}
							}
						}
					}
				}
			}
		}
	}
    `

	let res;
	try {
		res = await shopClient.request(query);
	} catch(err) {
		throw "Failed to get product"
	}

    return await !!res.products.items.length ? mapProduct(res.products.items[0]) : false
}

export async function mapProduct(item) {
	const variant = item.configurable_product_options_selection.variant
	return {
		...item.configurable_product_options_selection.variant,

		inStock: variant && variant.stock_status === 'IN_STOCK',

		// map price_range object into an easier to use object 
		price: variant ? mapPrices(item.configurable_product_options_selection.variant.price_range) : null,

		// // map images and sort media gallery photos by position
		// media_gallery: item.media_gallery.map(obj => mapImage(obj)).sort((a, b) => (a.position > b.position) ? 1 : -1),
	}
}