import { shopClient } from '../api/public'
import getManufacturers from './getManufacturers'
import { getUniqueCategories, getManufacturerLabel, mapPrices } from './productHelpers'

/**
 * Get the product gallery data from Magento
 */
export default async function getProductGallery() {
    const query = `
        {
            products(filter: { featured_product: { eq: "1" } }, pageSize: 99) {
                items {
                    __typename
                    id
                    sku
                    name
                    url_key
                    manufacturer # need to do separate call to get name
                    categories {
                        id
                        name
                        level
                    }
                    price_range {
                        minimum_price {
                            regular_price {
                                value
                                currency
                            }
                            final_price {
                                value
                                currency
                            }
                            discount {
                                amount_off
                                percent_off
                            }
                        }
                        maximum_price {
                            regular_price {
                                value
                                currency
                            }
                            final_price {
                                value
                                currency
                            }
                            discount {
                                amount_off
                                percent_off
                            }
                        }
                    }
                    stock_status
                    image {
                        url
                        label
                    }
                    small_image {
                        url
                        label
                    }
                }
            }
        }
    `
    const response = await shopClient.request(query)
    return await mapGallery(response.products.items)
}

export async function mapGallery(items) {
    const manufacturers = await getManufacturers()
    
    return {
        products: items.flatMap(item => {
            // remove all grouped products from products array 
			// we're using grouped products as promo collections (linked to directly via campaigns)
			if (item.__typename === 'GroupedProduct') {
				return []
			}

            return {
                ...item,

                // replace manufacturer ID with object containing ID and label
                manufacturer: getManufacturerLabel(item.manufacturer, manufacturers),

                // map price_range object into an easier to use object 
                price: mapPrices(item.price_range),
            }
        }),
        categories: getUniqueCategories(items),
    }   
}