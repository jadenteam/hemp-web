/**
 * Return JSON object of all social media links
 */
export default [
    {
        label_short: "FB",
        label: "Facebook",
        icon: "facebook",
        link: "https://www.facebook.com/hempcultureau",
        external: true
    },
    {
        label_short: "IG",
        label: "Instagram",
        icon: "instagram",
        link: "https://www.instagram.com/hempculture_au",
        external: true
    },
    // {
    //     label_short: "YT",
    //     label: "YouTube",
    //     icon: "youtube",
    //     link: "https://youtube.com",
    //     external: true
    // }
]