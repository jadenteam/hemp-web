/**
 * Return JSON object of all store data
 */
export const stores = [
    { id: 3, link: 'https://bodycare.hempforever.store', label: 'Bodycare', description: 'Natural hemp products for your face, body and hair' },
    { id: 4, link: 'https://clothing.hempforever.store', label: 'Clothing', description: 'Comfortable, stylish and sustainable hemp fashion' },
    { id: 5, link: 'https://food.hempforever.store', label: 'Food', description: 'Nutritious hemp food products to satisfy every craving' },
    { id: 6, link: 'https://lifestyle.hempforever.store', label: 'Lifestyle', description: 'Elegant hemp touches to complement your home' },
    //{ id: 7, link: 'https://hempforever.store', target: '_blank', label: 'Growhouse', description: 'All your hemp growing and extraction essentials' },
    // { link: 'https://hempforever.store', target: '_blank', label: 'Green Light District (+18)', description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit' },
]
export default stores

// Get a specific URL by category ID
// ignore if in development mode 
export const getURLByID = (id) => {
    const isDev = process.env.NODE_ENV === 'development'
    return id && !isDev ? stores.find(store => store.id === Number(id)).link : ''
}

export const getProductURLByCategoryID = (category_id, url_key) => {
    return `${getURLByID(category_id)}/shop/product/${url_key}`
}