import getManufacturers from './getManufacturers'
import getProductConfigurableOptionsMeta from './getProductConfigurableOptionsMeta';

/**
 * Get a list of unique categories from the products list 
 * @param {Array} products The products list 
 * @return {Array} Category objects 
 */
export function getUniqueCategories(products) {
    const categories = products.flatMap(p => p.categories).filter(cat => cat.level === 2)
    const unique = []
    const map = new Map()
    for (const item of categories) {
        if (!map.has(item.id)) {
            map.set(item.id, true);    // set any value to Map
            unique.push(item);
        }
    }
    return unique
}

export function getManufacturerLabel(id, manufacturers) {
    const defaultManufacturer = 'Hemp Forever' // used by products with no listed manufacturer 
    const manufacturer = manufacturers.find(obj => obj.value == id)
    return manufacturer ? { id: parseInt(manufacturer.value), label: manufacturer.label } : defaultManufacturer
}

export function getManufacturerID(label, manufacturers) {
    const manufacturer = manufacturers.find(obj => obj.label === label)
    return manufacturer ? { id: parseInt(manufacturer.value), label: manufacturer.label } : {}
}

export function getAudienceLabel(id, audiences) {
    const audience = audiences.find(obj => obj.value == id)
    return audience ? audience : false
}

export function mapUpsellProductsAsColours(item, colours) {
    // add the current item to the product list (to be mapped)
    if (item.upsell_products.length) {
        item.upsell_products.unshift({ colour: item.colour, url_key: item.url_key })
    }

    // map upsell products to format required by SelectOption component 
    return item.upsell_products.map(product => {
        const colour = colours.find(obj => obj.value == product.colour)

        if (!!colour) {
            return {
                uid: product.url_key,
                label: colour.label
            }
        }

        return false
    })
}

/**
 * Convert the massive price_range object into something easier to use 
 */
export function mapPrices(price_range, __typename) {
    const hasPriceRange = price_range.minimum_price.final_price.value < price_range.maximum_price.final_price.value
    const isDiscounted = price_range.minimum_price.discount.amount_off > 0 || price_range.maximum_price.discount.amount_off > 0

    return {
        // if product has a range of prices we just display the minimum with a prefix eg. "from $10"
        regular: hasPriceRange ? price_range.minimum_price.regular_price.value : price_range.maximum_price.regular_price.value,
        discounted: hasPriceRange ? price_range.minimum_price.final_price.value : price_range.maximum_price.final_price.value,
        currency: price_range.minimum_price.final_price.currency,

        // flags to simplify logic in components
        isDiscounted: isDiscounted,

        // No hasFromPrice if we are a simple product, otherwise just use normal hasPriceRange boolean
        hasFromPrice: __typename === 'SimpleProduct' ? false : hasPriceRange,
    }
}

/**
 * Mutate images objects inside media gallery
 */
export function mapImage(obj) {
    return {
        ...obj,
        src: obj.url,
        label: obj.label,
        height: false,
        width: false
    }
}

/**
 * Get UID's of each option variation
 * should've been able to get this via configurable_options but Magento GraphQL doesn't work for some reason 
 * see https://devdocs.magento.com/guides/v2.4/graphql/interfaces/configurable-product.html#configProdOptions 
 *
 * @param {Array} options 
 * @param {Array} variants 
 */
export function mapOptions(options, variants) {
    // collate all variants and options into single object
    const allVariants = variants ? variants.map((variant, i) => variant.attributes).flat() : []
    // const deduped = allVariants.filter((variant, i, self) => self.findIndex(variant2 => (variant2.uid === variant.uid)) === i)

    const output = options ? options.map(opt => ({
        ...opt,
        values: opt.values.map(itm => ({
            ...allVariants.find((item) => (item['value_index'] === itm['value_index']) && item),
            ...itm
        }))
    })) : false

    return output
}

export function mapCartOptions(options, variants) {
    const allVariants = variants ? variants.map((variant, i) => variant.attributes).flat() : []

    const merged = options ? options.map(opt => ({
        ...allVariants.find((item) => (item['value_index'] === opt['value_id']) && item),
    })) : []

    return Object.assign({}, ...merged.map(s => ({ [s.code]: s.uid })))
}

export async function mapCart({ cart }) {
    return {
        id: cart.id,
        email: cart.email,
        total_quantity: cart.total_quantity,
        items: await mapCartProducts(cart.items),
        prices: {
            subtotal: cart.prices.subtotal_excluding_tax.value,
            discount: !!cart.prices.discounts ? cart.prices.discounts[0].amount.value : 0,
            total: cart.prices.grand_total.value
        },
        payment_methods: cart.available_payment_methods,
        coupon: cart.applied_coupons && !!cart.applied_coupons.length ? cart.applied_coupons[0].code : '',
        shipping_address: {
            fname: cart.shipping_addresses[0] ? cart.shipping_addresses[0].firstname : '',
            lname: cart.shipping_addresses[0] ? cart.shipping_addresses[0].lastname : '',
            address: cart.shipping_addresses[0] ? cart.shipping_addresses[0].street.join(' ') : '',
            suburb: cart.shipping_addresses[0] ? cart.shipping_addresses[0].city : '',
            state: cart.shipping_addresses[0] ? cart.shipping_addresses[0].region.code : '',
            region_id: cart.shipping_addresses[0] ? cart.shipping_addresses[0].region.region_id : '',
            postcode: cart.shipping_addresses[0] ? cart.shipping_addresses[0].postcode : '',
            country: cart.shipping_addresses[0] ? cart.shipping_addresses[0].country.code : 'AU',
            phone: cart.shipping_addresses[0] ? cart.shipping_addresses[0].telephone : ''
        },
        shipping_methods: cart.shipping_addresses[0] ? cart.shipping_addresses[0].available_shipping_methods : []
    }
}

export async function mapCartProducts(items) {
    const manufacturers = await getManufacturers()

    return items.map(item => {
        return {
            ...item,

            product: {
                ...item.product,
                inStock: item.product.stock_status === 'IN_STOCK',

                // replace manufacturer ID with object containing ID and label
                manufacturer: getManufacturerLabel(item.product.manufacturer, manufacturers),

                // map price_range object into an easier to use object 
                price: mapPrices(item.product.price_range),

                // get uid's of each option variation
                options: mapOptions(item.product.configurable_options, item.product.variants)
            },

            // get uid for selected cart options
            options: mapCartOptions(item.configurable_options, item.product.variants)
        }
    })
        // sort by product ID 
        // prevents cart order changing when updating items 
        .sort((a, b) => a.product.id > b.product.id)
}

export async function getInitialisedProductOption(slug, product) {
    let configurableMetaProducts = []
    let initialisedOptionsLookup = {}

    if (product.__typename !== "ConfigurableProduct") return null

    try {

        if (product.variants && product.variants.length > 0) {
            for (const variant of product.variants) {
                const productVariant = variant.product
                const attributeVariant = variant.attributes[0]
                const meta = await getProductConfigurableOptionsMeta(slug, { [attributeVariant.code]: attributeVariant.uid })

                // Create a lookup of initialisedOptions keyed by the productVariantId we will later need to look up.
                initialisedOptionsLookup = {
                    ...initialisedOptionsLookup,
                    [productVariant.id]: {
                        [attributeVariant.code]: attributeVariant.uid
                    }
                }

                // Create a list of our configurableMetaProducts
                configurableMetaProducts.push(meta)

            }
        }

        // Filter out the ones that are not in stock
        configurableMetaProducts = configurableMetaProducts.filter(product => product.inStock)

        // Get the cheapest configurableOptionsMeta that we have
        const cheapestConfigurableMetaProduct = configurableMetaProducts
            .sort((a, b) => a.price.discounted < b.price.discounted ? -1 : a.price.discounted > b.price.discounted ? 1 : 0)[0] // <-- grabbing first element

        // Lookup the initialisedOptions keyed by our cheapestConfigurableMetaProduct ID.
        return initialisedOptionsLookup[cheapestConfigurableMetaProduct && cheapestConfigurableMetaProduct.id] || null

    } catch (error) {
        console.error(error)
    }

}