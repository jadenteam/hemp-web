import axios from 'axios'

import { shopClient } from '../api/public'
import { cartOptions } from './getCart'
import { mapCart } from './productHelpers'

export const placeOrder = async (id, email, paymentMethod) => {

	const body = {
		qp: {
			additional_data: {}, 
			method: paymentMethod
		},
		email
	}

	const req = { securePayObject: null, err: null }

	try {
		req.response = await axios.post(process.env.MAGENTO_REST_API + 'df-payment/' + id + '/place-order', { ...body })
	} catch(err) {
		if(err.response) {
			req.err = err.response.data.message
		} else {
			req.err = 'Failed to connect to server'
		}
		return req
	}

	req.securePayObject = JSON.parse(req.response.data).p

	return req

}

export const makePayment = async (postData) => {

	const data = new FormData();

	for ( let key in postData ) {
		data.append(key, postData[key]);
	}

	const req = { response: null, err: null }

	const instance = axios.create()

	// Catches result from 302 to api after authorise
	instance.interceptors.response.use(response => {
		return response
	}, error => {
		return Promise.reject(error)
	})

	try {

		req.response = await instance({
			method: 'post',
			url: process.env.SECUREPAY_AUTHORISE_ENDPOINT,
			data,
			headers: {
				'Content-Type': `multipart/form-data; boundary=${data._boundary}`
			},
			maxRedirects: 0
		})

	} catch(err) {
		throw err
	}

	return req.response

}

export async function addCheckoutDetailsToCart(id, { fname, lname, address, suburb, state, region_id, country, postcode, email, phone, method }) {

	try {

		try {
			await addGuestEmail(id, email)
		} catch(err) {
			throw 'Failed to add email to cart'
		}

		try {
			await addBillingAddress(id, fname, lname, address, suburb, state, region_id, country, postcode, phone)
		} catch(err) {
			throw 'Failed to add billing address to cart'
		}

		const cart = { response: null, err: null } 

		try {
			cart.response = await addShippingAddress(id, fname, lname, address, suburb, state, region_id, country, postcode, phone)
		} catch(err) {
			throw 'Failed to add shipping address to cart'
		}

		return cart.response

	} catch(err) {
		throw err
	}
}

export const addGuestEmail = async (id, email) => {

	const query = `
		mutation {
			setGuestEmailOnCart(input: {
				cart_id: "${id}"
				email: "${email}"
			}) 
			{
				cart {
					email
				}
			}
		}
	`
	const response = await shopClient.request(query)
	return response.setGuestEmailOnCart.cart.email

}

export const addShippingMethod = async (id, method ) => {

	const body = {
		addressInformation: {
			shipping_carrier_code: method,
			shipping_method_code: method
		}
	}

	const req = { response: null, err: null }

	try {
		req.response = await axios.post(process.env.MAGENTO_REST_API + 'guest-carts/' + id + '/shipping-information', { ...body })
	} catch(err) {
		throw err
	}

	return req.response

}

export async function addShippingAddress(id, fname, lname, address, suburb, state, region_id, country, postcode, phone) {
	const query = `
		mutation {
			setShippingAddressesOnCart(
				input: {
					cart_id: "${id}"
					shipping_addresses: [
						{
							address: {
								firstname: "${fname}"
								lastname: "${lname}"
								street: ["${address}"]
								city: "${suburb}"
								region: "${state}"
								region_id: ${region_id}
								postcode: "${postcode}"
								country_code: "${country}"
								telephone: "${phone}"
								save_in_address_book: false
							}
						}
					]
				}
			) {
				cart {
					${cartOptions}
				}
			}
		}
	`
	
	try {
		const response = await shopClient.request(query)
		return await mapCart(response.setShippingAddressesOnCart)
	} catch(e) {
		return e.response
	}
}

export async function addBillingAddress(id, fname, lname, address, suburb, state, region_id, country, postcode, phone) {
	const query = `
		mutation {
			setBillingAddressOnCart(
				input: {
					cart_id: "${id}"
					billing_address: {
						address: {
							firstname: "${fname}"
							lastname: "${lname}"
							street: ["${address}"]
							city: "${suburb}"
							region: "${state}"
							region_id: ${region_id}
							postcode: "${postcode}"
							country_code: "${country}"
							telephone: "${phone}"
							save_in_address_book: false
						}
					}
				}
			) {
			cart {
				billing_address {
					firstname
					lastname
					company
					street
					city
					region {
						code
						label
					}
					postcode
					telephone
					country {
						code
						label
					}
				}
			}
			}
		}
	`

	const response = await shopClient.request(query)
	return response.setBillingAddressOnCart.cart.billing_address
}


export async function addDeliveryMethod(cartId, method) {
	const query = `
		mutation {
			setShippingMethodsOnCart(input: {
				cart_id: "${cartId}"
				shipping_methods: [
					{
						carrier_code: "${method}"
						method_code: "${method}"
					}
				]
			}) 
			{
				cart {
					shipping_addresses {
						selected_shipping_method {
							carrier_code
							method_code
							carrier_title
							method_title
						}
					}
				}
			}
		}
	`

	const response = await shopClient.request(query)
	return response.setShippingMethodsOnCart.cart.shipping_addresses
}

export async function addCouponCode(id, couponCode) {
	const query = `
		mutation {
			applyCouponToCart(
				input: {
					cart_id: "${id}"
					coupon_code: "${couponCode}"
				}
			) 
			{
				cart {
					${cartOptions}
				}
			}
		}
	`

	try {
		const response = await shopClient.request(query)
		return await mapCart(response.applyCouponToCart)
	} catch(e) {
		return e.response
	}
}

export async function removeCouponCode(id) {
	const query = `
		mutation {
			removeCouponFromCart(
				input: {
					cart_id: "${id}"
				}
			) 
			{
				cart {
					${cartOptions}
				}
			}
		}
	`

	try {
		const response = await shopClient.request(query)
		if(response.removeCouponFromCart.cart.applied_coupons === null) {
			return await mapCart(response.removeCouponFromCart) 
		} else {
			throw 'Coupon code still applied'
		}
	} catch(e) {
		return e.response
	}
}