import { shopClient } from '../api/public'

/**
 * Get a product categories by it's url key 
 */
export default async function getShopCategoryID(url_key) {
    const query = `
    {
        categories( filters: { url_key: { eq: "${url_key}" } } ) {
            items {
                id
                level
                name
                url_key
                children {
                    id
                    level
                    name
                    url_key
                }
            }
        }
    }
    `
    const response = await shopClient.request(query)
    return response.categories.items.map(item => item.id)
}