## Environment

| package | version |
| ------- | ------- |
| node    | 14.15.1 |
| yarn    | 1.22.10 |

## Getting Started

Create .env.local from the example file and fill in details

```bash
cp .env.example .env
```

Install dependencies

```bash
yarn
```

Run the dev server

```bash
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

## Implementation

The frontend is built in [Next.js](https://nextjs.org/docs) and [React.js](https://reactjs.org/docs)

For markup and styling, the frontend uses [TailwindCSS](https://tailwindcss.com/docs) v2.0.1, a utility-first CSS framework. Customisations are located in `tailwind.config.js` (custom colours, spacing, font sizes, etc)

While using Tailwind in React, element/class duplication should be avoided. All elements that are styled the same way and are used more than once should be split into it's own reusable component (eg. headings, buttons, links, etc)

# Issues

## CORS

If you run into issues fetching data from the Magento server, check the CORS config.

1. SSH onto the server
2. Edit the Magento apache config file `httpd-app.conf`

```bash
sudo vi /opt/bitnami/apps/magento/conf/httpd-app.conf
```

3. Make any changes needed then restart apache

```bash
sudo /opt/bitnami/ctlscript.sh restart apache
```

More info:

- [Bitnami CORS](https://docs.bitnami.com/general/apps/wordpress/configuration/enable-cors-wordpress/) (for Wordpress but same principle applies)
- [Restarting Bitnami services](https://docs.bitnami.com/bch/apps/magento/administration/control-services/)
