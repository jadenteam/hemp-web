
/**
 * Apply a map to each value in an object, preserving keys 
 * @param {*} obj 	The object to map
 * @param {*} fn 	Callback applied to each value 
 */
export default function objectMap(obj, fn) {
	return Object.fromEntries( Object.entries(obj).map(([k, v], i) => [k, fn(v, k, i)]) )
}