/**
 * Convert WordPress date string to theme format 
 * @param {String} dateString 	The date tof romat in readable string format
 */
export function format(dateString) {

	const splits = dateString.split(" ")
	const dates = splits[0].split("-")
	const times = splits[1].split(":")
	
	const date = new Date(dates[0], dates[1], dates[2], times[0], times[1], times[2]);

	const formatter = new Intl.DateTimeFormat('en-AU', { month: 'short' });
	const month = formatter.format(date)

	return `${month} ${date.getDate()}, ${date.getFullYear()}`
};