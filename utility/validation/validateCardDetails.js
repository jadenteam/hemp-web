import * as yup from 'yup'

import { getValidationErrors } from './processValidation'

export const validateCardDetails = async cardDetails => {

    const year = new Date().getFullYear().toString().substr(-2)

    const schema = yup.object().shape({
        EPS_CARDNUMBER: yup.string().matches(/^\d+$/).length(16).required(),
        EPS_EXPIRYMONTH: yup.number().required().positive().integer().max(12),
        EPS_EXPIRYYEAR: yup.number().required().positive().integer().min(year),
        EPS_CCV: yup.number().required().positive().integer().max(999)
    })

    cardDetails = schema.cast(cardDetails)

    const validation = { isValid: false, message: '', errors: [] }

    try {
        await schema.validate(cardDetails, { abortEarly: false })
        validation.isValid = true
    } catch(err) {
        validation.errors = getValidationErrors(err)
        validation.isValid = false
    }

    return validation

}