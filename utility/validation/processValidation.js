export const getValidationErrors = err => {

    const validationErrors = {}

    err.inner.forEach(error => {
        if (error.path) {
          validationErrors[error.path] = error.message.replace(error.path, '').trim()
        }
    })

    return validationErrors

}