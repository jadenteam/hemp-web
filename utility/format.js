const currency = new Intl.NumberFormat("en-US", {
    style: "currency",
    currency: "USD", // set as USD to prevent formatting as 'A$xx.xx' <-- accommodates for future internationalisation
    minimumFractionDigits: 2,
    maximumFractionDigits: 5
})

const dollarsToCents = (dollars) => dollars * 100

const orderNumber = (num) => {
    return `#${String(num).padStart(7, '0')}`
}

const cc_format = value => {
    const v = value.replace(/\s+/g, '').replace(/[^0-9]/gi, '')
    const matches = v.match(/\d{4,16}/g);
    const match = matches && matches[0] || ''
    const parts = []

    for (i=0, len=match.length; i<len; i+=4) {
        parts.push(match.substring(i, i+4))
    }

    if (parts.length) {
        return parts.join(' ')
    } else {
        return value
    }
}

const cc_ccv = value => {
    const v = value.replace(/\s+/g, '').replace(/[^0-9]/gi, '')
    return v.substring(0,3)
}

const cc_ym = value => {
    const v = value.replace(/\s+/g, '').replace(/[^0-9]/gi, '')
    return v.substring(0,2)
}

/* Accepts Decimal and returns toFixed string */
const stringCurrency = value => {
    return value.toFixed(2).toString()
}

module.exports = { currency, dollarsToCents, orderNumber, cc_format, cc_ccv, cc_ym, stringCurrency };