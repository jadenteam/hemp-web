/**
 * Split an array into chunks of set size
 * @param {Array} array 	The array to split 
 * @param {Int} size 		The size of each chunk 
 * @return {Array}			Array of arrays 
 */
export function chunk(array, size) {
	if (!array || !array.length || size === 0) {
		return [];
	}

	const head = array.slice(0, size);
	const tail = array.slice(size);

	return [head, ...chunk(tail, size)];
}

/**
 * Group an array of objects by a value of the objects
 * @param {Array} objectArray 	The array to split into grouped object
 * @param {String} property 	The array key to group by 
 * @return {Object}				Object with keys reflecting grouped value name 
 */
export function groupBy(objectArray, property) {
	return objectArray.reduce(function (acc, obj) {
		var key = obj[property]
		if (!acc[key]) {
			acc[key] = []
		}
		acc[key].push(obj)
		return acc
	}, {})
}

/**
 * Merge two object arrays on a shared value 
 * @param {Array} a1 	First array of objects 
 * @param {Array} a2 	Second array of objects 
 * @param {String} key 	The key to group by, shared between both object arrays 
 * @return {Array}		Object array with merged objects, joined by key value 
 */
export function mergeBySharedValue(a1, a2, key) {
	return a1.map(itm => ({
		...a2.find((item) => (item[key] === itm[key]) && item),
		...itm
	}))
}