import { GraphQLClient } from 'graphql-request'

const shopClient = new GraphQLClient(process.env.MAGENTO_API, {
    headers: {
        'Accept': 'application/json',
        'Authorization': `Bearer ${process.env.MAGENTO_ADMIN_TOKEN}`,
    },
    method: 'POST'
})

module.exports = {
    endpoint: `${process.env.MAGENTO_API}/graphql`,
    shopClient
}