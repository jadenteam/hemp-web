import React from 'react'

import Header from '../components/Header'
import Layout from '../components/Layout'
import ShopHero from '../components/modules/ShopHero'
import { H1 } from '../components/modules/Headings'
import Product from '../components/modules/Product'
import Promo from '../components/modules/Promo'
import { SubscribeMini } from '../components/modules/Subscribe'
import PromoLinks from '../components/modules/PromoLinks'
import ProductShowcase from '../components/modules/ProductShowcase'

import getProducts from '../repository/getProducts'
import getProductShowcase from '../repository/getProductShowcase'
import getWordPressData from '../repository/getWordPressData'
import { chunk } from '../utility/array'
import getMenuFeaturedProducts from '../repository/getMenuFeaturedProducts'
import { useBanner } from '../hooks/useBanner'

export default function HomeGreenlight({ data, products, showcase, menuFeaturedProducts, banner }) {

    const [bannerActive, closeBanner] = useBanner()
    const productGroups = chunk(products.items, 3)

    return <Layout seo={data.seo} noheader menuFeaturedProducts={menuFeaturedProducts}>
        <div className="relative h-screen md:h-vw-65 mb-10 md:mb-vw-6 overflow-hidden">
            <Header 
                menuFeaturedProducts={menuFeaturedProducts}
                banner={banner}
                bannerActive={bannerActive}
                closeBanner={closeBanner}
            />
            <ShopHero 
                carousel={data.carousel} 
                banner={banner} 
                bannerActive={bannerActive}
            />
        </div>
		<div className="relative -top-vw-10 px-3 md:px-vw-7">
            {!!productGroups.length && <div className="mt-vw-4">
                <ProductGrid products={productGroups[0]} />
            </div>}
            <div className="mt-vw-8">
                <H1 className="text-center leading-tight -mb-vw-1.5 md:text-vw-10xl">New Arrivals</H1>
                <ProductShowcase products={showcase} hideRelated />
            </div>
            {productGroups.length > 1 && <div className="mt-vw-4">
                <ProductGrid products={productGroups[1]} addSubscribe />
            </div>}
            {data.promos.map((promo, i) => <div key={i} className="mt-vw-10">
                <Promo data={promo} reverse={i % 2} />
            </div>)}
            <div className="mt-vw-10 border-t border-grey-300 pt-vw-6">
                <PromoLinks {...data.footer} />                
            </div>
        </div>
    </Layout>
}

/**
 * Display product grid 
 * @param {Array} products          Products to display
 * @param {Boolean} addSubscribe    If true, replace the last item in row with the subscribe form 
 */
export function ProductGrid({ products, addSubscribe }) {
    return <section>
        <ul className="grid md:grid-cols-3 gap-4 md:gap-vw-2">
            {products.map((product, i) => {
                if(addSubscribe && products.length === i + 1) {
                    return <li key={i}><SubscribeMini /></li>
                } 
                return <Product key={i} {...{product}} quickbuy large />
            })}
        </ul>
    </section>
}

export async function getStaticProps(context) {
    return {
        props: {
			data: await getWordPressData(`shop/greenlight`),
            products: await getProducts({}, 1, 8),
            showcase: await getProductShowcase(),
            menuFeaturedProducts: await getMenuFeaturedProducts(),
            banner: await getWordPressData(`shop/greenlight/banner`)
        }
    }
}
