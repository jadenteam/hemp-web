import Layout from '../components/Layout'
import { H1 } from '../components/modules/Headings'
import getWordPressData from '../repository/getWordPressData'
import getMenuFeaturedProducts from '../repository/getMenuFeaturedProducts'

export default function FAQPage({ data: { title, body, seo }, menuFeaturedProducts }) {

	return (
		<Layout {...{seo}} title={title} menuFeaturedProducts={menuFeaturedProducts}>
			<H1>{title}</H1>
			{<div id="prestyled" dangerouslySetInnerHTML={{ __html: body }} className="w-full md:w-2/3 mx-auto" />}
		</Layout>
	)
}

export async function getStaticProps(context) {
    return {
        props: {
			data: await getWordPressData('help'),
			menuFeaturedProducts: await getMenuFeaturedProducts()
        }
    }
}