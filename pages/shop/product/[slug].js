import getProduct from '../../../repository/getProduct'
import getWordPressData from '../../../repository/getWordPressData'
import getMenuFeaturedProducts from '../../../repository/getMenuFeaturedProducts'
import { getProductSlugs } from '../../../repository/getProductSlugs'
import { SingleProduct } from '../../../components/modules/SingleProduct'
import { getInitialisedProductOption } from '../../../repository/productHelpers';

export default function SingleProductPage(props) {
	return <SingleProduct {...props} />
}

export async function getStaticPaths() {

	try {

		const productSlugs = await getProductSlugs()
		return {
			paths: productSlugs.map(productSlug => ({ params: { slug: productSlug } })),
			fallback: false
		}

	} catch(err) {
		return { paths: [], fallback: false }
	}

}

export async function getStaticProps(context) {

	let related_articles = [];
	let product = {};

	try {
		product = await getProduct(context.params.slug)
	} catch (err) {
		return { notFound: true } // trigger 404
	}

	let initialisedOption = await getInitialisedProductOption(context.params.slug, product)

	try {
		related_articles = await getWordPressData('related-articles', {
			sku: product?.sku,
			brand: product?.manufacturer.label,
			shop: process.env.SITE_KEY
		})
	} catch (err) {
		// no need to throw error, just fail silently
	}

	return {
		props: {
			related_articles,
			product,
			menuFeaturedProducts: await getMenuFeaturedProducts(),
			initialisedOption,
            banner: await getWordPressData(`shop/${process.env.SITE_KEY}/banner`)
		}
	}
}