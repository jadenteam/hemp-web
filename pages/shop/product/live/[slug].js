import getWordPressData from '../../../../repository/getWordPressData'
import getMenuFeaturedProducts from '../../../../repository/getMenuFeaturedProducts'
import getProduct from '../../../../repository/getProduct'
import { SingleProduct } from '../../../../components/modules/SingleProduct'

export default function LiveProductPage(props) {
    return <SingleProduct {...props} />
}

export async function getServerSideProps(context) {

    let related_articles = [];
    let product = {};

    try {
        product = await getProduct(context.params.slug)
    } catch (err) {
        return { notFound: true } // trigger 404
    }

    try {
            related_articles = await getWordPressData('related-articles', {
            sku: product ?.sku,
            brand: product ?.manufacturer.label,
            shop: process.env.SITE_KEY
        })
    } catch (err) {
        // no need to throw error, just fail silently
    }

    return {
        props: {
            related_articles,
            product,
            menuFeaturedProducts: await getMenuFeaturedProducts(),
            banner: await getWordPressData(`shop/${process.env.SITE_KEY}/banner`)
        }
    }
}