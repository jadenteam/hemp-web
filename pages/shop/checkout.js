import { useEffect } from 'react'
import Link from 'next/link'

import Layout from '../../components/Layout'
import Icon from '../../components/modules/Icons'
import FormPayment from '../../components/modules/FormPayment'
import { NumberOption, SelectOption } from '../../components/modules/Form'
import { IconButton } from '../../components/modules/Button'

//import { getProductAttributeInCart, removeFromCart, updateInCart } from '../../repository/getCart'
import useProductOptions from '../../hooks/useProductOptions'
//import getPaymentMethods from '../../repository/getPaymentMethods'
import { useCartContext } from '../../context/cart'
import Header from '../../components/Header'
import useCartUpdate from '../../hooks/useCartUpdate'
import { useDataLayer } from '../../hooks/useDataLayer'

import ModalProduct from "../../components/modules/ModalProduct";
import useBreakpoints from '../../hooks/useBreakpoints'
import { currency, stringCurrency } from '../../utility/format'

import getMenuFeaturedProducts from '../../repository/getMenuFeaturedProducts'
import ManufacturerLink from '../../components/modules/ManufacturerLink'
import ProductStockLabel from '../../components/modules/ProductStock'

export default function CheckoutPage({ menuFeaturedProducts }) {
	const { cart } = useCartContext()
	const { loading, updateCartItem, removeCartItem } = useCartUpdate()
	const { isMobile, isDesktop } = useBreakpoints()
	const [changeDataLayer] = useDataLayer()

	useEffect(() => {
		if(cart.items.length > 0) {
			try {
				const dlItems = cart.items.map(item => {
					const { name, price, manufacturer, sku } = item.product
					return {
						item_name: name,
						item_id: sku,
						item_brand: manufacturer.label,
						price: stringCurrency(price.regular),
						quantity: item.quantity
					}
				})
				changeDataLayer({
					dataLayer: {
						event: "begin_checkout",
						ecommerce: {
							items: dlItems
						}
					}
				})
			} catch(err) {
				// Failed to process event - ignore
			}
		}
	}, [cart])

	return <Layout noheader title="Checkout" menuFeaturedProducts={menuFeaturedProducts}>
		<Header checkout menuFeaturedProducts={menuFeaturedProducts} />
		<div className="p-5 pt-8 md:pt-vw-2 md:px-vw-7">
			{cart.total_quantity > 0 ?
				<div className="flex md:flex-row flex-col border-t border-grey-300 mb-vw-3">
					<article className="flex-grow">
						<h1 className="uppercase text-2xl md:text-vw-xl my-vw-2">Items</h1>
						{isDesktop ?
							<div className="grid grid-cols-checkout-mobile md:grid-cols-checkout gap-y-vw-0.5">
								<HeaderCell>Product</HeaderCell>
								<HeaderCell>{/* blank */}</HeaderCell>
								<HeaderCell>Price</HeaderCell>
								{/* <HeaderCell>Colour</HeaderCell> */}
								<HeaderCell>Size</HeaderCell>
								<HeaderCell>Quantity</HeaderCell>
								<HeaderCell>Remove</HeaderCell>
								{cart && !!cart.items.length && cart.items.map((item, i) => <ProductRow 
									key={item.id} 
									cartID={item.id} 
									product={item.product} 
									initialQuantity={item.quantity} 
									initialOptions={item.options} 
									update={updateCartItem} 
									remove={removeCartItem}
									{...{loading}} 
								/>)}
							</div>
						: <div>
							{cart && !!cart.items.length && cart.items.map((item, i) => <ModalProduct
								key={item.id} 
								cartID={cart.id} 
								id={item.id} 
								product={item.product} 
								initialQuantity={item.quantity} 
								initialOptions={item.options} 
								update={updateCartItem}
								remove={removeCartItem}
								{...{loading}}
							/>)}
							</div>
						}
					</article>
					<article className="w-full md:w-vw-25 ml-0 md:ml-vw-2">
						<h1 className="uppercase text-2xl md:text-vw-xl my-vw-2 mt-20 md:mt-7">Pay</h1>
						<div>
							<FormPayment />
						</div>
					</article>
				</div>
			: 
				<div>
					There are no products in your cart.
				</div>
			}
		</div>
	</Layout>
}

/**
 * Display a single cell inside header row
 */
export function HeaderCell({ children }) {
	return <div className="pt-vw-0.5 pb-vw-1 px-vw-0.5 text-center text-xs md:text-vw-xs">{children}</div>
}

/**
 * Display a single cell inside product row
 */
export function ProductCell({ children }) {
	return <div className="text-center flex justify-center items-center bg-grey-100 text-sm md:text-vw-sm">{children}</div>
}

/**
 * Display the product data and functionality to update/remove
 */
export function ProductRow({ loading, cartID, product, initialOptions, initialQuantity, update, remove }) {
	const { name, manufacturer, price, options, url_key, image, inStock, only_x_left_in_stock } = product 
	const { isConfigurable, meta, quantity, selectedOptions } = useProductOptions(url_key, options, initialOptions, initialQuantity)

	const regular_price = currency.format(price.regular);

	return <>
		<ProductCell>
			<Link href={`/shop/product/${url_key}`}>
				<a>
					<img src={image.url} alt={image.label} className="block w-full object-contain h-vw-6" />
				</a>
			</Link>
		</ProductCell>
		<div className="flex flex-col bg-grey-100 py-vw-1 px-vw-0.5">
			<div className="text-grey-600 text-vw-sm leading-tight"><ManufacturerLink {...manufacturer} /></div>
			<div className="text-vw-sm leading-tight">{name}</div>
			<div className="flex-grow flex items-bottom text-vw-xs">
				<ProductStockLabel 
					inStock={isConfigurable ? meta.inStock : inStock} 
					only_x_left_in_stock={isConfigurable ? meta.only_x_left_in_stock : only_x_left_in_stock} 
				/>
			</div>
		</div>
		<ProductCell>
			<div className="leading-snug">
				{/* {price.isDiscounted && <span className="text-grey-500 line-through mr-1 md:mr-vw-0.3">{regular_price}</span>}
				<span>{price.isDiscounted ? currency.format(price.discounted) : regular_price}</span> */}
				{price.isDiscounted && <span className="text-grey-500 line-through mr-1 md:mr-vw-0.3">{isConfigurable && !!meta.price ? currency.format(meta.price.regular) : regular_price}</span>}
				<span>
					{isConfigurable && !!meta.price ? 
						(meta.price.isDiscounted ? currency.format(meta.price.discounted) : currency.format(meta.price.regular))
					:
						(price.isDiscounted ? currency.format(price.discounted) : regular_price)
					}
				</span>
			</div>
		</ProductCell>
		{/* <ProductCell>
			<SelectOption 
				name="color"
				values={options.find(item => item.attribute_code === 'color').values}
				selected={selectedOptions.color}
				setSelected={handleOptionChange} 
			/>
		</ProductCell> */}
		<ProductCell>
			{options && (
				loading ? 
					<Icon type="loading" className="relative top-1 md:top-vw-0.3" /> 
				:  
					<SelectOption 
						name="size"
						values={options.find(item => item.attribute_code === 'size').values}
						selected={selectedOptions.size}
						setSelected={e => update(cartID, product, 'size', e.target.value)} 
					/>
				)
			}
		</ProductCell>
		<ProductCell>
			{loading ? 
				<Icon type="loading" className="relative top-1 md:top-vw-0.3" />
			: 
				<NumberOption name="quantity" value={quantity} setValue={value => update(cartID, product, 'quantity', value)} max={isConfigurable ? (meta.only_x_left_in_stock || 99) : (only_x_left_in_stock ? only_x_left_in_stock : 99)} />
			}
		</ProductCell>
		<ProductCell>
			<IconButton icon="close" variant="transparent" onClick={() => remove(cartID, product)} />
		</ProductCell>
	</>
}

export async function getServerSideProps(context) {
	return {
		props: {
			menuFeaturedProducts: await getMenuFeaturedProducts()
		}
	}
}