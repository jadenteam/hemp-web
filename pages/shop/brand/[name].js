
import Layout from '../../../components/Layout'
import { H1 } from '../../../components/modules/Headings'
import Product from '../../../components/modules/Product'

import getProducts from '../../../repository/getProducts'
import getMenuFeaturedProducts from '../../../repository/getMenuFeaturedProducts'
import getManufacturers from '../../../repository/getManufacturers'
import getWordPressData from '../../../repository/getWordPressData'
import { getManufacturerID } from '../../../repository/productHelpers'
import { useBanner } from '../../../hooks/useBanner'

export default function BrandListPage({ name, products, menuFeaturedProducts, banner }) {

	const [bannerActive, closeBanner] = useBanner()

	return (
		<Layout
			title={`${name} | Hemp Products Australia | Hemp Forever`}
			menuFeaturedProducts={menuFeaturedProducts}
			banner={banner}
			bannerActive={bannerActive}
			closeBanner={closeBanner}
		>
			<section>
				<H1>{name}</H1>
				<ul className="mt-10 md:mt-vw-4 grid md:grid-cols-4 gap-4 md:gap-vw-2">
					{products.items.map((product, i) => {
						const alt = i % 10 === 8 || i % 10 === 9 // every 9th/10th item 
						return <Product key={i} {...{ product }} className={alt && 'md:col-span-2'} quickbuy />
					})}
				</ul>
			</section>
		</Layout>
	)
}

export async function getStaticPaths() {

	try {

		const res = await getManufacturers()
		return {
			paths: res.map(brand => ({ params: { name: brand.label } })),
			fallback: false
		}

	} catch (err) {
		return { paths: [], fallback: false }
	}

}

export async function getStaticProps(context) {

	let brandID;

	try {

		const manufacturers = await getManufacturers()
		brandID = getManufacturerID(context.params.name, manufacturers).id

	} catch (err) {
		console.error('Could not find brand')
	}

	return {
		props: {
			name: context.params.name,
			products: await getProducts({ 'manufacturer': brandID }, 1, 1000), // Hack to get all products
			menuFeaturedProducts: await getMenuFeaturedProducts(),
			banner: process.env.SITE_KEY !== 'main' ? await getWordPressData(`shop/${process.env.SITE_KEY}/banner`) : {} // have to use conditional because brands are used on homepage
		}
	}
}