import { useState } from 'react'

import Layout from '../../../components/Layout'
import { H1 } from '../../../components/modules/Headings'
import Product from '../../../components/modules/Product'

import getGroupedProduct from '../../../repository/getGroupedProduct'
import { getGroupedProductSlugs } from '../../../repository/getProductSlugs'
import getMenuFeaturedProducts from '../../../repository/getMenuFeaturedProducts'
import getWordPressData from '../../../repository/getWordPressData'
import { useBanner } from '../../../hooks/useBanner'

export default function CollectionListPage({ group, menuFeaturedProducts, banner }) {

	const [bannerActive, closeBanner] = useBanner()

	return (
		<Layout 
			title={group.name} 
			menuFeaturedProducts={menuFeaturedProducts}
			banner={banner}
            bannerActive={bannerActive}
            closeBanner={closeBanner}
		>
			<section>
				<H1>{group.name}</H1>
				<div dangerouslySetInnerHTML={{ __html: group.description }}></div>
				<ul className="mt-10 md:mt-vw-4 grid md:grid-cols-4 gap-4 md:gap-vw-2">
					{group.products.map((product, i) => {
						const alt = i % 10 === 8 || i % 10 === 9 // every 9th/10th item 
						return <Product key={i} {...{product}} className={alt && 'col-span-2'} quickbuy />
					})}
				</ul>
			</section>
		</Layout>
	)
}

export async function getStaticPaths() {

	try {

		const groupedProductSlugs = await getGroupedProductSlugs()
		return {
			paths: groupedProductSlugs.map(groupedProductSlug => ({ params: { slug: groupedProductSlug } })),
			fallback: false
		}

	} catch(err) {
		return { paths: [], fallback: false }
	}

}

export async function getStaticProps(context) {
    return {
        props: {
			group: await getGroupedProduct(context.params.slug),
			menuFeaturedProducts: await getMenuFeaturedProducts(),
			banner: await getWordPressData(`shop/${process.env.SITE_KEY}/banner`)
		}
	}
}