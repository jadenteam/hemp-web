import Layout from '../../../components/Layout'
import getMenuFeaturedProducts from '../../../repository/getMenuFeaturedProducts'
import { orderNumber } from '../../../utility/format'
import { Button } from '../../../components/modules/Button'
import Header from '../../../components/Header'
import Link from 'next/link'

export default function OrderCompletePage({ id, menuFeaturedProducts }) {
    return (
        <Layout noheader title="Order complete" menuFeaturedProducts={menuFeaturedProducts}>
            <Header checkout menuFeaturedProducts={menuFeaturedProducts} />
            <div className="border-t border-b border-grey-200 grid grid-cols-3 px-5 md:px-0 py-vw-7 md:mx-vw-7 mt-vw-1.5">
                <div className="md:col-span-1 col-span-3 flex md:block justify-center">
                    <img src="/images/thank-you.svg" />
                </div>
                <div className="md:col-span-2 col-span-3 mt-vw-5 md:mt-0 flex items-center">
                    <div>
                        <h1 className="font-serif font-bold tracking-tight text-5xl md:text-vw-3xl underline">Thanks for your purchase!</h1>
                        <p className="text-vw-4xl md:text-vw-xl mt-vw-5 md:mt-vw-2 leading-relaxed">
                            Your order number is <strong>{orderNumber(id)}.</strong> Please check your email for full details of you order. Thank you for shopping with Hemp Forever, we hope to see you again soon.
                        </p>
                        <div className="md:w-vw-20 mt-vw-5 md:mt-0">
                            <Link href="/shop">
                                <a>
                                    <Button variant="black" className="mt-vw-2">Continue Shopping</Button>
                                </a>
                            </Link>
                        </div>
                    </div>
                </div>
            </div>
        </Layout>
    )
}

export async function getServerSideProps(context) {
    return {
        props: {
            id: context.params.id,
            menuFeaturedProducts: await getMenuFeaturedProducts()
        }
    }
}