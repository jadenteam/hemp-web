import { useEffect } from 'react'
import cn from 'classnames'
import Breadcrumbs from 'nextjs-breadcrumbs'
import Link from 'next/link'
import { useRouter } from 'next/router'

import Layout from '../../components/Layout'
import Product from '../../components/modules/Product'
import Header from '../../components/Header'

import getProducts from '../../repository/getProducts'
import getMenuFeaturedProducts from '../../repository/getMenuFeaturedProducts'
import getWordPressData from '../../repository/getWordPressData'
import useBreakpoints from '../../hooks/useBreakpoints'
import { useMenuContext } from '../../context/menu'
import ShopMobileFilters from '../../components/modules/ShopMobileFilters'
import getShopCategories from '../../repository/getShopCategories'
import { useSortableProductList } from '../../hooks/useSortableProductList'
import { useFilteredSubMenu } from '../../hooks/useFilteredSubMenu'
import { useBanner } from '../../hooks/useBanner'
import { useDataLayer } from '../../hooks/useDataLayer'
import { stringCurrency } from '../../utility/format'

const getShopCategoryTitle = (path) => {
	const prefix = '/shop'
	const defaultPath = "Hemp Forever"

	switch (process.env.SITE_KEY) {
		case 'bodycare': {
			switch (path) {
				case `${prefix}/face`: return "Hemp Skincare | Buy Hemp Products Online | Hemp Forever"
				case `${prefix}/body`: return "Hemp Body Care | Buy Hemp Products Online | Hemp Forever"
				case `${prefix}/hair`: return "Hemp Hair Products | Buy Hemp Hair Products | Hemp Forever"
				case `${prefix}/women`: return "Hemp Body Care Products for Women | Hemp Forever"
				case `${prefix}/men`: return "Hemp Body Care Products for Men | Hemp Forever"
				default:
					return defaultPath
			}
		}
		case 'clothing': {
			switch (path) {
				case `${prefix}/women`: return "Women's Hemp Clothing | Buy Hemp Clothing | Hemp Forever"
				case `${prefix}/men`: return "Men's Hemp Clothing | Buy Hemp Clothing | Hemp Forever"
				default:
					return defaultPath
			}
		}
		case 'food': {
			switch (path) {
				case `${prefix}/food`: return "Hemp Foods | Buy Hemp Food Online | Hemp Forever"
				case `${prefix}/superfood-blends`: return "Hemp Superfood Blends | Buy Hemp Superfoods | Hemp Forever"
				default:
					return defaultPath
			}
		}
		case 'homeware': {
			switch (path) {
				case `${prefix}/homewares`: return "Hemp Homewares | Buy Hemp Homewares | Hemp Forever"
				case `${prefix}/diy`: return "Hemp DIY | Buy Hemp DIY Products Online | Hemp Forever"
				case `${prefix}/education`: return "Hemp Education | Buy Hemp Books Online | Hemp Forever"
				case `${prefix}/fragrance-incense`: return "Hemp Fragrance | Hemp Incense Online | Hemp Forever"
				case `${prefix}/pets`: return "Hemp for Pets | Hemp Oil for Pets Online | Hemp Forever"
				default:
					return defaultPath
			}
		}
		default:
			return defaultPath
	}

}

export default function ProductsListPage({ products, allProducts, category, parentCategory, parentCategoryKey, level, menuFeaturedProducts, banner }) {
	const { menu } = useMenuContext()
	const { isDesktop } = useBreakpoints()
	const submenu = useFilteredSubMenu(category, level, parentCategory, parentCategoryKey, allProducts)
	const [sortedProducts, sorters, sortedByLabel, handleChangeSort] = useSortableProductList(products)
	const router = useRouter()
	const [bannerActive, closeBanner] = useBanner()
	const [changeDataLayer] = useDataLayer()

	const pageTitle = getShopCategoryTitle(router.asPath)

	useEffect(() => {

		try {
			const dlItems = sortedProducts.map(product => {
				const { name, price, manufacturer, sku } = product
				return {
					item_name: name,
					item_id: sku,
					item_brand: manufacturer.label,
					price: stringCurrency(price.regular)
				}
			})
			changeDataLayer({
				dataLayer: {
					event: "view_item_list",
					ecommerce: {
						items: dlItems
					}
				}
			})
		} catch (err) {
			// Failed to process event - ignore
		}

	}, [sortedProducts])

	return (
		<Layout
			noheader
			title={pageTitle}
			menuFeaturedProducts={menuFeaturedProducts}
		>
			<Header
				menuFeaturedProducts={menuFeaturedProducts}
				banner={banner}
				bannerActive={bannerActive}
				closeBanner={closeBanner}
			/>

			<section className="py-5 px-3 md:py-vw-2 md:px-vw-7">
				<Breadcrumbs
					rootLabel="Home"
					listClassName="add-separators md:add-separators-desktop flex space-x-2 md:space-x-vw-0.5 capitalize"
					activeItemClassName="text-grey-400"
				/>
			</section>

			{isDesktop &&
				<section className="flex justify-between mb-18 md:mb-vw-1 p-3 pt-4 md:pt-vw-2 md:px-vw-7 text-sm md:text-vw-sm">
					<ul className="flex flex-grow items-center">
						{
							!!menu && submenu.map((item, i) => <li key={item.id} className='mr-6 md:mr-vw-2'>
								<Link href={item.link}>
									<a className={cn('uppercase', item.active && 'text-grey-400')}>{item.label}</a>
								</Link>
							</li>)
						}
					</ul>
					<div className="relative">
						<Dropdown options={sorters} onChange={(e) => handleChangeSort(e.target.value)} />
					</div>
				</section>
			}

			<section className="p-3 md:px-vw-7">
				<ul className="grid md:grid-cols-4 gap-4 md:gap-vw-2">
					{sortedProducts.map((product, i) => {
						return <Product key={product.id} {...{ product }} quickbuy />
					})}
				</ul>
			</section>

			<ShopMobileFilters {...{ sorters, sortedByLabel }} onChangeSort={handleChangeSort} menu={!!menu ? submenu : []} />
		</Layout>
	)
}

export async function getStaticPaths() {

	const addAudiencePaths = process.env.SITE_KEY === 'clothing' || process.env.SITE_KEY === 'bodycare'

	try {
		const res = await getShopCategories() // get all possible shop subcategories

		const audiences = ['men', 'women']

		const categories = res.reduce((acc, curr) => {

			if (addAudiencePaths) {
				audiences.map(audience => {
					// do not add certain categories for certain audiences
					if (audience === 'men' && curr.slug === 'dresses') {
						return acc
					}
					acc.push({ params: { category: [audience, curr.slug] } })
				})
			} else {
				acc.push({ params: { category: [curr.slug] } })
				curr.subcategories.map(sub => acc.push({ params: { category: [curr.slug, sub.slug] } }))
			}

			return acc

		}, [])

		return {
			paths: [
				{ params: { category: false } },  // root path - always here
				...(addAudiencePaths ? audiences.map(audience => ({ params: { category: [audience] } })) : []), // audience paths
				...categories // category and subcategory paths
			],
			fallback: false
		}

	} catch (err) {
		return { paths: [], fallback: false }
	}

}

export async function getStaticProps(context) {
	const { category } = context.params
	let error = null

	// ensure we always have a category value 
	// if categories available, onlyuse last in array (most specific subcategory)
	const parentCategoryKey = category ? category[0] : false
	const categoryKey = category ? category.slice(-1)[0] : process.env.SITE_KEY

	try {
		const products = await getProducts({
			category: categoryKey,
			parentCategoryKey
		})

		// Yes we are calling this twice. We need to receive the entire set of products with the generic category key passed through.
		// We are doing this in this function as it will happen on build so we don't mind.
		const allProducts = await getProducts({
			category: process.env.SITE_KEY,
			parentCategoryKey
		})

		return {
			props: {
				products: products.items,
				allProducts: allProducts.items,
				key: category ? category : 'all', // necessary for reloading page between dynamic routes
				category: categoryKey,
				parentCategoryKey,
				parentCategory: parentCategoryKey !== categoryKey && parentCategoryKey, // only add if different from category
				level: category && category.length ? category.length : 0,
				menuFeaturedProducts: await getMenuFeaturedProducts(),
				banner: process.env.SITE_KEY !== 'main' ? await getWordPressData(`shop/${process.env.SITE_KEY}/banner`) : {}, // have to use conditional because brands are used on homepage
				error
			}
		}
	} catch (err) {
		return {
			props: {
				products: [],
				key: category ? category : 'all', // necessary for reloading page between dynamic routes
				category: categoryKey,
				parentCategory: parentCategoryKey !== categoryKey && parentCategoryKey, // only add if different from category
				level: category && category.length ? category.length : 0,
				menuFeaturedProducts: await getMenuFeaturedProducts(),
				banner: process.env.SITE_KEY !== 'main' ? await getWordPressData(`shop/${process.env.SITE_KEY}/banner`) : {}, // have to use conditional because brands are used on homepage
				error: 'Could not get products'
			}
		}
	}


}

/**
 * Display filter select for mobile 
 */
export function Dropdown({ options, onChange }) {
	return (
		<select onChange={onChange}>
			{options.map(option => <option key={option.value} value={option.value}>{option.label}</option>)}
		</select>
	)
}