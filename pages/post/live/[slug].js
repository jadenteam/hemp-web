import getWordPressData from '../../../repository/getWordPressData'
import getMenuFeaturedProducts from '../../../repository/getMenuFeaturedProducts'
import { SinglePost } from '../../../components/modules/SinglePost'

export default function LiveBlogPost({ data, menuFeaturedProducts }) {
    return <SinglePost {...{ data }} menuFeaturedProducts={menuFeaturedProducts} />
}

export async function getServerSideProps(context) {
    let data;

    try {
        data = await getWordPressData('post', {
            slug: context.params.slug
        })
    } catch (err) {
        data = {
            error: err
        }
    }

    return {
        props: {
            data,
            menuFeaturedProducts: await getMenuFeaturedProducts()
        },
    }
}