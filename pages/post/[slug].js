import getMenuFeaturedProducts from '../../repository/getMenuFeaturedProducts'
import getWordPressData from '../../repository/getWordPressData'
import { SinglePost } from '../../components/modules/SinglePost'

export default function PostPage({ data, menuFeaturedProducts }) {
	return <SinglePost {...{ data }} menuFeaturedProducts={menuFeaturedProducts} />
}

export async function getStaticPaths() {

	try {

		const res = await getWordPressData('static/post') // get all possible post slugs
		return {
			paths: res.map(slug => ({ params: { slug } })),
			fallback: false
		}

	} catch(err) {
		return { paths: [], fallback: false }
	}

}

export async function getStaticProps(context) {

	let data;

	try {
		data = await getWordPressData('post', {
			slug: context.params.slug
		})
	} catch(err) {
		data = {
			error: err
		}
	}

	return { 
		props: { 
			data,
			menuFeaturedProducts: await getMenuFeaturedProducts()
		} 
	}
}