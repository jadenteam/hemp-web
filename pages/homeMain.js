import Header from '../components/Header'
import Layout from '../components/Layout'
import { H2 } from '../components/modules/Headings'
import PostItem from '../components/modules/PostItem'
import Hero from '../components/modules/Hero'
import Subscribe from '../components/modules/Subscribe'
import Mission from '../components/modules/Mission'
import Testimonials from '../components/modules/Testimonials'
import ProductGallery from '../components/modules/ProductGallery'
import ProductShowcase from '../components/modules/ProductShowcase'
import Location from '../components/modules/Location'
import { FadeIn } from '../components/modules/Parallax'

import getWordPressData from '../repository/getWordPressData'
import getProductGallery from '../repository/getProductGallery'
import getProductShowcase from '../repository/getProductShowcase'
import getMenuFeaturedProducts from '../repository/getMenuFeaturedProducts'

export default function HomeMain({ data: { carousel, featured_posts, latest_posts, mission, testimonials, product_gallery, product_showcase, location, subscribe, seo }, gallery, showcase, menuFeaturedProducts }) {
	return (
		<Layout
			{...{ seo }}
			noheader
			menuFeaturedProducts={menuFeaturedProducts}
			title="Buy Hemp Foods & Hemp Products Australia | Hemp Forever"
		>
			<div className="relative h-screen md:h-vw-50 mb-10 md:mb-vw-6 overflow-hidden">
				<Header home menuFeaturedProducts={menuFeaturedProducts} />
				<Hero {...{ carousel }} />
			</div>

			<div className="p-5 md:px-vw-7 md:pt-0 md:pb-vw-4">
				<section className="pt-10 pb-20 md:py-0 md:mb-vw-4">
					<FadeIn>
						<H2>{featured_posts.title}</H2>
						<p className="text-md md:text-vw-lg">{featured_posts.description}</p>
					</FadeIn>
					<FadeIn className="md:flex flex-col flex-wrap mt-6 md:flex-row md:pb-vw-4 md:mt-vw-3 md:-mx-vw-1">
						{featured_posts.data.map((post, i) => <PostItem key={i} {...post} className="mb-10 md:mb-0 md:w-1/2 md:px-vw-1" />)}
					</FadeIn>
				</section>

				<FadeIn as="section" className="mb-14 md:mb-vw-8">
					<H2>{product_gallery.title}</H2>
					<ProductGallery {...gallery} className="overflow-hidden" />
				</FadeIn>

				<FadeIn>
					<Mission {...mission} className="mt-20 md:mt-0" />
				</FadeIn>
			</div>

			<FadeIn>
				<Subscribe data={subscribe} className="my-4 md:my-vw-4" variant="home" />
			</FadeIn>

			<div className="py-20 px-5 md:px-vw-7">
				<FadeIn as="section" className="mb-4 md:mb-vw-4">
					<H2>{product_showcase.title}</H2>
					<p className="md:mb-vw-4 ">{product_showcase.description}</p>
					<ProductShowcase products={showcase} isHome className="mt-4 md:mt-vw-2" />
				</FadeIn>

				<FadeIn>
					<Testimonials {...testimonials} />
				</FadeIn>

				<FadeIn as="section" className="mb-4 md:mb-vw-4">
					<H2>{latest_posts.title}</H2>
					<p>{latest_posts.description}</p>
					<div className="flex flex-wrap mt-4 md:pb-vw-4 md:mt-vw-3 md:-mx-vw-1">
						{latest_posts.data.map((post, i) => <PostItem key={i} {...post} className="mb-10 md:mb-0 w-full md:w-1/3 md:px-vw-1" />)}
					</div>
				</FadeIn>

				<FadeIn>
					<Location {...location} />
				</FadeIn>
			</div>
		</Layout>
	)
}

export async function getStaticProps(context) {
	return {
		props: {
			data: await getWordPressData('home'),
			gallery: await getProductGallery(),
			showcase: await getProductShowcase(),
			menuFeaturedProducts: await getMenuFeaturedProducts()
		}
	}
}