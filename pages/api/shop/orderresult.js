import Cors from 'cors'

const cors = initMiddleware(
    // You can read more about the available options here: https://github.com/expressjs/cors#configuration-options
    Cors({
      // Only allow requests with GET, POST and OPTIONS
      methods: ['GET', 'POST', 'OPTIONS'],
    })

)

function initMiddleware(middleware) {
    return (req, res) =>
      new Promise((resolve, reject) => {
        middleware(req, res, (result) => {
          if (result instanceof Error) {
            return reject(result)
          }
          return resolve(result)
        })
    })
}

export default async function handler(req, res) {
    
    await cors(req, res)

    const params = req.query
    const response = { success: false, message: '' }

    console.log(params)

    if(params.summarycode === '1') {

      response.success = true
      response.message = 'The transaction processed successfully'

    } else if(params.summarycode !== '1') {

      if(params.summarycode === '2') response.message = 'Declined by bank'
      if(params.summarycode === '3') response.message = 'Declined for other reason'

    }

    return res.status(200).json(response)

}