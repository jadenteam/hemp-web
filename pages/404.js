
import { useRouter } from 'next/router'

import Layout from '../components/Layout'
import getMenuFeaturedProducts from '../repository/getMenuFeaturedProducts'
import { Button } from '../components/modules/Button'

export default function NotFoundPage({ menuFeaturedProducts }) {
	const router = useRouter()
	return <Layout title="404 Page Not Found | Hemp Forever" menuFeaturedProducts={menuFeaturedProducts}>
		<div className="flex flex-col justify-center items-center">
			<div className="text-6xl font-bold">
				404 Page Not Found
			</div>
			<div className="my-20">
				<Button variant="black" className="px-10 rounded-full" onClick={() => router.push('/')}>Back To Home</Button>
			</div>
		</div>
	</Layout>
}

export async function getStaticProps(context) {
	return {
		props: {
			menuFeaturedProducts: await getMenuFeaturedProducts()
		}
	}
}