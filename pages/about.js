import Layout from '../components/Layout'
import { H1, H2, H3, H4 } from '../components/modules/Headings'
import { ResponsiveImage, PlaceholderImage } from '../components/modules/Image'

import useModal from '../hooks/useModal'
import getWordPressData from '../repository/getWordPressData'
import React from 'react'
import getMenuFeaturedProducts from '../repository/getMenuFeaturedProducts'
import { TeamModal } from '../components/modules/TeamModal'

export default function AboutPage({ data: { title, body, image, intro, primary, values, team, seo }, menuFeaturedProducts }) {

	const { setModal, closeModal } = useModal()

	const openModal = info => {
		setModal({
			content: <TeamModal {...info} />,
			style: 'fullscreen'
		})
	}

	return (
		<Layout {...{seo}} title={title} menuFeaturedProducts={menuFeaturedProducts}>
			<section className="relative md:pt-vw-1 flow-root">
				<H1 className="md:absolute md:w-2/3 z-10">{title}</H1>
				<div dangerouslySetInnerHTML={{ __html: intro }} className="text-xl font-bold bottom-0 mt-5 md:text-vw-xl md:mt-0 md:absolute d:text-vw-2xl md:w-1/3" />
				{<ResponsiveImage objectFit="contain" image={image} alt="hero" className="mt-8 md:mt-vw-7 md:float-right md:w-3/5" />}
			</section>

			<section className="mt-8 md:flex md:mt-vw-6">
				<div className="md:w-1/2 md:pr-vw-3">
					{primary.text && <p dangerouslySetInnerHTML={{ __html: primary.text }} />}
					{<ResponsiveImage objectFit="contain" image={primary.image} alt="primary" className="mt-8 w-full md:mt-24" />}
				</div>
				<div className="md:w-1/2 md:pl-vw-6">
					{values.map((value, i) => <Value key={i} {...value} />)}					
				</div>
			</section>

			<section className="mt-10 md:mt-vw-8">
				<H2 className="mb-4 md:mb-0">{team.title}</H2>
				<p>{team.subtitle}</p>
				<div className="mt-6 md:flex md:mt-vw-3 md:-mx-vw-1">
					{team.members.map((member, i) => {

						const { name, image, role, bio } = member

						return (
							<div key={i} className="mb-7 md:mb-0 md:w-1/3 md:mx-vw-1 cursor-pointer" onClick={() => openModal({ name, role, image, bio })}>
								<ResponsiveImage objectFit="contain" image={image} alt={name} className="w-full" />
								<h2 className="mt-3 md:mt-vw-1.5 leading-12 font-serif font-bold text-2xl md:text-vw-2xl">{name}</h2>
								<p className="text-sm md:text-vw-sm">{role}</p>
							</div>
						)
					})}
				</div>
			</section>

			<section id="prestyled-about" className="mt-4 md:mb-0 md:mt-vw-6" dangerouslySetInnerHTML={{ __html: body }} />
		</Layout>
	)
}

export function Value({ title, description }) {
	return <section>
		<H3 className="mb-2 mt-10 md:mb-vw-1 md:mt-vw-7 md:w-3/4">{title}</H3>
		<div dangerouslySetInnerHTML={{ __html: description }} />
	</section>
}

export async function getStaticProps(context) {
    return {
        props: {
			data: await getWordPressData('about'),
			menuFeaturedProducts: await getMenuFeaturedProducts()
        }
    }
}