import { useState } from 'react'
import cn from 'classnames'

import Layout from '../components/Layout'
import { Button, IconButton } from '../components/modules/Button'
import { H1 } from '../components/modules/Headings'
import { LabelledField } from '../components/modules/Form'

import getWordPressData from '../repository/getWordPressData'
import sendMail from '../repository/sendMail'
import useBreakpoints from '../hooks/useBreakpoints'
import getMenuFeaturedProducts from '../repository/getMenuFeaturedProducts'

export default function ContactPage({ data: { title, contacts, form, seo }, menuFeaturedProducts }) {
	const fields = { name: '', email: '', subject: '', message: '' }
	const [formErrors, setFormErrors] = useState(fields)
	const [formStatus, setFormStatus] = useState({ status: null, message: '' })
	const [values, setValues] = useState(fields)
	const { isDesktop } = useBreakpoints()
	const bgImage = isDesktop ? { backgroundImage: 'url(/images/contact-bg.png)' } : {}

	function handleChange(e) {
		setValues(prevState => ({
			...prevState,
			[e.target.name]: e.target.value
		}))
	}

	async function handleSubmit(e) {
		e.preventDefault()

		// Reset form errors and status on submit
		setFormErrors({})
		setFormStatus('')

		let res;

		try {
			res = await sendMail(values);
		} catch (err) {
			if ("formErrors" in err.data) setFormErrors(err.data.formErrors);
			if ("errorMessage" in err.data) setFormStatus({ status: "error", message: err.data.errorMessage });
			return;
		}

		setFormStatus({ status: "success", message: "Successfully submitted form. We'll be in touch!" });
	}

	return (
		<Layout {...{ seo }} title={title} menuFeaturedProducts={menuFeaturedProducts}>
			<div className="relative md:mx-vw-8">
				<div className="hidden md:block absolute -top-vw-4 -right-vw-20 w-vw-80 h-vw-60 md:bg-right-top bg-contain bg-no-repeat -z-1" style={bgImage}>
					<img className="absolute top-vw-16 left-vw-43 w-vw-7 h-vw-7 z-20" src="/images/map-pin.svg" alt="" />
					<div className="absolute top-vw-18 left-vw-42.25 w-vw-8 h-vw-8 ">
						<img className="animate-radar 			w-vw-0.5 h-vw-0.5 absolute top-1/2 left-1/2 -translate-x-1/2 block" src="/images/map-ring.svg" />
						<img className="animate-radar wait-400 	w-vw-0.5 h-vw-0.5 absolute top-1/2 left-1/2 -translate-x-1/2 block" src="/images/map-ring.svg" />
						<img className="animate-radar wait-800 	w-vw-0.5 h-vw-0.5 absolute top-1/2 left-1/2 -translate-x-1/2 block" src="/images/map-ring.svg" />
					</div>
				</div>
				<H1 className="mb-4 md:mb-vw-4">{title}</H1>
				{contacts.map((contact, i) => <Contact key={i} {...contact} />)}

				<section className="bg-grey-100 border-t-8 border-primary py-5 px-5 mt-6 md:border-t-vw-10 md:py-vw-5 md:px-vw-7 md:mt-vw-6">
					<h3 className="uppercase font-bold text-xl md:text-vw-xl mb-4 md:mb-vw-1">{form.title}</h3>
					<p className="mb-8 md:mb-vw-3">{form.description}</p>
					<form onSubmit={handleSubmit} className="form__contact flex flex-col">
						<div className="flex md:space-x-vw-1 flex-col md:flex-row mb-3 md:mb-0">
							<LabelledField light error={formErrors.name} label="Name" type="text" value={values.name} onChange={handleChange} name="name" required className="md:w-1/2 mb-3 md:mb-vw-3" />
							<LabelledField light error={formErrors.email} label="Email" type="email" value={values.email} onChange={handleChange} name="email" required className="md:w-1/2 md:mx-vw-1 mb-3 md:mb-vw-3" />
						</div>
						<LabelledField light error={formErrors.subject} label="Subject" type="text" value={values.subject} onChange={handleChange} name="subject" required className="mb-3 md:mb-vw-3" />
						<LabelledField light error={formErrors.message} label="Message" type="textarea" value={values.message} onChange={handleChange} name="message" required className="mb-3 md:mb-vw-3" />
						<Button type="submit">Submit</Button>
						{formStatus.status && <span className={`mt-5 text-${formStatus.status}`}>{formStatus.message}</span>}
					</form>
				</section>
			</div>
		</Layout>
	)
}

export function Contact({ title, details, link }) {
	return <div className="mb-2 md:mb-vw-2">
		<div className="uppercase">{title}</div>
		<div className="flex items-center space-x-2 md:space-x-vw-1 mb-10 md:mb-vw-1">
			<span className="flex-grow md:flex-grow-0">{details}</span>
			{link && <IconButton href={link.url} target={link.target} icon="next" lightIcon />}
		</div>
	</div>
}

export async function getStaticProps(context) {
	return {
		props: {
			data: await getWordPressData('contact'),
			menuFeaturedProducts: await getMenuFeaturedProducts()
		}
	}
}