import React from 'react'

import Header from '../components/Header'
import Layout from '../components/Layout'
import ShopHero from '../components/modules/ShopHero'
import { H1 } from '../components/modules/Headings'
import Product from '../components/modules/Product'
import Promo from '../components/modules/Promo'
import { SubscribeMini } from '../components/modules/Subscribe'
import PromoLinks from '../components/modules/PromoLinks'
import ProductShowcase from '../components/modules/ProductShowcase'
import { FadeIn } from '../components/modules/Parallax'

import getProducts from '../repository/getProducts'
import getProductShowcase from '../repository/getProductShowcase'
import getWordPressData from '../repository/getWordPressData'
import getMenuFeaturedProducts from '../repository/getMenuFeaturedProducts'
import { chunk } from '../utility/array'
import { useBanner } from '../hooks/useBanner'

export default function HomeFood({ data, products, showcase, menuFeaturedProducts, banner }) {

    const [bannerActive, closeBanner] = useBanner()
    const productGroups = chunk(products.items, 3)

    return (
        <Layout
            seo={data.seo}
            noheader
            title="Hemp Food Products | Hemp Foods Australia | Hemp Forever"
            menuFeaturedProducts={menuFeaturedProducts}
        >
            <Header
                menuFeaturedProducts={menuFeaturedProducts}
                banner={banner}
                bannerActive={bannerActive}
                closeBanner={closeBanner}
            />
            <div className="relative h-vh-60 md:h-vw-65 mb-10 md:mb-vw-6 overflow-hidden mt-10 md:mt-0">
                <ShopHero
                    carousel={data.carousel}
                    banner={banner}
                    bannerActive={bannerActive}
                />
            </div>

            <div id="shop-home-content" className="relative -top-vw-10 md:px-vw-7">

                {!!productGroups.length && <div className="px-5 md:px-0 mt-vw-4">
                    <ProductGrid products={productGroups[0]} />
                </div>}

                <FadeIn className="px-5 md:px-0 mt-vw-8">
                    <H1 className="text-center leading-tight -mb-vw-1.5 md:text-vw-10xl">New Arrivals</H1>
                    <ProductShowcase products={showcase} hideRelated />
                </FadeIn>

                {productGroups.length > 1 && <div className="px-5 md:px-0 mt-vw-4">
                    <ProductGrid products={productGroups[1].slice(0, 2)} addSubscribe />
                </div>}

                {data.promos.map((promo, i) => <FadeIn key={i} className="mt-vw-10">
                    <Promo data={promo} reverse={i % 2} />
                </FadeIn>)}

                <FadeIn className="mx-5 md:mx-0 mt-20 md:mt-vw-10 border-t border-grey-300 pt-14 md:pt-vw-6">
                    <PromoLinks {...data.footer} />
                </FadeIn>
            </div>
        </Layout>
    )
}

/**
 * Display product grid 
 * @param {Array} products          Products to display
 * @param {Boolean} addSubscribe    If true, replace the last item in row with the subscribe form 
 */
export function ProductGrid({ products, addSubscribe }) {
    return <section>
        <ul className="grid md:grid-cols-3 gap-4 md:gap-vw-2">
            {products.map((product, i) => <FadeIn key={i}><Product {...{ product }} quickbuy large /></FadeIn>)}
            {addSubscribe && <FadeIn><li><SubscribeMini /></li></FadeIn>}
        </ul>
    </section>
}

export async function getStaticProps(context) {
    return {
        props: {
            data: await getWordPressData(`shop/food`),
            products: await getProducts({ filter: { filterBy: 'featured', count: 6 } }),
            showcase: await getProductShowcase(),
            menuFeaturedProducts: await getMenuFeaturedProducts(),
            banner: await getWordPressData(`shop/food/banner`)
        }
    }
}