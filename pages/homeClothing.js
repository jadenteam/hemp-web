import React from 'react'

import Header from '../components/Header'
import Layout from '../components/Layout'
import ShopHero from '../components/modules/ShopHero'
import { BigButton } from '../components/modules/Button'
import { H1 } from '../components/modules/Headings'
import Product from '../components/modules/Product'
import Promo from '../components/modules/Promo'
import { SubscribeMini } from '../components/modules/Subscribe'
import PromoLinks from '../components/modules/PromoLinks'
import ProductShowcase from '../components/modules/ProductShowcase'
import { FadeIn } from '../components/modules/Parallax'

import getProducts from '../repository/getProducts'
import getProductShowcase from '../repository/getProductShowcase'
import getWordPressData from '../repository/getWordPressData'
import getMenuFeaturedProducts from '../repository/getMenuFeaturedProducts'
import { useBanner } from '../hooks/useBanner'

export default function HomeClothing({ data, products, showcase, menuFeaturedProducts, banner }) {

    const [bannerActive, closeBanner] = useBanner()
    const { mens_button_sub, womens_button_sub } = data.audience;

    return (
        <Layout
            seo={data.seo}
            noheader
            title="Hemp Clothing | Hemp Clothing Australia | Hemp Forever"
            menuFeaturedProducts={menuFeaturedProducts}
        >
            <Header
                menuFeaturedProducts={menuFeaturedProducts}
                banner={banner}
                bannerActive={bannerActive}
                closeBanner={closeBanner}
            />
            <div className="relative h-vh-60 md:h-vw-65 mb-10 md:mb-vw-6 overflow-hidden mt-10 md:mt-0">
                <ShopHero
                    carousel={data.carousel}
                    banner={banner}
                    bannerActive={bannerActive}
                />
            </div>

            <div id="shop-home-content" className="relative -top-vw-10 md:px-vw-7">
                <FadeIn className="flex md:space-x-vw-2 flex-col md:flex-row px-5 md:px-0">
                    <BigButton href="/shop/women" passHref className="py-10 md:py-0 mb-5" title="Shop Women" text={womens_button_sub} />
                    <BigButton href="/shop/men" passHref className="py-10 md:py-0 mb-5" title="Shop Men" text={mens_button_sub} />
                </FadeIn>

                <FadeIn className="px-5 md:px-0 mt-vw-8">
                    <H1 className="text-center leading-tight -mb-vw-1.5 md:text-vw-10xl">New Arrivals</H1>
                    <ProductShowcase products={showcase} hideRelated />
                </FadeIn>

                <div className="px-5 md:px-0 mt-vw-4">
                    <ProductTitle>Featured styles Men</ProductTitle>
                    <ProductGrid products={products.mens.items} />
                </div>

                <div className="px-5 md:px-0 mt-vw-4">
                    <ProductTitle>Featured styles Women</ProductTitle>
                    <ProductGrid products={products.womens.items} />
                </div>

                {data.promos.map((promo, i) => <FadeIn key={i} className="mt-vw-10">
                    <Promo data={promo} reverse={i % 2} />
                </FadeIn>)}

                <FadeIn className="mx-5 md:mx-0 mt-20 md:mt-vw-10 border-t border-grey-300 pt-14 md:pt-vw-6">
                    <PromoLinks {...data.footer} />
                </FadeIn>
            </div>
        </Layout>
    )
}

export function ProductGrid({ products }) {
    return <section>
        <ul className="grid md:grid-cols-3 gap-4 md:gap-vw-2">
            {products.map((product, i) => {
                return <React.Fragment key={i}>
                    {i === 4 && <FadeIn><li><SubscribeMini /></li></FadeIn>}
                    <FadeIn>
                        <Product {...{ product }} quickbuy large />
                    </FadeIn>
                </React.Fragment>
            })}
        </ul>
    </section>
}

export function ProductTitle({ children }) {
    return <h2 className="pt-vw-2 pb-vw-4 uppercase text-center">{children}</h2>
}

export async function getStaticProps(context) {
    return {
        props: {
            data: await getWordPressData(`shop/clothing`),
            products: {
                mens: await getProducts({ category: 'men', sort: 'default', filter: { filterBy: 'featured', count: 3 } }),
                womens: await getProducts({ category: 'women', sort: 'default', filter: { filterBy: 'featured', count: 3 } }),
            },
            showcase: await getProductShowcase(),
            menuFeaturedProducts: await getMenuFeaturedProducts(),
            banner: await getWordPressData(`shop/clothing/banner`)
        }
    }
}