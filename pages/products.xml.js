import getProducts from '../repository/getProducts'
import { getProductURLByCategoryID } from '../repository/getStores'

const ProductFeed = () => {};

const clean = string => {

    // Cast to string
    string = string.toString()

    // Remove html (for short descriptions)
    string = string.replace(/<[^>]*>?/gm, '')

    // Encode special characters
    string = string.replace(/[\u00A0-\u9999<>\&]/g, i => '&#'+i.charCodeAt(0)+';')

    // Strip and return
    return string.trim()

}

const processCat = categories => {

    let string = ''

    categories.forEach((cat, i) => {
        string += `${cat.name} ${i !== categories.length -1 ? '> ' : ''}`
    })

    return clean(string)

}

const buildElements = (elements, type = 'base') => {

    if (type === 'base') {

        // Map base attributes
        return Object.keys(elements).map((key) => {

            let value = elements[key]

            if(!value) return ''

            switch(key) {
                case 'sku':
                    key = 'item_group_id'
                    value = clean(value)
                    break
                case 'price':
                    value = `${value.isDiscounted ? value.discounted.toFixed(2) : value.regular.toFixed(2)} ${value.currency}`
                    break
                case 'manufacturer':
                    key = 'brand'
                    value = value.label
                    break
                case 'categories':
                    key = 'product_type'
                    value = processCat(value)
                    break
                case 'stock_status':
                    value = `${value.toLowerCase()}`
                    key = 'availability'
                    break
                case 'description':
                    value = clean(value.html)
                    break
                default:
                    break
            }
            
            return `<g:${key}>${value}</g:${key}>`
        
        }).join('')

    } else if(type === 'standard_product') {
        
        // Map standard product attributes
        return Object.keys(elements).map((key) => {

            let value = elements[key]

            if(!value) return ''

            switch(key) {
                case 'name':
                    key = 'title'
                    value = clean(value)
                    break
                case 'image':
                    key = 'image_link'
                    value = value.url
                    break
                case 'media_gallery':
                    return value.map((image, index) => {
                        if(index === 0) return ''
                        const image_key = 'additional_image_link'
                        return `<g:${image_key}>${image.url}</g:${image_key}>`
                    }).join('')
                case 'url_key':
                    key = 'link'
                    value = `${process.env.SITE_URL}shop/product/${elements.url_key}` //getProductURLByCategoryID(value[0].id, elements.url_key)
                    break
                case 'stock_status':
                    value = `${value.toLowerCase()}`
                    key = 'availability'
                    break
                case 'shipping_weight':
                    value = `${value.toFixed(2)} kg`
                    break
                default:
                    break
            }
            
            return `<g:${key}>${value}</g:${key}>`
        
        }).join('')

    } else if(type === 'variant_product') {

        // Categories isn't an attribute, but is used to build the url so remove it from elements
        const categories = elements.categories
        delete elements.categories

        // Map variant product attributes
        return Object.keys(elements).map((key) => {

            let value = elements[key]

            if(!value) return ''

            switch(key) {
                case 'name':
                    key = 'title'
                    value = clean(value)
                    break
                case 'image':
                    key = 'image_link'
                    value = value.url
                    break
                case 'media_gallery':
                    return value.map((image, index) => {
                        if(index === 0) return ''
                        const image_key = 'additional_image_link'
                        return `<g:${image_key}>${image.url}</g:${image_key}>`
                    }).join('')
                case 'shipping_weight':
                    value = `${value.toFixed(2)} kg`
                    break
                case 'stock_status':
                    value = `${value.toLowerCase()}`
                    key = 'availability'
                    break
                case 'url_key':
                    value = `${process.env.SITE_URL}shop/product/${elements.url_key}` //getProductURLByCategoryID(categories[0].id, value)
                    key = 'link'
                    break
                default:
                    break
            }
            
            return `<g:${key}>${value}</g:${key}>`
        
        }).join('')

    } else if(type === 'variant_attributes') {

        // Map variant attribute attributes
        return elements.map((attribute) => {

            const { label, code } = attribute
            return `<g:${code}>${label}</g:${code}>`
        
        }).join('')

    }

}

const buildProduct = (p, variant = null) => {

    const { id, sku, gtin, name, description, categories, image, media_gallery, price, manufacturer, stock_status, shipping_weight, url_key } = p

    // build shared (base) google attributes 
    let item = `<item>` + buildElements({ gtin, sku, price, description, manufacturer, condition: 'new', categories })

    if(variant) {

        const { attributes, product } = variant

        product.url_key = url_key
        product.image = image
        
        // build google attributes from variant product info
        item += buildElements({ ...product, categories }, 'variant_product')

        // build google attributes from variant attribute info
        item += buildElements(attributes, 'variant_attributes') 
        
        
    } else {

        // build google attributes from variant attribute info
        item += buildElements({ id, name, url_key, stock_status, shipping_weight, image, media_gallery }, 'standard_product') 

    }
    
    item += `</item>`
    return item

}

const buildItem = p => {

    const { __typename } = p

    if(__typename === "SimpleProduct") {

        return buildProduct(p)

    } else {

        const { id, sku, gtin, name, description, categories, image, media_gallery, price, manufacturer, stock_status, weight, url_key, variants } = p

        return variants.map(variant => {

            return buildProduct({ id, sku, gtin, name, description, categories, image, media_gallery, price, manufacturer, stock_status, weight, url_key }, variant)

        }).join('')

        
    }

}

export const getServerSideProps = async ({ res }) => {

    const products = await getProducts({}, 1, 999999, false)
    const { items } = products

    res.setHeader("Content-Type", "text/xml");
    
    const feed = `<?xml version="1.0"?>
        <rss xmlns:g="http://base.google.com/ns/1.0" version="2.0">
            <channel>
                ${items.map(item => buildItem(item)).join('')}
            </channel>
        </rss>
    `;

    res.write(feed);
    res.end();

    return {
        props: {},
    };

};

export default ProductFeed;