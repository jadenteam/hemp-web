import React, { useState } from 'react'

import Layout from '../../../components/Layout'
import { H1 } from '../../../components/modules/Headings'
import PostItem from '../../../components/modules/PostItem'
import { LiveButton } from '../../../components/modules/Button'
import { chunk } from '../../../utility/array'

import getWordPressData from '../../../repository/getWordPressData'
import getMenuFeaturedProducts from '../../../repository/getMenuFeaturedProducts'

/**
 * Blog template - limit by tag
 */
export default function TagPostListPage({ data: { title, description, posts, total_posts, seo }, menuFeaturedProducts }) {
	const pageSize = 10
	const [activePages, setActivePages] = useState(0)

	// chunk the posts into pages
	const pages = chunk(posts, pageSize)

	return (
		<Layout {...{seo}} title={`${title} blog posts`} menuFeaturedProducts={menuFeaturedProducts}>
			<H1>{title}</H1>
			<p>{description}</p>

			<article className="flex flex-wrap mt-3 -mx-1 md:mt-vw-3 md:-mx-vw-1">
				{pages.map((page, pageCounter) => 
					pageCounter <= activePages && page.map((post, postCounter) => {
						return (
							<React.Fragment key={postCounter}>
								<PostItem {...post} className="w-full px-1 pb-10 md:w-1/2 md:px-vw-1 md:pb-vw-5" />
							</React.Fragment>
						)
					})
				)}
			</article>

			<div className="text-center">
				{((activePages+1) * pageSize < total_posts) && <LiveButton onClick={() => setActivePages(activePages + 1)}>Load More</LiveButton>}
			</div>
		</Layout>
	)
}

export async function getStaticPaths() {

	try {

		const res = await getWordPressData('static/tag') // get all possible tag slugs
		return {
			paths: res.map(tag => ({ params: { tag } })),
			fallback: false
		}

	} catch(err) {
		return { paths: [], fallback: false }
	}

}

export async function getStaticProps(context) {
	
	let data = {}

	try {
		data = await getWordPressData('blog/tag', { 
			tag: context.params.tag
		})
	} catch(err) {
		data.error = err
	}

    return {
       	props: {
			data,
			menuFeaturedProducts: await getMenuFeaturedProducts()
		}
    }

}