import React, { useState } from 'react'
import cn from 'classnames'
import parse from 'html-react-parser'

import Layout from '../../components/Layout'
import { H1 } from '../../components/modules/Headings'
import PostItem from '../../components/modules/PostItem'
import { LiveButton } from '../../components/modules/Button'
import Subscribe from '../../components/modules/Subscribe'
import { chunk } from '../../utility/array'

import useBreakpoints from '../../hooks/useBreakpoints'
import getWordPressData from '../../repository/getWordPressData'
import getMenuFeaturedProducts from '../../repository/getMenuFeaturedProducts'


/**
 * Blog template 
 */
export default function BlogPage({ data: { title, categories, posts, total_posts, subscribe, seo }, menuFeaturedProducts }) {
	const pageSize = 10
	const [activePages, setActivePages] = useState(0)
	const [filter, setFilter] = useState(0)

	// const filteredCategories = categories.filter((cat, i) => )
	const filteredPosts = posts ? posts.filter((post, i) => filter === 0 ? true : post.categories.find(cat => cat.id === filter)) : []

	// chunk the posts into pages
	const pages = chunk(filteredPosts, pageSize)

	return (
		<Layout {...{seo}} title={title} menuFeaturedProducts={menuFeaturedProducts}>
			<H1>{title}</H1>
			{<Categories {...{categories}} active={filter} setActive={setFilter} />}

			<article className="flex flex-wrap mt-3 -mx-1 md:mt-vw-3 md:-mx-vw-1">
				{pages.map((page, pageCounter) => 
					pageCounter <= activePages && page.map((post, postCounter) => {
						return (
							<React.Fragment key={postCounter}>
								{/* show subscribe component on 1st page only after 4th post */}
								{(pageCounter === 0 && postCounter === 4) && <Subscribe noBg data={subscribe} className="mx-1 mb-5 md:mx-vw-1 md:mb-vw-5" />}
								<PostItem {...post} className="w-full px-1 pb-10 md:w-1/2 md:px-vw-1 md:pb-vw-5" />
							</React.Fragment>
						)
					})
				)}
			</article>

			<div className="text-center">
				{((activePages+1) * pageSize < total_posts) && <LiveButton onClick={() => setActivePages(activePages + 1)}>Load More</LiveButton>}
			</div>
		</Layout>
	)
}

/**
 * Display category selection component. Different depending on window width 
 */
export function Categories({ categories, active, setActive }) {
	const { isMobile, isDesktop } = useBreakpoints()

	return <div className="mt-4 md:mt-vw-1">
		{isDesktop && <ul className="flex">
			<CategoryTitle as="li" />
			<li className="flex flex-wrap">
				<CategoryItem id={0} name="All" active={active === 0} callback={() => setActive(0)} />
				{categories && categories.map((cat, i) => <CategoryItem key={i} {...cat} active={cat.id === active} callback={() => setActive(cat.id)} />)}
			</li>
		</ul>}
		{isMobile && <div>
			<CategoryTitle as="span" />
			{categories && <Dropdown name="category" options={categories} onChange={e => setActive(e.target.value)} />}
		</div>}
	</div>
}

/**
 * Display category select for mobile 
 */
export function Dropdown({ name, options, onChange }) {
	return <select name={name} onChange={onChange}>
		{options.map((opt, i) => <option key={i} value={opt.id}>{opt.name}</option>)}
	</select>
}

/**
 * Display category title inside customisable element. eg. span or li 
 */
export function CategoryTitle({ as }) {
	const Element = as // render element based on passed props
	return <Element className="mr-1 text-sm uppercase text-grey-400 md:mr-vw-1 md:text-vw-sm">Categories:</Element>
}

/**
 * Display single category list item with link 
 */
export function CategoryItem({ id, name, active, callback }) {
	return <div className="text-sm md:text-vw-sm">
		<a onClick={callback} className={cn('px-1 md:px-vw-1 cursor-pointer',
			active && 'underline font-bold'
		)}>
			{parse(name)}
		</a>
	</div>
}

export async function getStaticProps(context) {
    return {
        props: {
			data: await getWordPressData('blog'),
			menuFeaturedProducts: await getMenuFeaturedProducts()
        }
    }
}