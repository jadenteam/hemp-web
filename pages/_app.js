import '../styles/globals.css'
import { useEffect } from 'react'
import TagManager from 'react-gtm-module'

import { ModalProvider } from '../context/modal'
import { CartProvider } from '../context/cart'
import { MenuProvider } from '../context/menu'

export default function App({ Component, pageProps }) {

  useEffect(() => {
    TagManager.initialize({ gtmId: 'GTM-TKDS6HL' })
  }, [])

  return <CartProvider>
    <MenuProvider>
      <ModalProvider>
        <Component {...pageProps} />
      </ModalProvider>
    </MenuProvider>
  </CartProvider>
}