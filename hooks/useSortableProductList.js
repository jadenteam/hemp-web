import { useState } from 'react'

export const useSortableProductList = (products) => {
    const [filteredProducts, setFilteredProducts] = useState(products)

    // Repitition by calling setSortedByLabel function but much better fix as we are tying sort values together with respective sort functions
    const _products = [...products]
    const sorters = [
        {
            value: 'default',
            label: 'Default',
            sort: (sortByType) => {
                setFilteredProducts(products)
                setSortedByLabel(sortByType)
            }
        },
        {
            value: 'alpha',
            label: 'A - Z',
            sort: (sortByType) => {
                setFilteredProducts(_products.sort((a, b) => a.name.localeCompare(b.name)))
                setSortedByLabel(sortByType)
            }
        },
        {
            value: 'alpha-reverse',
            label: 'Z - A',
            sort: (sortByType) => {
                setFilteredProducts(_products.sort((a, b) => b.name.localeCompare(a.name)))
                setSortedByLabel(sortByType)
            }
        },
        {
            value: 'price-asc',
            label: 'Lowest price',
            sort: (sortByType) => {
                setFilteredProducts(_products.sort((a, b) => a.price.regular < b.price.regular ? -1 : a.price.regular > b.price.regular ? 1 : 0))
                setSortedByLabel(sortByType)
            }
        },
        {
            value: 'price-desc',
            label: 'Highest price',
            sort: (sortByType) => {
                setFilteredProducts(_products.sort((a, b) => b.price.regular < a.price.regular ? -1 : b.price.regular > a.price.regular ? 1 : 0))
                setSortedByLabel(sortByType)
            }
        },
    ]

    // Get the sort item in our 'lookup' by the sortItemValue passed in
    const getSortItem = (sortItemValue) => sorters.find(sortItem => sortItem.value === sortItemValue)

    // Grab the label from our sort item
    const getSortedByLabel = (sortItemValue) => getSortItem(sortItemValue).label

    // Call the sort method on our sort item
    const handleChangeSort = (sortItemValue) => getSortItem(sortItemValue).sort(sortItemValue)

    const [sortedByLabel, setSortedByLabel] = useState(getSortedByLabel(sorters[0].value))

    return [
        filteredProducts,
        sorters,
        sortedByLabel,
        handleChangeSort
    ]
}