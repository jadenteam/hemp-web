
import { useCallback, useState } from 'react'
import cn from 'classnames'

/**
 * Provide state and classes used to create and animate carousel components
 * @param {Array}   items               The array of items to use in the carousel 
 * @param {String}  animationPrefix     Tailwind animation class prefix. Used to allow multiple different animation groups on the same hook
 * 
 * @example
 * const { carouselIndex, carouselActive, goTo, goToNext, goToPrev, animateSlide } = useCarousel(items, 'hero')
 */
export const useCarousel = (items, animationPrefix, cycle = true) => {
    const [carousel, setCarousel] = useState({ index: 0, direction: 'next' })

    const index = carousel.index

    /** Index booleans */
    const isStart = !!(carousel.index === 0)
    const isEnd = !!(carousel.index === items.length - 1)

    /** State helper methods */
    const getBeforePrevItemIndex = () => carousel.index > 1 ? carousel.index - 2 : carousel.index > 0 ? items.length - 1: items.length - 2
    const getPrevItemIndex = cycle || !isStart ? () => carousel.index > 0 ? carousel.index - 1 : items.length - 1 : () => {}
    const getNextItemIndex = cycle || !isEnd ? () => carousel.index < items.length - 1 ? carousel.index + 1 : 0 : () => {}

    /** Navigation methods */
    const goToNext = cycle || !isEnd ? () => setCarousel({ index: getNextItemIndex(), direction: 'next' }) : () => {}
    const goToPrev = cycle || !isStart ? () => setCarousel({ index: getPrevItemIndex(), direction: 'prev' }) : () => {}
    const goTo = (index) => setCarousel(prevState => {
        if(index === prevState.index) return prevState // do nothing
        return { 
            index: index, 
            direction: index > prevState.index ? 'next' : 'prev'
        }
    })

    /**
     * Animation classes for a sliding effect
     * @param {String}  defaultClass    Additional classes applied to the element
     * @param {Integer} counter         The index of the active slide in the set
     * 
     * @example 
     * <div className={animateSlide('custom-class', counter)}></div>
     */
    const animateSlide = (defaultClass, counter) => {
        if(items.length <= 1) {
            return defaultClass
        }

        return cn(
            defaultClass,
            'opacity-0 -z-1',
            counter === carousel.index && `animate-${animationPrefix}-${carousel.direction}-in`,
            (carousel.direction === 'next' && counter === getPrevItemIndex()) && `animate-${animationPrefix}-${carousel.direction}-out`,
            (carousel.direction === 'prev' && counter === getNextItemIndex()) && `animate-${animationPrefix}-${carousel.direction}-out`
        )
    }

    /**
     * Animation classes for a text flick-up effect
     * @param {String}  defaultClass    Additional classes applied to the element
     * @param {Integer} counter         The index of the active slide in the set
     * 
     * @example 
     * <div className={animateFlick('custom-class', counter)}></div>
     */
    const animateFlick = (defaultClass, counter) => {
        if(items.length <= 1) {
            return defaultClass
        }

        return cn(
            defaultClass,
            'relative opacity-0 z-10',
            counter === carousel.index && `animate-${animationPrefix}-text-in`,
            (carousel.direction === 'next' && counter === getPrevItemIndex()) && `animate-${animationPrefix}-text-out`,
            (carousel.direction === 'prev' && counter === getNextItemIndex()) && `animate-${animationPrefix}-text-out`
        )
    }

    /**
     * Animation classes for a cycling foreground/background effect
     * @param {String}  defaultClass    Additional classes applied to the element
     * @param {Integer} counter         The index of the active slide in the set
     * 
     * @example 
     * <div className={animateCycle('custom-class', counter)}></div>
     */
    const animateCycle = (defaultClass, counter) => {
        if(items.length <= 1) {
            return defaultClass
        }

        return cn(
            defaultClass,
            'opacity-0 -z-1',
            counter === carousel.index && `animate-${animationPrefix}-${carousel.direction}-in`,
            (carousel.direction === 'next' && counter === getNextItemIndex()) && `animate-${animationPrefix}-upcoming`,
            (carousel.direction === 'prev' && counter === getPrevItemIndex()) && `animate-${animationPrefix}-leaving`,
            (carousel.direction === 'next' && counter === getPrevItemIndex()) && `animate-${animationPrefix}-${carousel.direction}-out`,
            (carousel.direction === 'prev' && counter === getNextItemIndex()) && `animate-${animationPrefix}-${carousel.direction}-out`
        )
    }

    /**
     * Animation classes for a swiping left to right effect
     * @param {String}  defaultClass    Additional classes applied to the element
     * @param {Integer} counter         The index of the active slide in the set
     * 
     * @example 
     * <div className={animateCycle('custom-class', counter)}></div>
     */
     const animateSwipe = (defaultClass, counter) => {
        if(items.length <= 1) {
            return defaultClass
        }

        const baseClasses = 'transition-all duration-500 transform'
    
        return cn(
            defaultClass,
            baseClasses,
            counter <= getPrevItemIndex() && `-translate-x-vw-40 opacity-0`,
            counter === carousel.index && `translate-x-vw-0 opacity-100`,
            counter === getNextItemIndex() && `translate-x-vw-35 opacity-100`,
            counter > getNextItemIndex() && `translate-x-vw-60 opacity-100`,
        )
    }

    return { 
        carouselIndex: carousel.index, 
        carouselActive: items && items.length > 1, 
        goTo,
        isStart,
        isEnd,
        goToNext, 
        goToPrev, 
        animateSlide,
        animateCycle,
        animateFlick,
        animateSwipe
    }
}
export default useCarousel