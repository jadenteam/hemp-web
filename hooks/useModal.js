import { useContext } from 'react'

import { ModalContext } from '../context/modal'

/**
 * Modal hook to access and interact with the ModalProvider
 * 
 * @example
 * const { setModal, closeModal } = useModal()
 * 
 * <button onClick={setModal({ content: <Content />, style: 'drawer|popup' })}>Open Modal</button>
 */
const useModal = () => {
	const context = useContext(ModalContext)

	if(context === undefined) {
		throw new Error('useModal must be used within a ModalProvider')
	}

	return context
}
export default useModal