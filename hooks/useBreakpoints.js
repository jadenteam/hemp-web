import { useState, useEffect } from 'react'
import resolveConfig from 'tailwindcss/resolveConfig'
import tailwindConfig from '../tailwind.config.js'

import objectMap from '../utility/objectMap'

/**
 * Provide a standardised way to access tailwind breakpoints in React 
 */
export const useBreakpoints = () => {
	const [windowWidth, setWindowWidth] = useState(undefined)

	// get the tailwind breakpoints from config 
	// map breakpoint values from strings to integers. eg. "640px" becomes 640
	const breakpoints = objectMap(resolveConfig(tailwindConfig).theme.screens, v => parseInt(v, 10))

	// set and update window size 
	useEffect(() => {
		window.addEventListener('resize', handleResize);

		// Call handler right away so state gets updated with initial window size
		handleResize();

		return () => window.removeEventListener('resize', handleResize);
	}, []);

	// Set window width to state
	function handleResize() {
		setWindowWidth(window.innerWidth)
	}

	return {
		// individual breakpoints
		isXs: windowWidth < breakpoints.sm,
		isSm: windowWidth >= breakpoints.sm && windowWidth < breakpoints.md,
		isMd: windowWidth >= breakpoints.md && windowWidth < breakpoints.lg,
		isLg: windowWidth >= breakpoints.lg && windowWidth < breakpoints.xl,
		isXl: windowWidth >= breakpoints.xl && windowWidth < breakpoints['2xl'],
		is2Xl: windowWidth >= breakpoints['2xl'],

		// grouped breakpoints
		isMobile: windowWidth < breakpoints.md,
		isDesktop: windowWidth >= breakpoints.md
	}
}
export default useBreakpoints