
import { useCallback, useEffect, useState } from 'react'
import cn from 'classnames'
import getProductConfigurableOptionsMeta from '../repository/getProductConfigurableOptionsMeta'
import { getInitialisedProductOption } from '../repository/productHelpers';
import { getSalableQuantity } from '../repository/getSalableQuantity';

/**
 * Provide state and for individual cart items
 * @param {Array}   options             The product options array (undefined if a non-configurable product)
 * @param {Object}  initialOptions      Prefills the selectedOptions return value 
 * @param {Object}  initialQuantity     Prefills the quantity return value 
 * 
 * @example
 * const { quantity, setQuantity, selectedOptions, handleOptionChange } = useProductOptions(slug, options)
 */
export const useProductOptions = (slug, options, initialOptions, initialQuantity=1, product) => {
	const [quantity, setQuantity] = useState(initialQuantity)
    const [meta, setMeta] = useState({})
    const [selectedOptions, setSelectedOptions] = useState(false)

    const [hasTriedFreshMetaFetch, setHasTriedFreshMetaFetch] = useState(false)

    useEffect(async () => {
        if(initialOptions) {
            setSelectedOptions(initialOptions)
            await fetchMeta(slug, initialOptions) // refresh the meta for the selected option combination
        } else if(options) {
            const reduced = options.reduce((acc, item) => (acc[item.attribute_code] = item.values[0].uid, acc), {})
            setSelectedOptions(reduced)
            await fetchMeta(slug, reduced) // refresh the meta for the selected option combination
        }
    }, [options, initialOptions])

    useEffect(async () => {
        // There may be a difference between the meta we currently are displaying and what is actually valid due to it being created on build.
        // Fetch fresh meta in the background.
        if (!hasTriedFreshMetaFetch && product) {
            const initialisedOption = await getInitialisedProductOption(slug, product)
            // If we get nothing back then dont continue
            if (!initialisedOption) return null
            setSelectedOptions(initialisedOption)
            await fetchMeta(slug, initialisedOption) // refresh the meta for the selected option combination
            setHasTriedFreshMetaFetch(true)
        }
    }, [meta])

    async function handleOptionChange(e) {
		e.preventDefault()
        const newState = {
			...selectedOptions,
			[e.target.name]: e.target.value
        }
    
		setSelectedOptions(newState)
        await fetchMeta(slug, newState)
    }

    // fetch and set the meta info for the current specific configurable options combination 
    async function fetchMeta(slug, uids) {
        try {
            const response = await getProductConfigurableOptionsMeta(slug, uids)

            // Need to grab the salable quantity for this product variant
            const salableQuantity = await getSalableQuantity(response.sku)

            setMeta({
                ...response,
                salableQuantity
            })
        } catch(err) {
            // don't show an error
            console.error('Could not retrieve product meta', err)
        }
    }

    return { 
        isConfigurable : !!options,
        meta,
        quantity,
        setQuantity,
        selectedOptions,
        handleOptionChange
    }
}
export default useProductOptions