import { useEffect, useState } from 'react'

import { getAudienceAssociationByAudienceName } from '../repository/getAudienceAssociations'
import { useMenuContext } from '../context/menu'

export const useFilteredSubMenu = (category, level, parentCategory, parentCategoryKey, products) => {
    const [subMenu, setSubMenu] = useState([])
    const { submenu } = useMenuContext()

    function getMaybeAudienceAssociation() {
        // Need to check both category and parent category for some audienceAssociation
        return getAudienceAssociationByAudienceName(category) || getAudienceAssociationByAudienceName(parentCategory)
    }

    useEffect(() => {
        const audienceAssociation = getMaybeAudienceAssociation(category)
        const filteredSubMenu = submenu(category, level, parentCategory).filter(subMenuItem => {
            // Don't do any filtering if we don't have a parentCategoryKey.
            if (!parentCategoryKey) return true
            return products.some(product => {
                return product.categories.some(productCategory => {
                    return audienceAssociation
                        // If we currently have some audienceAssociation with our subMenu
                        ? (productCategory.id === subMenuItem.id || subMenuItem.id === 0) && audienceAssociation.includes(Number(product.audience.value))
                        // Otherwise just filter without considering any audienceAssociations
                        : productCategory.id === subMenuItem.id || subMenuItem.id === 0
                })
            })
        })
        setSubMenu(filteredSubMenu)

    }, [submenu])

    return subMenu
}