import { useState, useEffect } from 'react'
import Cookies from 'js-cookie'

export const useBanner = () => {

    const cookieName = 'hemp-banner'
    const [cookie, setCookie] = useState('')

    useEffect(() => {

        const initialCookie = Cookies.get(cookieName)

        if(initialCookie) {
            setCookie(initialCookie)
        } else {
            Cookies.set(cookieName, 'true', { 'path' : '/', 'sameSite': 'strict' })
            setCookie('true')
        }

    }, [])

    const closeBanner = () => {
        
        Cookies.set(cookieName, 'false', { 'path' : '/', 'sameSite': 'strict' })
        setCookie('false')

    }

    return [cookie === 'true', closeBanner]

}