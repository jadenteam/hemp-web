import { useState, useEffect } from 'react'
// import { useViewportScroll } from 'framer-motion'

const useScrollDetect = (y) => {
	const [triggered, setTriggered] = useState(null)
	const [current, setCurrent] = useState(null)

	// // get scroll position and set animation trigger state 
	// const { scrollY } = useViewportScroll() 
	// useEffect(() => {
	// 	scrollY.onChange(value => {
	// 		setTriggered(value >= y)
	// 		setCurrent(value)			
	// 	})
	// }, [scrollY])

	// const toggleTrigger = () => {
	// 	let scrollY = window.scrollY
	// 	if(scrollY >= y) {
	// 		setTriggered(true)
	// 	} else {
	// 		setTriggered(false)
	// 	}
	// 	setCurrent(scrollY)
	// }

	// useEffect(() => {
	// 	window.addEventListener('scroll', toggleTrigger)
	// 	return () => {
	// 		window.removeEventListener('scroll', toggleTrigger)
	// 	}
	// })    

	return {
		current,
		triggered
	}
}
export default useScrollDetect