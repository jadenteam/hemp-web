import { useEffect } from 'react'

/**
 * Prevent page scrolling when passed param is true. 
 * Useful for modals/menus or in conjunction with useOutsideClickTrigger
 * 
 * @param {boolean} preventScroll 	Trigger to prevent scrolling
 * 
 * @example 
 * const [open, setOpen] = useState(false)
 * usePreventScroll(open)
 * 
 * <Component open={open} />
 */
export const usePreventScroll = (preventScroll) => {
	
	useEffect(() => {
		const scrollbarWidth = (window.innerWidth - document.body.clientWidth) + 'px'
		document.body.style.overflow = preventScroll ? 'hidden' : 'unset'
		document.body.style.marginRight = preventScroll ? scrollbarWidth : '0'
	}, [preventScroll]);
}
export default usePreventScroll