import { useEffect } from 'react'

/**
 * Trigger the passed callback function when the mouse is clicked outside the bounds of the referenced element 
 * Does NOT prevent ability to click other elements 
 * 
 * 	@example 
 * 	const [open, setOpen] = useState(false)
 * 	const componentRef = useRef(null)
 * 	useOutsideClickTrigger(componentRef, () => setOpen(false));
 *
 * 	<Component ref={componentRef} open={open} />
 */
export const useOutsideClickTrigger = (ref, callback) => {
	useEffect(() => {
		// trigger callback on click if outside the ref 
		function handleClickOutside(e) {
			if(ref.current && !ref.current.contains(e.target)) {
				callback()
			}
		}

		// add listeners
		document.addEventListener('mousedown', handleClickOutside)
		document.addEventListener('touchstart', handleClickOutside)

		return () => {
			document.removeEventListener('mousedown', handleClickOutside)
			document.removeEventListener('touchstart', handleClickOutside)
		}
	}, [ref]);
}
export default useOutsideClickTrigger