import { useState, useEffect } from 'react'

import TagManager from 'react-gtm-module'

export function useDataLayer(initDataLayer = null) {

    const [dl, setDl] = useState()

    useEffect(() => {
        if(initDataLayer !== null) {
            setDl(initDataLayer)
            TagManager.dataLayer(initDataLayer)
        }
    }, [])

    const changeDataLayer = newDataLayer => {

        setDl(newDataLayer)
        TagManager.dataLayer({ 
            dataLayer: {
                ecommerce: null
            }
        })
        TagManager.dataLayer(newDataLayer)
        
    }

    return [changeDataLayer]
}