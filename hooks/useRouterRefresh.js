import { useCallback } from 'react'
import { useRouter } from 'next/router'

/**
 * Refresh data on the NextJS page without navigating away
 * Useful for refetching data after an operation eg. adding an item to the cart 
 */
export default function useRouterRefresh() {
	const router = useRouter()
	const refresh = useCallback(() => router.replace(router.asPath), [router.asPath])
	return refresh
}