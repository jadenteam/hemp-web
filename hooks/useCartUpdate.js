
import { useState } from 'react'
import { useCartContext } from '../context/cart'
import { getProductAttributeInCart, updateInCart, removeFromCart } from '../repository/getCart'
import { useDataLayer } from './useDataLayer'
import { stringCurrency } from '../utility/format'

/**
 * Provide loading state and methods for updating items in the cart
 * 
 * @example
 * const {loading, updateCartItem, removeCartItem} = useCartUpdate()
 */
export const useCartUpdate = () => {
	const {cart, setCart} = useCartContext()
	const [loading, setLoading] = useState(false)
	const [changeDataLayer] = useDataLayer()

	// update the items in the cart
	async function updateCartItem(cartItemID, product, key, newValue) {
		setLoading(true)

		// get the quantity/size values to add a new product
		const quantity 	= key === 'quantity' ? newValue : getProductAttributeInCart(cart, product.id, 'quantity')
		const options 	= key !== 'quantity' ? { [key]: newValue } : getProductAttributeInCart(cart, product.id, 'options')

		// update item in cart
		const newCart = await updateInCart(cart.id, cartItemID, product.sku, quantity, options)
		setCart(newCart)
		setLoading(false)
	}

	// remove item from cart
	async function removeCartItem(cartItemID, product = null) {
		setLoading(true)
		const newCart = await removeFromCart(cart.id, cartItemID)
		if(product) {
			try {
				const { sku, name, manufacturer, price } = product
				changeDataLayer({
					dataLayer: {
						event: "remove_from_cart",
						ecommerce: {
							items: [{
								item_id: sku,
								item_name: name,
								item_brand: manufacturer.label,
								affiliation: process.env.SITE_KEY,
								price: stringCurrency(price.regular)
							}]
						}
					}
				})
			} catch(err) {
				// Failed to process event - ignore
			}
		}

		setCart(newCart)
		setLoading(false)
	}

    return { 
		loading, 
		setLoading, 
		updateCartItem, 
		removeCartItem
    }
}
export default useCartUpdate