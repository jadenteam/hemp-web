import { useState, useRef, useEffect } from 'react'

/**
 * Get the dimensions of a variable height element 
 * 
 * @example
 * const { ref, height, offsetTop, offsetBottom } = useElementDimensions()
 * 
 * <div ref={ref}>Element with a variable height</div>
 */
const useElementDimensions = () => {
	const ref = useRef(null)
	const [height, setHeight] = useState(0)
	const [offsetTop, setOffsetTop] = useState(0)

	useEffect(() => {
		if(ref.current){
			setHeight(ref.current.offsetHeight)
			setOffsetTop(ref.current.offsetTop)
		}
	}, [ref])

	return {
		ref,
		height,
		offsetTop,
		offsetBottom: offsetTop + height 
	}
}
export default useElementDimensions