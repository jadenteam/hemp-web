module.exports = {
    env: {
        // env values
        SITE_KEY: process.env.SITE_KEY,
        SHOP_CATEGORY_ID: process.env.SHOP_CATEGORY_ID,
        DARK_MODE: !!process.env.DARK_MODE, // parse as boolean
        WORDPRESS_API: process.env.WORDPRESS_API,
        MAGENTO_API: process.env.MAGENTO_API,
        MAGENTO_REST_API: process.env.MAGENTO_REST_API,
        MAGENTO_REST_FRONTEND_TOKEN: process.env.MAGENTO_REST_FRONTEND_TOKEN,
        MAGENTO_ADMIN_TOKEN: process.env.MAGENTO_ADMIN_TOKEN,
        SITE_URL: process.env.SITE_URL,
        SECUREPAY_AUTHORISE_ENDPOINT: process.env.SECUREPAY_AUTHORISE_ENDPOINT,

        // derived helper values
        IS_MAIN: process.env.SITE_KEY === 'main',
        IS_SHOP: ['bodycare', 'clothing', 'food', 'homeware', 'growhouse', 'greenlight'].includes(process.env.SITE_KEY),
    },
    images: {
        domains: ['139.99.188.85', 'cms.hempforever.store', 'shop.hempforever.store', 'magento.hempforever.store'], // TODO: remove after dev 
    },
    async rewrites() {
        // "block" main sites from being active on the shops 
        // TODO: make this work or figure out how to ignore certain files in pipeline during deployment 
        const shopBlocks = [
            { source: '/about', destination: '/404' },
            { source: '/blog', destination: '/404' },
            { source: '/contact', destination: '/404' },
            { source: '/post', destination: '/404' },
        ]

        // load different homepages depending on environment 
        switch (process.env.SITE_KEY) {
            case 'bodycare':
                return shopBlocks.concat([
                    { source: '/', destination: '/homeBodycare' }
                ])
            case 'clothing':
                return shopBlocks.concat([
                    { source: '/', destination: '/homeClothing' }
                ])
            case 'food':
                return shopBlocks.concat([
                    { source: '/', destination: '/homeFood' }
                ])
            case 'homeware':
                return shopBlocks.concat([
                    { source: '/', destination: '/homeHomeware' }
                ])
            case 'growhouse':
                return shopBlocks.concat([
                    { source: '/', destination: '/homeGrowhouse' }
                ])
            case 'greenlight':
                return shopBlocks.concat([
                    { source: '/', destination: '/homeGreenlight' }
                ])
            case 'main':
            default:
                return [
                    { source: '/', destination: '/homeMain' }
                ]
        }
    },

    async redirects() {
        return [
            {
                source: '/collections/accessories-gga',
                destination: 'https://hempforever.store/',
                statusCode: 301,
            },
            {
                source: '/collections/hair',
                destination: 'https://bodycare.hempforever.store/shop/women/hair',
                statusCode: 301,
            },
            {
                source: '/collections/hemp-clothing',
                destination: 'https://clothing.hempforever.store/',
                statusCode: 301,
            },
            {
                source: '/collections/hemp-food',
                destination: 'https://food.hempforever.store/',
                statusCode: 301,
            },
            {
                source: '/collections/hemp-products',
                destination: 'https://lifestyle.hempforever.store/',
                statusCode: 301,
            },
            {
                source: '/collections/incense',
                destination: 'https://lifestyle.hempforever.store/shop/fragrance-incense',
                statusCode: 301,
            },
            {
                source: '/collections/kitchen/kitchen',
                destination: 'https://food.hempforever.store/',
                statusCode: 301,
            },
            {
                source: '/collections/kitchen/kitchen',
                destination: 'https://food.hempforever.store/',
                statusCode: 301,
            },
            {
                source: '/collections/mens-clothing',
                destination: 'https://clothing.hempforever.store/shop/men',
                statusCode: 301,
            },
            {
                source: '/collections/nutrients-additives-and-pesticides',
                destination: 'https://hempforever.store/',
                statusCode: 301,
            },
            {
                source: '/collections/womens-clothing',
                destination: 'https://clothing.hempforever.store/shop/women',
                statusCode: 301,
            },
            {
                source: '/pages/about-us',
                destination: 'https://hempforever.store/about',
                statusCode: 301,
            },
            {
                source: '/pages/bubble-bags-retailers',
                destination: 'https://hempforever.store/',
                statusCode: 301,
            },
            {
                source: '/pages/faqs',
                destination: 'https://hempforever.store/faqs',
                statusCode: 301,
            },
        ]
    },
}