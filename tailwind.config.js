module.exports = {
	// mode: 'jit', // can't enable JIT mode due to breaking carousel hook classes. Would need to be updated to use. 
	purge: {
		content: ['./pages/**/*.js', './components/**/*.js', './hooks/**/*.js'],

		options: {
			safelist: [ 
				// all animations - they're generated on the fly so get missed by PurgeCSS
				'animate-hero-next-in',	
				'animate-hero-prev-in',
				'animate-hero-next-out', 	
				'animate-hero-prev-out',
				'animate-hero-text-in',	
				'animate-hero-text-out',		
				'animate-showcase-next-in', 	
				'animate-showcase-upcoming',
				'animate-showcase-prev-in', 	
				'animate-showcase-next-out', 
				'animate-showcase-prev-out', 
				'animate-showcase-leaving'
			]
		}
	},
	darkMode: 'class', // mode depends on class 'dark' on <html>
	theme: {
		extend: {
			fontSize: {
				'vw-xs'		: ['.8vw', '1.6vw'],
				'vw-sm'		: ['1vw', '1.8vw'],
				'vw-base'	: ['1.2vw', '1.9vw'],
				'vw-lg'		: ['1.4vw', '1.9vw'],
				'vw-xl'		: ['1.75vw', '2vw'],
				'vw-2xl'	: ['2.1vw', '2.2vw'],
				'vw-3xl'	: ['3vw', '3.1vw'],
				'vw-4xl'	: ['4vw', '4.2vw'],
				'vw-5xl'	: ['5vw', '5.3vw'],
				'vw-6xl'	: ['6vw', '6.5vw'],
				'vw-7xl'	: ['7vw', '7.5vw'],
				'vw-10xl'	: ['10vw', '11vw'],
				'vw-13xl'	: ['13vw', '15vw'],
			},
			maxWidth: {
				'vw-20.5'	: '20.5vw'
			},
			spacing: {
				// view width
				'vw-0.1' 	: '.1vw',
				'vw-0.3' 	: '.3vw',
				'vw-0.5' 	: '.5vw',
				'vw-0.6' 	: '.6vw',
				'vw-0.7' 	: '.7vw',
				'vw-1' 		: '1vw',
				'vw-1.5' 	: '1.5vw',
				'vw-2' 		: '2vw',
				'vw-2.5' 	: '2.5vw',
				'vw-3' 		: '3vw',
				'vw-4' 		: '4vw',
				'vw-5' 		: '5vw',
				'vw-6' 		: '6vw',
				'vw-7' 		: '7vw',
				'vw-8' 		: '8vw',
				'vw-9' 		: '9vw',
				'vw-10' 	: '10vw',
				'vw-11' 	: '11vw',
				'vw-12' 	: '12vw',
				'vw-13' 	: '13vw',
				'vw-14' 	: '14vw',
				'vw-15' 	: '15vw',
				'vw-16' 	: '16vw',
				'vw-18' 	: '18vw',
				'vw-20' 	: '20vw',
				'vw-23' 	: '23vw',
				'vw-25' 	: '25vw',
				'vw-28' 	: '28vw',
				'vw-30' 	: '30vw',
				'vw-35' 	: '35vw',
				'vw-40' 	: '40vw',
				'vw-42.25' 	: '42.25vw', // yuck 
				'vw-43' 	: '43vw',
				'vw-50' 	: '50vw',
				'vw-60' 	: '60vw',
				'vw-65' 	: '65vw',
				'vw-80' 	: '80vw',

				// view height
				'vh-40' 	: '40vh',
				'vh-60' 	: '60vh',

				// pixel height
				'px-624': '624px',
				'px-468': '468px',
				'px-535': '535px'
			},
			lineHeight: {
				'vw-1' 	: '1vw'
			},
			borderWidth: {
				'vw-1' 	: '.1vw',
				'vw-2' 	: '.2vw',
				'vw-5' 	: '.5vw',
				'vw-10' : '1vw',
			},
			keyframes: {
				'fade-slide-in-left' : {
					'0%' 	: { transform: 'translateX(5vw)', 	opacity: 0, zIndex: -1 },
					'100%' 	: { transform: 'translateX(0)', 	opacity: 1, zIndex: 1 }
				},
				'fade-slide-in-up' : {
					'0%' 	: { transform: 'translateY(5vw)', 	opacity: 0 },
					'100%' 	: { transform: 'translateY(0)', 	opacity: 1 }
				},
				'fade-slide-in-right' : {
					'0%' 	: { transform: 'translateX(-5vw)', 	opacity: 0, zIndex: -1 },
					'100%' 	: { transform: 'translateX(0)', 	opacity: 1, zIndex: 1 }
				},
				'fade-slide-out-left' : {
					'0%' 	: { transform: 'translateX(0)', 	opacity: 1, zIndex: 1 },
					'100%' 	: { transform: 'translateX(-5vw)', 	opacity: 0, zIndex: -1 }
				},
				'fade-slide-out-right' : {
					'0%' 	: { transform: 'translateX(0)', 	opacity: 1, zIndex: 1 },
					'100%' 	: { transform: 'translateX(5vw)', 	opacity: 0, zIndex: -1 }
				},
				'cycle-in-left' : {
					'0%' 	: { transform: 'translateX(25vw) scale(0.6)',	filter: 'blur(1vw)', 	opacity: 1, zIndex: 2 },
					'100%' 	: { transform: 'translateX(0) scale(1)',		filter: 'blur(0)', 		opacity: 1, zIndex: 2 }
				},
				'cycle-in-left-back' : {
					'0%' 	: { transform: 'translateX(25vw) scale(0.2)', 	filter: 'blur(1vw)', 	opacity: 1, zIndex: 1 },
					'100%' 	: { transform: 'translateX(25vw) scale(0.6)', 	filter: 'blur(1vw)',	opacity: 1, zIndex: 1 }
				},
				'cycle-in-right' : {
					'0%' 	: { transform: 'translateX(-5vw) scale(0.2)', 	opacity: 0, zIndex: -1 },
					'100%' 	: { transform: 'translateX(0) scale(1)', 		opacity: 1, zIndex: 2 }
				},
				'cycle-out-left' : {
					'0%' 	: { transform: 'translateX(0) scale(1)', 		opacity: 1, zIndex: 2 },
					'100%' 	: { transform: 'translateX(-5vw) scale(0.2)', 	opacity: 0, zIndex: -1 }
				},
				'cycle-out-right' : {
					'0%' 	: { transform: 'translateX(0) scale(1)', 		filter: 'blur(0)',		opacity: 1, zIndex: 2 },
					'100%' 	: { transform: 'translateX(25vw) scale(0.6)', 	filter: 'blur(1vw)',	opacity: 1, zIndex: 1 }
				},
				'cycle-out-right-back' : {
					'0%' 	: { transform: 'translateX(25vw) scale(0.6)', 	filter: 'blur(1vw)',	opacity: 1, zIndex: 1 },
					'100%' 	: { transform: 'translateX(25vw) scale(0.2)', 	filter: 'blur(1vw)',	opacity: 0, zIndex: -1 }
				},
				'flick-up' : {
					'0%' 	: { top: '2vw', opacity: 0, zIndex: -1 },
					'100%' 	: { top: 0, 	opacity: 1, zIndex: 1 }
				},
				'fade-out' : {
					'0%' 	: { opacity: 1, zIndex: -1 },
					'100%' 	: { opacity: 0, zIndex: -1 }
				},
				'fade-in' : {
					'0%' 	: { opacity: 0},
					'100%' 	: { opacity: 1 }
				},
				'expand-out' : {
					'0%' 		: { opacity: 1, },
					'100%' 		: { opacity: 0, transform: 'scale(10)' }
				}
			},
			animation: {
				'hero-next-in' 		: 'fade-slide-in-left .2s ease-in forwards',
				'hero-prev-in' 		: 'fade-slide-in-right .2s ease-in forwards',
				'hero-next-out' 	: 'fade-slide-out-left .2s ease-out forwards',
				'hero-prev-out' 	: 'fade-slide-out-right .2s ease-out forwards',
				'hero-text-in'		: 'flick-up .6s ease-in-out forwards',
				'hero-text-out'		: 'fade-out 1s ease-out forwards',
				'showcase-next-in' 	: 'cycle-in-left .4s ease-in-out forwards',
				'showcase-upcoming' : 'cycle-in-left-back .4s ease-in-out forwards',
				'showcase-prev-in' 	: 'cycle-in-right .4s ease-in-out forwards',
				'showcase-next-out' : 'cycle-out-left .4s ease-in-out forwards',
				'showcase-prev-out' : 'cycle-out-right .4s ease-in-out forwards',
				'showcase-leaving' 	: 'cycle-out-right-back .4s ease-in-out forwards',
				'radar'				: 'expand-out 4s infinite',
				'modal-drawer-in'	: 'fade-slide-in-left .3s ease-in forwards',
				'modal-popup-in'	: 'fade-slide-in-up .3s ease-in forwards',
				'modal-tint-in'		: 'fade-in .15s ease-in forwards'
			},
			zIndex: {
				'-1': '-1'
			},
			transitionProperty: {
				'positions' : 'top, left'
			},
			gridTemplateColumns: {
				'checkout': '7vw auto 7vw 7vw 7vw 7vw',
				'checkout-mobile': '20vw auto 20vw 10vw 15vw 15vw'
			}
		},
		colors: {
			transparent: 'transparent',
			primary: '#5E8B52',
			success: '#388E4B',
			info: '#47BEE6',
			error: '#ED1B30',
			warn: '#F28253',
			highlight: '#FF8CF5',
			grey: {
				50	: '#F9F9F9',
				100	: '#F3F3F3',
				200	: '#E2E2E2',
				300	: '#D1D1D1',
				400	: '#AFAFAF',
				500	: '#8C8C8C',
				600	: '#7E7E7E',
				700	: '#545454',
				800	: '#3F3F3F',
				900	: '#2A2A2A',
			},
			black: {
				'trans' : {
					'light' : 'rgba(0, 0, 0, .2)',
					DEFAULT : 'rgba(0, 0, 0, .5)',
					'dark' 	: 'rgba(0, 0, 0, .8)',
				},
				DEFAULT: '#000000'
			},
			white: {
				'trans' : {
					'light' : 'rgba(255, 255, 255, .3)',
					DEFAULT : 'rgba(0, 0, 0, .5)',
					'dark' 	: 'rgba(0, 0, 0, .8)',
				},
				DEFAULT: '#FFFFFF',
			},
		},
		fontFamily: {
			'sans' : ['Lato', 'sans-serif'],
			'serif': ['adobe-garamond-pro', 'serif'],
		}
	},
	variants: {
		extend: {
			padding: ['last'], // enable last: prefix (last-child) for padding styles 
		},
		scrollbar: ['rounded']
	},
	plugins: [
		require('@tailwindcss/line-clamp'),
		require('tailwind-scrollbar'),
	],
}