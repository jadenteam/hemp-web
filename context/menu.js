import React, { useContext, useEffect, useState } from 'react'

import { defaultMenu, getAudienceAssociatedMenu, getMenu } from '../repository/getShopMenuItems'

/** Menu Context */
export const MenuContext = React.createContext()

/** 
 * Menu Provider
 * Wraps the root element and allows use of the global modal 
 */

export const MenuProvider = props => {
	const [menu, setMenu] = useState(defaultMenu[process.env.SITE_KEY])

	useEffect(async () => {
		if (process.env.IS_SHOP) {
			if (process.env.SITE_KEY === 'clothing' || process.env.SITE_KEY === 'bodycare') {
				const shopMenu = await getAudienceAssociatedMenu()
				setMenu(shopMenu)
			} else {
				const shopMenu = await getMenu()
				setMenu(prevState => ([
					...prevState,
					...shopMenu
				]))
			}
		}
	}, [])

	// get subcategories by parent category slug
	function submenu(slug=false, level=0, parent=false) {
		let output = [
			{ id: 0, label: 'All', slug: '', link: '/shop', active: level <= 1 }
		]

		if(menu) {
			// based on the passed menu level, return the appropriate categories as the submenu 
			switch(level) {
				// third-level categories
				case 2: 
					let thirdLevel = []
					// if men/women fake "categories", we only go 2 levels deep
					if(['men', 'women'].includes(parent)) {
						thirdLevel = menu.find(item => item.slug === parent)
						if(thirdLevel) {
							output = setActiveItem(output.concat(thirdLevel.subcategories), slug)
						}
					} 
					// otherwise we need to get the parent category before we get the third level
					else {
						const parentLevel = menu.find(item => item.slug === parent)
						if(parentLevel) {
							thirdLevel = parentLevel.subcategories.find(sub => sub.slug === slug)
							if(thirdLevel) {
								output = setActiveItem(output.concat(parentLevel.subcategories), slug)
							}
						}
					}				
					break
				
				// second-level categories
				case 1: 
					const secondLevel = menu.find(item => item.slug === slug)
					if(secondLevel) {
						output = setActiveItem(output.concat(secondLevel.subcategories), slug)
					}
					break 

				// base (top) categories 
				case 0: 
				default: 
					output = setActiveItem(output.concat(menu), '')
					break
			}
		}

		return output
	}

	return <MenuContext.Provider value={{ menu, submenu }} {...props} >
		{props.children}
	</MenuContext.Provider>
}

/** set active flag on the currently active menu item according to submenu slug */
function setActiveItem(categories, activeSlug) {
	return categories.map(cat => cat.slug === activeSlug ? {
		...cat, 
		active: true
	} : cat)
}

export function useMenuContext() {
	return useContext(MenuContext)
}