import React, { useContext, useEffect, useState } from 'react'
import getCart from '../repository/getCart'

/** Cart Context */
export const CartContext = React.createContext()

/** Cart default state  */
export const defaultCart = {
	id: '',
	total_quantity: 0,
	items: [],
	prices: {
		subtotal: 0,
		discount: 0,
		total: 0
	},
	payment: [],
	coupon: ''
}

/** 
 * Cart Provider
 * Wraps the root element and allows use of the global modal 
 */
export const CartProvider = props => {
	const [cart, setCart] = useState(defaultCart)

	// set cart data 
	useEffect(async () => {
		const response = await getCart()
		setCart(response)
	}, [])

	// Reset cart back to initial state  
	const clearCart = () => {
		setCart(defaultCart)
	}

	return <CartContext.Provider value={{ cart, setCart, clearCart }} {...props} >
		{props.children}
	</CartContext.Provider>
}

export function useCartContext() {
	return useContext(CartContext)
}