import React, { useCallback, useEffect, useState } from 'react'
import Modal from '../components/modules/Modal'
import usePreventScroll from '../hooks/usePreventScroll'

/** Modal Context */
export const ModalContext = React.createContext()

/** 
 * Modal Provider
 * Wraps the root element and allows use of the global modal 
 */
export const ModalProvider = props => {
	
	const [modal, setModal] = useState({
		content: null, 
		style: null
	})

	// Set content back to null on close 
	const closeModal = useCallback(() => {
		setModal({
			content: null, 
			style: null
		})
	}, [setModal])

	usePreventScroll(!!modal && modal.content)

	return <ModalContext.Provider value={{ setModal, closeModal }} {...props} >
		{props.children}
		{!!modal && modal.content && <Modal {...modal} close={closeModal} />}
	</ModalContext.Provider>
}