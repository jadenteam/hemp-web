import React, { useState } from 'react'
import cn from 'classnames'
import Link from 'next/link'
import { useRouter } from 'next/router'

import { useCartContext } from '../context/cart'
import { useMenuContext } from '../context/menu'
import HeaderFixed from './HeaderFixed'
import { IconButton } from './modules/Button'
import ScreenTint from './modules/ScreenTint'
import Cart from './modules/Cart'
import Icon, { ShopBag } from './modules/Icons'
import Search from './modules/Search'

import useScrollDirection from '../hooks/useScrollDirection'
import usePreventScroll from '../hooks/usePreventScroll'
import useModal from '../hooks/useModal'

import getSocial from '../repository/getSocial'
import { currency } from "../utility/format"
import Banner from './modules/Banner'

/**
 * Header and main navigation for desktop/mobile breakpoints and fixed header for scroll
 */
export default function Header({ home, checkout, menuFeaturedProducts, banner, bannerActive, closeBanner }) {

	const scrollDirection = useScrollDirection()
	let stores = menuFeaturedProducts;

	return <header className="relative z-40">
		<Banner {...{ banner, bannerActive, closeBanner }} />
		<NavDesktop {...{ stores, home, checkout }} />
		<NavMobile {...{ stores, home, checkout, banner, bannerActive }} />
		<HeaderFixed active={scrollDirection === 'up'} {...{ stores }} />
	</header>
}

/**
 * Desktop-only logo and navigation
 */
export function NavDesktop({ stores, home, checkout }) {
	//const suffix = process.env.IS_MAIN ? `-${home ? 'light' :'dark'}` : ''

	const suffix = process.env.IS_MAIN && home ? '-light' : '-dark'

	return <div className={cn('hidden relative z-10 content-center px-5 pt-1 md:flex md:px-vw-7 md:pt-vw-1',
		home && 'text-white'
	)}>
		<a href="/" className="w-vw-14 flex items-center">
			<img className="w-vw-9" src={`/images/logo-${process.env.SITE_KEY}${suffix}.svg`} alt="Hemp Forever" />
		</a>
		{process.env.IS_MAIN ? <MenuMain {...{ stores, home }} /> : <MenuShop {...{ checkout }} />}
		{process.env.IS_MAIN && <div className="flex justify-end space-x-4 items-center w-vw-10">
			{getSocial.map(obj => <a key={obj.label} target="_blank" href={obj.link}><Icon light={home} className="w-vw-2" type={obj.icon} /></a>)}
		</div>}
	</div>
}

/**
 * Mobile-only logo and navigation, including toggle button and pull-out drawer
 */
export function NavMobile({ stores, home, bannerActive, banner }) {
	const { menu } = useMenuContext()
	const [open, setOpen] = useState(false)

	const suffix = home ? '-light' : '-dark'

	// prevent scrolling when open
	usePreventScroll(open)

	return <div className={cn('flex items-center justify-between md:hidden px-5 py-2')}>

		{/* drawer toggle button */}
		<div>
			<IconButton variant="clear" onClick={() => setOpen(true)} icon="hamburger" lightIcon={home} />
		</div>

		{/* logo */}
		<a href="/" className="">
			<img className="mr-vw-5 ml-5 md:ml-0" src={`/images/logo-${process.env.SITE_KEY}${suffix}.svg`} alt="Hemp Forever" />
		</a>

		<div className="flex-grow"></div>

		{!process.env.IS_MAIN && <div className={cn(
			'fixed right-5 top-7',
			{
				'top-28': bannerActive && banner.field_banner_active
			}
		)}>
			<CartTotal />
		</div>}

		{/* drawer menu */}
		<nav className={cn('fixed z-20 w-2/3 inset-y-0 bg-white text-lg top-0 bottom-0 p-3 transition-positions duration-200',
			open ? 'left-1/3' : 'left-full'
		)}>
			<ul>
				{menu.map((page, i) => <DrawerItem key={i} {...page} active={i === 1} />)}
			</ul>
			<ul>
				<li className="relative">
					<span className="inline-block uppercase font-black p-1">Shops:</span>
				</li>
				{stores.map((page, i) => <DrawerItem key={i} {...page} active={i === 1} />)}
			</ul>
		</nav>

		{open && <ScreenTint fixed dark foreground onClick={() => setOpen(false)} />}

		{open && <div className="fixed top-3 left-3">
			<IconButton variant="white" onClick={() => setOpen(false)} icon="close" />
		</div>}

	</div>
}

/**
 * Header menu for main website 
 */
export function MenuMain({ stores, home }) {

	const { menu } = useMenuContext()

	return (
		<div className="flex flex-grow">
			<nav className="flex-grow">
				<ul className={cn('flex px-vw-1 border-b-vw-2 border-grey-200',
					home && 'border-white-trans-light'
				)}>
					{menu.map((page, i) => <MenuItem key={i} {...page} primary />)}
				</ul>
				<ul className="flex md:px-vw-1">
					<li className="flex items-center py-vw-1 px-vw-1.5">
						<ShopBag light={home} />
						<span className="inline-block uppercase font-black text-vw-sm pl-vw-0.7">Shops:</span>
					</li>
					{stores.map((page, i) => <MenuItem key={i} {...page} />)}
				</ul>
			</nav>
		</div>
	)
}

/**
 * Header menu for shop websites
 */
export function MenuShop({ checkout }) {
	const { setModal, closeModal } = useModal()
	const { menu } = useMenuContext()

	// get the current category slug from URL 
	// in the header we only care about second-level categories (index 0 in array)
	const router = useRouter()
	const { category } = router.query
	const currentCategory = category ? category[0] : ''

	function openSearch() {
		setModal({
			content: <Search {...{ closeModal }} />,
			style: 'drawer'
		})
	}

	return <nav className="flex flex-grow items-center justify-end">
		<ul className="flex px-1 md:px-0">
			{!checkout ?
				<>
					{menu.map((page, i) => <MenuItem key={i} {...page} active={page.slug === currentCategory} shop />)}
					<li className="flex items-center">
						<IconButton variant="white"
							icon="search"
							className="inline-block text-vw-sm"
							onClick={openSearch}
						/>
					</li>
					<li className="relative top-px py-vw-1 pl-vw-1 -mr-vw-1 w-vw-8"><CartTotal /></li>
				</>
				:
				<li className="relative top-px pt-vw-2 pb-vw-1 pl-vw-1">
					<Link href="/shop">
						<a className="uppercase flex space-x-vw-1">
							<span>Continue shopping</span>
							<Icon type="next" />
						</a>
					</Link>
				</li>
			}
		</ul>
	</nav>
}

/**
 * Single navigation item in desktop-only main menu 
 */
export function MenuItem({ link, label, target, active, primary, shop }) {
	return <li className="relative top-px">
		<a href={link} target={target} className={cn(
			'inline-block py-vw-0.7 text-vw-sm border-b-vw-2 border-transparent',
			active && 'text-grey-400',
			primary ? 'uppercase font-black hover:border-primary px-vw-1.5' : 'py-vw-1 px-vw-1',
			shop ? 'text-vw-base font-medium' : 'font-bold',
		)}>
			{label}
		</a>
	</li >
}

/**
 * Single navigation item in mobile-only main menu 
 */
export function DrawerItem({ link, label, target }) {
	return <li className="">
		<a href={link} target={target} className="inline-block p-1">
			{label}
		</a>
	</li>
}

/**
 * Shopping Bag fixed icon and link 
 */
export function CartTotal() {
	const { cart } = useCartContext()
	const { setModal, closeModal } = useModal()

	/** Open the cart drawer modal */
	function openModal() {
		setModal({
			content: <Cart {...{ closeModal }} />,
			style: 'drawer'
		})
	}

	return <div id="button__viewCart" className="md:fixed">
		<div onClick={openModal} className="flex justify-end items-center bg-grey-200 rounded-full h-8 md:h-vw-2 cursor-pointer">
			<span className="text-black text-base md:text-vw-sm leading-none p-2 md:p-vw-0.5 flex-grow">{!!cart && currency.format(cart.prices.total)}</span>
			<span className="flex justify-center items-center bg-black text-white rounded-full w-7 h-7 md:w-vw-2 md:h-vw-2 flex-shrink-0">
				<img src="/images/cart.svg" alt="Cart" />
			</span>
		</div>
	</div>
}