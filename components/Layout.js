import cn from 'classnames'
import Head from 'next/head'
import parse from 'html-react-parser'

import Header from './Header'
import Footer from './Footer'

export default function Layout({ className, children, noheader, wide, seo, title = '', menuFeaturedProducts, headContents = null, banner = null, bannerActive = false, closeBanner = null }) {
	return <main className="relative text-base antialiased overflow-x-hidden md:text-vw-base">
		{seo && <Head>{parse(seo.head)}</Head>}
		<Head>
			<link rel="icon" href="/favicon.ico" />
			<link rel="icon" href="/favicon.svg" type="image/svg+xml" />
			<link rel="apple-touch-icon" href="/favicon-apple.png" />
			<link rel="manifest" href="/manifest.webmanifest"></link>
			<meta charSet="UTF-8" />
			<meta httpEquiv="X-UA-Compatible" content="IE=edge" />
			<meta name="viewport" content="width=device-width, initial-scale=1.0" />
			<title>{title || 'Hemp Forever'}</title>

			{headContents}
		</Head>
		{noheader ?
			<div className={cn(
				'',
				className
			)}>{children}</div>
			:
			<>
				<Header
					menuFeaturedProducts={menuFeaturedProducts}
					banner={banner}
					bannerActive={bannerActive}
					closeBanner={closeBanner}
				/>
				<div className={cn(
					wide ? 'p-4 pt-8 md:pt-vw-2 md:px-vw-4' : 'p-3 pt-4 md:pt-vw-2 md:px-vw-7',
					className
				)}>{children}</div>
			</>
		}
		<Footer />
	</main>
}