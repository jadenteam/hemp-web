import { useState } from 'react'
import cn from 'classnames'

import { LiveButton } from './Button'
import setMailchimpSubscriber from '../../repository/setMailchimpSubscriber'

export default function FormEmail({ variant, isCheckout, description = "", GAClass = "" }) {
	const [value, setValue] = useState('')
	const [loading, setLoading] = useState(false)
	const [message, setMessage] = useState(null);

	async function handleSubmit() {
		const tags = [
			'newsletter',
			...process.env.IS_MAIN ? ['source-homepage'] : [],
			...process.env.IS_SHOP ? [`source-${process.env.SITE_KEY}`] : [],
			...isCheckout ? ['customer'] : []
		]

		let response
		try {
			// add mailchimp subscriber and attach a discount code
			response = await setMailchimpSubscriber(value, tags)
		} catch (err) {
			setMessage({
				type: "error",
				text: err.errorMessage
			});
			setLoading(false)
			return
		}

		setMessage({
			type: "success",
			text: "Thanks for joining us, you're now subscribed!"
		});
		setLoading(false)
		return
	}

	function validate(e) {
		e.preventDefault()
		setLoading(true)
		setMessage(null)
		handleSubmit()
	}

	return (
		<form onSubmit={validate}>
			<div className={cn('flex mb-1 md:mb-vw-1',
				variant === 'basic' && 'border-b md:border-b-vw-1 border-grey-900 dark:border-grey-600'
			)}>
				<input className={cn(
					'flex-grow w-full py-1 text-sm md:py-vw-1 md:text-vw-sm dark:bg-black',
					!variant || variant === 'bordered' && 'border md:border-vw-1 border-black rounded-none',
					variant === 'basic' ? '' : 'px-2 md:px-vw-2'
				)}
					type="email"
					placeholder="your email address..."
					value={value}
					onChange={e => setValue(e.target.value)}
					required
				/>
				<LiveButton
					{...{ loading }}
					size="small"
					variant={variant === 'basic' && 'white'}
					type="submit">Subscribe
				</LiveButton>
			</div>
			<p className={cn(
				'text-sm md:text-vw-sm mt-vw-1.5',
				message ? message.type === "success" && 'text-primary' : "",
				message ? message.type === "error" && 'text-error' : ""
			)}>
				{message ? message.text : description}
			</p>
		</form>
	);
}