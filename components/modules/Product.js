import cn from 'classnames'
import Link from 'next/link'

import Modal from '../modules/Modal'
import QuickBuy from '../modules/QuickBuy'
import Loader from '../modules/Loader'
import useModal from '../../hooks/useModal'

import { currency } from "../../utility/format";
import ManufacturerLink from './ManufacturerLink'
import { getURLByID } from '../../repository/getStores'

/**
 * Display single product item
 */
export default function Product({ product, quickbuy, large, className, defaultCategory=null, loading=false }) {
	const { manufacturer, name, image, price, url_key } = product
	const { setModal, closeModal } = useModal()

	/** Open the QuickBuy drawer modal */
	function openModal() {
		setModal({
			content: <QuickBuy {...{ product, closeModal }} quantity="1" />,
			style: 'drawer'
		})
	}

	// Prevent double processing of format
	const regular_price = price.regular 
		? currency.format(price.regular) 
		: currency.format(product.price.discounted)

	const url = getURLByID(defaultCategory ? defaultCategory : product?.categories?.[0]?.id)

	return <li className={cn('', className)}>
		<div className="bg-grey-100">
			<div className="flex space-x-2 md:space-x-vw-2 p-3 md:p-vw-1 leading-snug text-base md:text-vw-sm">
				<span className="relative flex-grow truncate">
					<Loader {...{loading}} inline>
						<ManufacturerLink {...manufacturer} />
					</Loader>
				</span>
				{/* {quickbuy && <button onClick={openModal} className="flex-shrink-0 font-bold">+ Quick Buy</button>} */}
			</div>
			<Loader {...{loading}}>
				<div className={large ? 'md:h-vw-30' : 'md:h-vw-20'}>
					<Link href={`${url}/shop/product/${url_key}`}>
						<a>
							<img src={image.url} alt={image.label} className="button__product block w-full h-full object-cover" />
						</a>
					</Link>
				</div>
			</Loader>
			<div className="p-3 md:p-0">
				<Loader className="px-vw-1 pt-vw-1 w-full" {...{loading}} inline>
					<div className="font-bold uppercase leading-snug text-base md:text-vw-base md:truncate">{name}</div>
				</Loader>
				<Loader className="px-vw-1 pb-vw-1 leading-snug" {...{loading}} inline>
					{price.hasFromPrice && <span className="mr-1 md:mr-vw-0.3">from</span>}
					{price.isDiscounted && !price.hasFromPrice && <span className="text-grey-500 line-through mr-1 md:mr-vw-0.3 text-xs md:text-vw-base">{regular_price}</span>}
					<span className="text-xs md:text-vw-base">{price.isDiscounted ? currency.format(price.discounted) : regular_price}</span>
				</Loader>
			</div>
		</div>
	</li>
}