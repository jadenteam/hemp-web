import React, { useEffect, useState } from 'react'
import cn from 'classnames'

import Icon, { Loading } from './Icons'
import Link from 'next/link'

/** Basic button */
export const Button = React.forwardRef(({ href, target, type, className, children, onClick, size, variant, disabled }, ref) => { 
	const Element = (href ? `a` : `button`) // render element based on passed props

	function handleClick(e) {
		if(!href) {
			e.preventDefault()
			onClick && onClick(e)
		}
	}

	return <Element 
		ref={ref}
		href={href} 
		onClick={onClick && handleClick} 
		type={type} 
		target={target} 
		disabled={disabled}
		className={cn('flex items-center font-bold uppercase text-center border text-sm md:text-vw-sm', 
			size === 'small' ? 'w-28 md:w-vw-12 h-14 md:h-vw-4' : 'w-full h-14 md:h-vw-4',
			size === 'large' && 'h-20 md:h-vw-6 text-lg md:text-vw-lg',
			variant === 'white' ? 'justify-end bg-white dark:bg-black text-black dark:text-white border-white dark:border-black' : 'justify-center',
			variant === 'outlined' && 'bg-white dark:bg-black text-black dark:text-white border-black dark:border-white',
			(!variant || variant === 'black') && 'bg-black dark:bg-white text-white dark:text-black border-black dark:border-white',
			disabled ? 'opacity-50 cursor-not-allowed' : 'cursor-pointer',
			className
		)}>
		{children}
	</Element> 
})

/** Button with spinner effect on click */
export function LiveButton(props) { 
	const { children, loading, variant } = props

	return <Button {...props}>
		{loading ? <Loading light={variant !== 'white'} /> : children}
	</Button>
}

export function IconButton({ href, target, className, children, onClick, variant, icon, large, disabled, lightIcon }) {
	const Element = (href ? `a` : `button`) // render element based on passed props

	return <Element className={cn('flex items-center', className)} {...{href, target, onClick, disabled}}>
		{children && <div className="uppercase font-bold text-xs mr-2 md:text-vw-sm md:mr-vw-0.5">{children}</div>}
		<div className={cn('rounded-full flex justify-center items-center text-3xl transition-opacity md:w-vw-2 md:h-vw-2 md:text-vw-base',
			(!variant || variant === 'primary') && 'bg-primary text-white',
			variant === 'white' && 'bg-white text-black',
			variant === 'black' && 'bg-black text-white',
			variant === 'grey' && 'bg-grey-200 text-black',
			variant === 'clear' && 'bg-transparent text-white',
			large ? 'w-14 h-14 md:w-vw-3 md:h-vw-3' : 'h-10 w-10 md:w-vw-2 md:h-vw-2',
			disabled ? 'opacity-50 cursor-not-allowed' : 'cursor-pointer',
		)}>
			<Icon type={icon} light={variant == 'black' ? true : lightIcon ? lightIcon : false} />
		</div>
	</Element>
}

export function BigButton({ href, passHref, title, text, className }) {
    return <Link href={href} passHref={passHref}>
		<BigButtonInner {...{href, title, text, className}} />
	</Link>
}

export const BigButtonInner = React.forwardRef(({ href, title, text, className }, ref) => {
    return <Button href={href} className={cn('flex-col w-full h-24 md:h-vw-8', className)}>
        <div className="font-black text-lg leading-none">{title}</div>
        <div className="normal-case text-grey-500">{text}</div>
    </Button>
})