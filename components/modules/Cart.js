import { useState } from 'react'
import cn from 'classnames'
import Link from 'next/link'

import { Button, IconButton } from './Button'
import ModalProduct from './ModalProduct'

import { removeFromCart } from '../../repository/getCart'
import useRouterRefresh from '../../hooks/useRouterRefresh'
import { useCartContext } from '../../context/cart'
import useCartUpdate from '../../hooks/useCartUpdate'

import { currency } from "../../utility/format";

/**
 * Display the quick buy info and payment form 
 */
export default function Cart({ closeModal }) {
	const { cart } = useCartContext()
	const { loading, updateCartItem, removeCartItem } = useCartUpdate()

	return <div id="element__cart" className="px-0 md:px-vw-3 pb-vw-2 flex flex-col h-full">
		<section className="flex items-center text-2xl md:text-vw-2xl py-5">
			<IconButton variant="white" onClick={closeModal} icon="back" className="" />
			<span className="ml-4 md:ml-vw-1 uppercase">Shopping Bag</span>
		</section>

		<section className="bg-highlight flex items-center justify-center p-5 md:p-vw-1.5 md:mt-vw-2">
			<img className="mr-3" src="/images/shipping-free.svg" alt="" />
			<span className="uppercase font-normal">
				Free shipping Australia wide!
			</span>
		</section>

		<div className="overflow-y-scroll pl-5 md:pl-0 pr-7 scrollbar-thin scrollbar-thumb-grey-300 scrollbar-track-transparent scrollbar-thumb-rounded-full">
			<ul className="pb-5">
				{cart && !!cart.items.length && cart.items.map((item, i) => <ModalProduct
					key={item.id}
					cartID={cart.id}
					id={item.id}
					product={item.product}
					initialQuantity={item.quantity}
					initialOptions={item.options}
					update={updateCartItem}
					remove={removeCartItem}
					{...{ loading }}
				/>)}
			</ul>
		</div>

		<div className="px-5 py-5">
			<table className="w-full text-base md:text-vw-base my-vw-1 leading-4 mb-5">
				<tbody>
					<tr>
						<th className="font-normal text-left">{cart.total_quantity} items</th>
						<td className="text-right">{currency.format(cart.prices.total)}</td>
					</tr>
					<tr>
						<th className="font-normal text-left">Shipping</th>
						<td className="text-right">Free AU Shipping</td>
					</tr>
				</tbody>
			</table>
			<Link href="/shop/checkout" passHref>
				<Button className="button__checkout w-full">Checkout</Button>
			</Link>
		</div>
	</div>
}

export function CartItem({ name }) {
	return <li className="flex">
		<div>
			{name}
		</div>
	</li>
}