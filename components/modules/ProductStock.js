/**
 * Display styled product stock label 
 */
export default function ProductStockLabel({ inStock, only_x_left_in_stock }) {
	if(inStock) {
		if(!!only_x_left_in_stock) {
			return <span className="text-warn">Only {only_x_left_in_stock} left!</span> 
		}
		return <span className="text-success">In stock</span> 
	}
	return <span className="text-error">Out of stock</span>
}