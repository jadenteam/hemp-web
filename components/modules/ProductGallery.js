import { useState } from 'react'
import cn from 'classnames'

import { IconButton } from './Button'
import Pips from './Pips'
import Product from './Product'

import useCarousel from '../../hooks/useCarousel'
import useBreakpoints from '../../hooks/useBreakpoints'
import { chunk } from '../../utility/array'

/**
 * Display carousel of products with optional category filters
 */
export default function ProductGallery({ className, products=[], categories=[], defaultCategory=null }) {

	const { isMobile } = useBreakpoints()
	
	// split products into category groups 
	const [category, setCategory] = useState(!!categories.length ? categories[0].id : null)
	const filteredProducts = categories.length ? products.filter(categoryFilter, category) : products 

	function categoryFilter(item) {
		return item.categories.find(cat => cat.id === this)
	}

	// chunk the products into groups according to window size
	const productRows = chunk(filteredProducts, isMobile ? 1 : 4)

	// create the carousel state based on product rows 
	const { carouselIndex, carouselActive, goTo, goToNext, goToPrev, animateSlide } = useCarousel(productRows, 'hero')

	return <div className={cn('relative pt-5', className)}>
		<Categories categories={categories} current={category} setCurrent={setCategory} />
		{carouselActive && <div className="absolute top-5 right-0 flex justify-end items-center">
			<IconButton variant="black" onClick={goToPrev} icon="prev" />
			<IconButton variant="black" onClick={goToNext} className="ml-2 md:ml-vw-0.5" icon="next" />
		</div>}
		<ul className="grid p-vw-0 md:py-vw-1 mt-5 md:mt-0 mb-5 md:mb-vw-0.5">
			{productRows.map((chunk, i) => <ProductRow key={i} counter={i} products={chunk} animateSlide={animateSlide} defaultCategory={defaultCategory} />)}
		</ul>
		<Pips active={carouselActive} total={productRows.length} counter={carouselIndex} goTo={goTo} className="justify-end" dark />
	</div>
}

/**
 * Display and handle the category filter list
 */
export function Categories({ categories, current, setCurrent }) {	
	if(!categories.length) return null

	const { isDesktop } = useBreakpoints()

	function handleClick(value, e) {
		e.preventDefault()
		setCurrent(value)
	}

	function handleChange(e) {
		setCurrent(e.target.value)
	}
		
	return isDesktop ? 
		<ul className="flex space-x-2 md:space-x-vw-1 items-center py-vw-1">
			{categories.map((cat, i) => <li key={i} className="text-vw-sm uppercase">
				<a onClick={e => handleClick(cat.id, e)} className={cn('py-vw-1 cursor-pointer',
					cat.id === current && 'underline font-bold'
				)}>{cat.name}</a>
			</li>)}
		</ul>
	: 
		<Dropdown name="category" options={categories} onChange={handleChange} />

}

/**
 * Display single product row. Number of items in row depends on window size 
 */
export function ProductRow({ counter, products, animateSlide, defaultCategory }) {
	return <li className={animateSlide('col-span-full row-span-full w-full', counter)}>
		<ul className="flex space-x-2 md:space-x-vw-1">
			{products.map((product, i) => <Product key={i} {...{product}} className="w-full md:w-1/4 md:max-w-vw-20.5" defaultCategory={defaultCategory} />)}
		</ul>
	</li>
}

/**
 * Display category select for mobile 
 */
export function Dropdown({ name, options, onChange }) {
	return <select name={name} onChange={onChange} className="p-2">
		{options.map((opt, i) => <option key={i} value={opt.id}>{opt.name}</option>)}
	</select>
}