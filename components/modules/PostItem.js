import cn from 'classnames'
import Link from 'next/link'

import { H4 } from './Headings'
import PostDateline from './PostDateline'
import { ResponsiveImage, PlaceholderImage } from './Image'

export default function PostItem({ className, slug, title, categories, date, excerpt, image, linkToHome = false }) {
	// append the home URL to front of link if needed
	// TODO: replace with repository call
	const linkPrefix = linkToHome ? 'https://home.hempforever.store' : ''
	const link = `${linkPrefix}/post/${slug}`
	const target = linkToHome ? '_blank' : '_self'

	return <article className={cn('', className)}>
		<Link href={link} passHref>
			<a target={target}>
				{image ? <ResponsiveImage image={image} alt={title} className="w-full" objectFit="contain" /> : <PlaceholderImage className="w-full" />}
			</a>
		</Link>
		<PostDateline categories={categories} date={date} />
		<Link href={link} passHref>
			<a target={target}>
				<H4 className="mb-1 md:mb-vw-1">{title}</H4>
			</a>
		</Link>
		<p className="line-clamp-2" >{excerpt}</p>
	</article>
}