import { H6 } from './Headings'
import Icon from "../../components/modules/Icons"
import Link from 'next/link';

export default function PostSidebar({ tags, slug }) {

	const shareUrl = `${process.env.SITE_URL}post/${slug}`;

	return <div className="pt-6 md:pt-0 md:sticky md:top-vh-60">
		<H6 className="py-5 md:py-vw-0.5">Share</H6>
		<div className="flex-grow flex justify-start space-x-4">
			<a target="_blank" href={`https://www.facebook.com/sharer.php?u=#${shareUrl}`}>
				<Icon className="w-8 md:w-vw-2" type="facebook" />
			</a>
			<a target="_blank" href={`https://twitter.com/intent/tweet?&url=${shareUrl}`}>
				<Icon className="w-8 md:w-vw-2" type="twitter" />
			</a>
			<a target="_blank" href={`mailto:?subject=Hemp Forever&body=Hey, I found this and thought you might enjoy seeing it too! ${shareUrl}`}>
				<Icon className="w-8 md:w-vw-2" type="email" />
			</a>
		</div>
		{!!tags.length && <>
			<H6 className="py-5 md:py-vw-0.5">Tags</H6>
			<ul className="flex flex-wrap">
				{tags.map((tag, i) => <Tag key={i} {...tag} />)}
			</ul>
		</>}
	</div>
}

export function Tag({ name, slug }) {
	return <li className="leading-none mr-1 mb-1 md:mr-vw-0.5 md:mb-vw-0.7">
		<Link href={`/blog/tag/${slug}`} passHref>
			<a className="border border-black leading-loose hover:bg-primary hover:text-white hover:border-primary text-xs px-2 py-1 whitespace-nowrap
				md:border-vw-1 md:leading-none md:text-vw-xs md:px-vw-0.5 md:py-vw-0.1">{name}</a>
		</Link>
	</li>
}