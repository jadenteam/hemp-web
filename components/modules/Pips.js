import cn from 'classnames'

/**
 * Display navigation pips for carousel slides with optional numbers
 */
export default function Pips({ className, total, counter, goTo, showNumbers, extend, dark, active }) {
	if(!active) return null

	const currentStyle = dark ? 'border-black dark:border-white' : 'border-white dark:border-black'
	const regularStyle = dark ? 'border-grey-200 dark:border-grey-800' : 'border-white-trans-light'

	return <ul className={cn('flex z-10 space-x-2 md:space-x-vw-1', className)}>
		{extend && <li className={cn('flex-grow block', regularStyle)}></li>}
		{[...Array(total)].map((x, i) => <li 
			key={i} 
			onClick={() => goTo(i)} 
			className={cn('block text-sm md:text-vw-sm pb-2',
				!!goTo && 'cursor-pointer',
				dark ? 'text-black dark:text-white' : 'text-white dark:text-black',
				showNumbers ? 'flex-grow border-t-2 md:border-t-vw-2' : 'w-8 md:w-vw-8 border-b-2 md:border-b-vw-2',
				counter === i ? currentStyle : regularStyle
			)}>
				{showNumbers && (i + 1)}
			</li>
		)}
	</ul>
}