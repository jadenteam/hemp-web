import Link from 'next/link'
import { useRouter } from 'next/router'
import cn from 'classnames'
import { useEffect, useState } from 'react'

import { LiveButton } from './Button'
import { LabelledField } from './Form'
import { useCartContext } from '../../context/cart'

import { addCheckoutDetailsToCart, addCouponCode, removeCouponCode, placeOrder, makePayment, addDeliveryMethod } from '../../repository/getCheckout'
import getCountries from '../../repository/getCountries'
import setMailchimpSubscriber, { checkMemberNotUsedCoupon, checkMemberNotExists, generateMailchimpCoupon, updateMailchimpSubscriber } from '../../repository/setMailchimpSubscriber'

import { currency, stringCurrency } from '../../utility/format'
import { CardNumber, CCV, ExpiryMonth, ExpiryYear } from './CardDetails'
import { validateCardDetails } from '../../utility/validation/validateCardDetails'
import { useDataLayer } from '../../hooks/useDataLayer'

export default function FormPayment() {
	const { cart, setCart } = useCartContext()
	const [stepComplete, setStepComplete] = useState(false)
	const [globalLoading, setGlobalLoading] = useState(false)
	const [globalLoadingMessage, setGlobalLoadingMessage] = useState('')

	const setGlobalLoader = (loading, message = '') => {
		setGlobalLoading(loading)
		setGlobalLoadingMessage(message)
	}

	return !stepComplete ?
		<Details {...{ cart, setCart }} setSubmitted={val => setStepComplete(val)} />
		:
		<div className="relative">
			{globalLoading && <div className="absolute h-full w-full bg-white bg-opacity-90 z-10 text-black flex items-center justify-center text-md">{globalLoadingMessage}</div>}
			<CompleteDetails 
				{...cart.shipping_address} 
				email={cart.email} 
				setSubmitted={val => setStepComplete(val)} 
			/>
			<Payment
				{...{ cart, setCart, globalLoading, setGlobalLoader }} 
				email={cart.email} 
				address={cart.shipping_address}
			/>
		</div>
}

export function Details({ cart, setCart, setSubmitted }) {
	const [errors, setErrors] = useState([])
	const [regions, setRegions] = useState([])
	const [loading, setLoading] = useState(false)
	const [email, setEmail] = useState('')
	const [shipping, setShipping] = useState({
		fname: '',
		lname: '',
		address: '',
		suburb: '',
		state: '',
		region_id : '',
		postcode: '',
		country: '',
		phone: ''
	})

	// get the available regions (just aus states for now)
	useEffect(async () => {
		const countries = await getCountries(shipping.country)
		setRegions(countries[0].available_regions)
	}, [])

	// prefill cart data 
	useEffect(() => {
		if (!!cart && cart.email) {
			setEmail(cart.email)
		}

		if (!!cart && cart.shipping_address) {
			setShipping(cart.shipping_address)
		}
	}, [cart])

	// handle field value changes
	function handleChange(e) {
		setShipping(prevState => ({
			...prevState,
			[e.target.name]: e.target.value
		}))
	}

	// handle region dropdown change
	// each option needs to assign two values to state
	function handleRegionChange(e) {
		setShipping(prevState => ({
			...prevState,
			['state']: e.target.value,
			['region_id']: e.target[e.target.selectedIndex].getAttribute('data-id'),
		}))
	}

	async function handleSubmit() {

		setLoading(true)

		const method = 'freeshipping'
		const response = await addCheckoutDetailsToCart(cart.id, { ...shipping, email, method })

		if (response.errors) {
			setErrors(response.errors)
		} else {
			setCart(response)
			setErrors([])
			setSubmitted(true)
		}

		setLoading(false)
	}

	function validate(e) {
		e.preventDefault()

		// do validation
		console.log('validating...')

		// submit
		handleSubmit(e)
	}

	return <form className="form__shippingInfo" onSubmit={validate}>
		<div className="flex md:space-x-vw-1 flex-col md:flex-row mt-vw-1">
			<LabelledField className="md:w-1/2" label="First name" type="text" name="fname" required value={shipping.fname} onChange={handleChange} />
			<LabelledField className="md:w-1/2" label="Last name" type="text" name="lname" required value={shipping.lname} onChange={handleChange} />
		</div>
		<LabelledField label="Street Address" type="text" name="address" required className="mt-vw-1" value={shipping.address} onChange={handleChange} />
		<LabelledField label="Suburb" type="text" name="suburb" required className="mt-vw-1" value={shipping.suburb} onChange={handleChange} />
		<div className="flex md:space-x-vw-1 flex-col md:flex-row mt-vw-1">
			<LabelledField className="md:w-1/2" label="State" type="select" name="state" required options={regions} value={shipping.state} onChange={handleRegionChange} placeholder />
			<LabelledField className="md:w-1/2" label="Postcode" type="text" name="postcode" required value={shipping.postcode} onChange={handleChange} />
		</div>
		<LabelledField label="Email" type="email" name="email" required className="mt-vw-1" value={email} onChange={e => setEmail(e.target.value)} />
		<LabelledField label="Phone number" type="tel" name="phone" required className="mt-vw-1" value={shipping.phone} onChange={handleChange} />
		<LiveButton {...{ loading }} size="large" className="mt-6 md:mt-vw-2" type="submit">Continue to payment</LiveButton>
		<div className="text-error">{errors.map((e, i) => <p key={i}>{e.message}</p>)}</div>
	</form>
}

export function CompleteDetails({ fname, lname, address, suburb, state, postcode, phone, email, setSubmitted }) {
	return <section className="relative mb-4 md:mb-vw-3">
		<div className="font-bold">{fname} {lname}</div>
		<div>{address}</div>
		<div>{suburb} {state} {postcode}</div>
		<div className="text-sm md:text-vw-sm">{email}</div>
		<div className="text-sm md:text-vw-sm">{phone}</div>
		<button className="absolute top-0 right-0 text-xs md:text-vw-xs cursor-pointer text-info" onClick={() => setSubmitted(false)}>Edit details</button>
	</section>
}

export function Payment({ cart, setCart, email, globalLoading, setGlobalLoader }) {

	const router = useRouter()
	const [error, setError] = useState(null)
	const [cardDetailErrors, setCardDetailErrors] = useState(null)

	const [cardDetails, setCardDetails] = useState({ EPS_CARDNUMBER: '', EPS_EXPIRYMONTH: '', EPS_EXPIRYYEAR: '', EPS_CCV: '' })
	const [shippingMethod, setShippingMethod] = useState('freeshipping')
	const [paymentMethod, setPaymentMethod] = useState('dfe_secure_pay')
	const [subscribeToMailchimp, setSubscribeToMailchimp] = useState(false)
	const [mailchimpError, setMailchimpError] = useState(null)
	const [mailchimpCoupon, setMailchimpCoupon] = useState('')
	const [existingMailchimpMember, setExistingMailchimpMember] = useState(false)

	const [changeDataLayer] = useDataLayer()

	const dlItems = cart.items.map(item => {
		const { name, sku, price, manufacturer } = item.product
		return {
			item_name: name, // Name or ID is required.
			item_id: sku,
			price: stringCurrency(price.regular),
			item_brand: manufacturer.label,
			quantity: item.quantity
		}
	})

	useEffect(async () => {

		setGlobalLoader(true, 'Loading...')

		try {
			await addDeliveryMethod(cart.id, shippingMethod)
		} catch(err) {
			setError('Failed to add shipping method')
		}

		// Check if member is subscribed and if so check if they have used their discount
		const memberNotexists = await checkMemberNotExists(email)

		if(!memberNotexists.success) {
			// The member exists

			setGlobalLoader(true, 'Grabbing your coupon...')
			setExistingMailchimpMember(true)

			// Check that the member has no coupon generated
			const memberNotUsedCoupon = await checkMemberNotUsedCoupon(email)

			if(memberNotUsedCoupon.success) {

				// If no coupon on member generate one and apply to cart
				const coupon = { code: null }
				const generatedCoupon = await generateMailchimpCoupon(email)

				if(generatedCoupon.success) {
					coupon.code = generatedCoupon.coupon
				} else {
					setMailchimpError(generatedCoupon.error)
					setGlobalLoader(false)
					return
				}

				setGlobalLoader(true, 'Applying your coupon...')

				// Clear any existing coupons on the cart
				if(cart.coupon) await removeCouponCode(cart.id, cart.coupon)

				// Add the coupon
				const response = await addCouponCode(cart.id, coupon.code)

				if (response.errors) {
					setError(response.errors.toString())
				} else {
					setMailchimpCoupon(coupon.code)
					setCart(response)
				}

			}

		} else {
			setExistingMailchimpMember(false)
		}

		setGlobalLoader(false)

	}, [])

	// TODO: add any other payment options
	const paymentLogos = {
		dfe_secure_pay: '/images/payment-creditcard-colour.svg',
		// apply_pay: '/images/payment-apple-colour.svg',
		// afterpay: '/images/payment-afterpay-colour.svg'
	}

	const cardDetailIndex = {
		EPS_CARDNUMBER: 'Card Number',
		EPS_EXPIRYMONTH: 'Expiry Month',
		EPS_EXPIRYYEAR: 'Expiry Year',
		EPS_CCV: 'CCV'
	}

	const handleCardDetailChange = e => {

		setCardDetailErrors(null)
		setCardDetails(prevState => ({
			...prevState,
			[e.target.name]: e.target.value
		}))

	}

	const handleSubscribeToMailchimp = async checked => {

		setSubscribeToMailchimp(checked)
		setGlobalLoader(true, 'Creating your coupon...')

		if(checked) {

			const coupon = { code: null }

			// Coupon has been previously generated
			if(mailchimpCoupon) {
				coupon.code = mailchimpCoupon
			} else {

				const generatedCoupon = await generateMailchimpCoupon(email)

				if(generatedCoupon.success) {
					coupon.code = generatedCoupon.coupon
				} else {
					setMailchimpError(generatedCoupon.error)
					setGlobalLoader(false)
					return
				}

			}

			setGlobalLoader(true, 'Applying your coupon...')

			// Clear any existing coupons on the cart
			if(cart.coupon) await removeCouponCode(cart.id, cart.coupon)

			// Add the coupon
			const response = await addCouponCode(cart.id, coupon.code)

			if (response.errors) {
				setError(response.errors.toString())
			} else {
				setMailchimpCoupon(coupon.code)
				setCart(response)
			}
			
		} else {

			if(mailchimpCoupon && cart.coupon === mailchimpCoupon) {
				const response = await removeCouponCode(cart.id, cart.coupon)
				if (response.errors) {
					setError(response.errors.toString())
				} else {
					setCart(response)
				}
			}

		}

		setGlobalLoader(false)

	}

	const handleSubmit = async e => {

		e.preventDefault()

		if(globalLoading) return

		setGlobalLoader(true, 'Placing your order...')

		const validation = await validateCardDetails(cardDetails)

		if(!validation.isValid) {
			setCardDetailErrors(validation.errors)
			setGlobalLoader(false)
			return
		}

		const order = await placeOrder(cart.id, email, paymentMethod)

		if(order.err) {
			setError('Failed to place order: ' + order.err)
			setGlobalLoader(false)
			return
		}

		const { securePayObject } = order
		securePayObject.EPS_RESULTURL = process.env.NODE_ENV === "production" ? `${process.env.SITE_URL}api/shop/orderresult` : `http://localhost:3000/api/shop/orderresult`

		const payment = { response: null, error: null }

		try {
			const response = await makePayment({ ...securePayObject, ...cardDetails })
			payment.response = response.data
		} catch(err) {
			// Something failed while processing order result (making request to api)
			payment.error = 'please contact support.'
		}

		if(!payment.error) {

			// Requests succeeded
			if(payment.response.success === true) {

				if(existingMailchimpMember && mailchimpCoupon) {

					// update member to have coupon
					try {
						await updateMailchimpSubscriber(email, ['customer'], mailchimpCoupon)
					} catch(err) {
						// ignore
					}

				} else {

					if(subscribeToMailchimp && mailchimpCoupon) {
						try {
							await setMailchimpSubscriber(email, ['customer'], mailchimpCoupon)
						} catch(err) {
							// ignore
						}
					}

				}
				try{
					changeDataLayer({
						dataLayer: {
							event: "purchase",
							ecommerce: {
								transaction_id: securePayObject.EPS_REFERENCEID,
								affiliation: process.env.SITE_KEY,
								value: cart.prices.total,
								shipping: "0.00",
								currency: "AUD",
								coupon: mailchimpCoupon,
								items: dlItems
							}
						}
					})
				} catch(err) {
					// Failed to process event - ignore
				}

				// Requests succeeded and payment succeeded
				router.push(`/shop/order/${securePayObject.EPS_REFERENCEID}`)
			} else {
				// Requests succeeded but payment failed
				setError('We could not process your payment: ' + payment.response.message)
			}

		} else {
			// A request failed
			setError('We could not process your payment: ' + payment.error)
		}

		setGlobalLoader(false)
		return

	}

	const changeShippingMethod = async method => {
		
		setGlobalLoader(true, 'Updating shipping method...')
		setShippingMethod(method)
		await addDeliveryMethod(cart.id, method)
		setGlobalLoader(false)
		
	}

	return <form onSubmit={handleSubmit}>
		<div>
			{/* Card type */}
			<ul className="flex space-x-1 mb-4 md:mb-vw-1">
				{cart.payment_methods.map(m => <li key={m.code} className="flex-grow">
					<button className={cn(
						'flex justify-center items-center w-full h-full bg-grey-100 rounded p-3 md:py-vw-1 cursor border',
						m.code === paymentMethod ? 'border-warn' : 'border-grey-100'
					)} onClick={() => setPaymentMethod(m.code)}>
						<img src={paymentLogos[m.code]} alt={m.title} />
					</button>
				</li>)}
			</ul>
			
			{paymentMethod === 'dfe_secure_pay' &&
				<div className="cardDetails">
					<CardNumber 
						className="md:w-full"
						label="Card Number"
						name="EPS_CARDNUMBER"
						required 
						value={cardDetails.EPS_CARDNUMBER} 
						onChange={handleCardDetailChange} 
					/>
					<div className="flex md:space-x-vw-1 flex-col md:flex-row mt-vw-1 mb-vw-1">
						<ExpiryMonth 
							className="md:w-1/3"
							label="Month"
							type="number"
							name="EPS_EXPIRYMONTH"
							required
							value={cardDetails.EPS_EXPIRYMONTH}
							onChange={handleCardDetailChange}
						/>
						<ExpiryYear 
							className="md:w-1/3"
							label="Year"
							type="number"
							name="EPS_EXPIRYYEAR"
							required
							value={cardDetails.EPS_EXPIRYYEAR}
							onChange={handleCardDetailChange}
						/>
						<CCV
							className="md:w-1/3"
							label="CCV"
							name="EPS_CCV"
							required
							value={cardDetails.EPS_CCV}
							onChange={handleCardDetailChange}
						/>
					</div>
					{cardDetailErrors && Object.keys(cardDetailErrors).map(key => <div className="text-error md:text-vw-sm">{`${cardDetailIndex[key]} ${cardDetailErrors[key]}`}</div>)}
				</div>
			}

			{/* Delivery */}
			<h2 className="uppercase text-lg md:text-vw-lg mt-8 mb-2 md:my-vw-1">Delivery method</h2>
			<div className="">
				{cart.shipping_methods.map((method, i) => <label key={i} className="flex items-center bg-grey-100 rounded p-3 md:py-vw-0.5 md:px-vw-1 cursor-pointer">
					<input type="radio" name="shipping" value={method.carrier_code} onChange={() => changeShippingMethod(method.carrier_code)} checked={shippingMethod === method.carrier_code} />
					<span className="flex-grow pl-3 md:pl-vw-1">{method.carrier_title}</span>
					<span>{method.amount.value} {method.amount.currency}</span>
				</label>)}
			</div>

			{/* Coupon */}
			<h2 className="uppercase text-lg md:text-vw-lg mt-8 mb-2 md:my-vw-1">Your order</h2>
			<CouponField {...{ cart, setCart, globalLoading, setGlobalLoader }} />
			
			{/* Summary */}
			<table className="w-full text-sm md:text-vw-sm my-10 md:my-vw-2">
				<tbody>
					<tr>
						<th className="font-normal text-left">{cart.total_quantity} items</th>
						<td className="text-right">{currency.format(cart.prices.subtotal)}</td>
					</tr>
					{!!cart.coupon &&
						<tr>
							<th className="font-normal text-left">Discount</th>
							<td className="text-right">{currency.format(cart.prices.discount)}</td>
						</tr>
					}
					<tr>
						<th className="font-normal text-left">Shipping</th>
						<td className="text-right">Free</td>
					</tr>
					<tr>
						<th className="font-normal text-left">Returns</th>
						<td className="text-right">Free for 30 days</td>
					</tr>
					<tr className="border-t text-lg md:text-vw-lg">
						<th className="font-normal text-left">Total</th>
						<td className="text-right">{currency.format(cart.prices.total)}</td>
					</tr>
				</tbody>
			</table>

			{/* Newsletter subscribe */}
			{!existingMailchimpMember && <div>
				<label className="flex items-center space-x-2 md:space-x-vw-1 mb-5 md:mb-vw-2 cursor-pointer">
					<input onChange={e => handleSubscribeToMailchimp(e.target.checked)} type="checkbox" className="h-5 w-5 md:h-vw-1.5 md:w-vw-1.5" />
					<span className="block ml-5">Subscribe to our newsletter</span>
				</label>
				{mailchimpError && <div className="text-sm text-error md:text-vw-sm" >{mailchimpError}</div>}
			</div>}

			{/* Submit */}
			{error && <div className="text-sm text-error" >{error}</div>}
			<br />
			<LiveButton {...{ globalLoading }} size="large" type="submit">Place your order</LiveButton>
			<div className="text-xs md:text-vw-xs text-center mt-4 md:mt-vw-0.5">
				By clicking 'Place your order', you agree to the <Link href="/terms"><a target="_blank" className="text-info">Terms &amp; Conditions</a></Link>
			</div>

		</div>
	</form>
}

export function CouponField({ cart, setCart, globalLoading, setGlobalLoader }) {
	const [errors, setErrors] = useState([])
	const [value, setValue] = useState('')

	async function applyCoupon(e) {
		e.preventDefault()

		setErrors([])
		setGlobalLoader(true, 'Applying your coupon...')

		const response = await addCouponCode(cart.id, value)

		if (response.errors) {
			setErrors(response.errors)
		} else {
			setCart(response)
		}

		setGlobalLoader(false)

	}

	async function removeCoupon(e) {
		e.preventDefault()
		setGlobalLoader(true, 'Removing coupon...')

		const response = await removeCouponCode(cart.id)

		if (response.errors) {
			setErrors(response.errors)
		} else {
			setCart(response)
			setValue('')
		}

		setGlobalLoader(false)

	}

	return <>
		<div className="flex space-x-vw-1 flex-col md:flex-row mt-vw-1">
			{!!cart.coupon ?
				<div className="md:w-2/3">{cart.coupon}</div>
				:
				<LabelledField label="Gift card or promo code" type="text" name="coupon" className="md:w-2/3" value={value} onChange={e => setValue(e.target.value)} />
			}

			<button onClick={!!cart.coupon ? removeCoupon : applyCoupon} className="w-1/3 border rounded uppercase">
				{globalLoading ? '...' : !!cart.coupon ? 'Remove' : 'Apply'}
			</button>
		</div>
		{errors && !!errors.length && errors.map((err, i) => <div className="text-sm text-error" key={i}>{err.message}</div>)}
	</>
}