import cn from 'classnames'

import { IconButton } from './Button'
import ModalProduct from './ModalProduct'

/**
 * Display the quick buy info and payment form 
 */
export default function QuickBuy( props ) {
	return <article className="p-vw-4">
		<section className="flex items-center text-2xl md:text-vw-2xl py-5">
			<IconButton variant="white" onClick={props.closeModal} icon="back" className="" />
			<span className="ml-4 md:ml-vw-1 uppercase">Quick Buy</span>
		</section>
		<ModalProduct {...props} quickbuy />
	</article>
}