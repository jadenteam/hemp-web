import { useEffect, useState } from 'react'
import cn from 'classnames'
import { cc_format, cc_ccv, cc_ym } from '../../utility/format'

export function CardNumber({ 
		label, 
		placeholder, 
		name, 
		required, 
		light, 
		className, 
		value, 
		onChange, 
		disabled=false
	}) {

	const handleChange = e => {
		e.preventDefault()
        e.target.value = e.target.value.replaceAll(' ', '').substring(0,16)
        onChange(e)
    }

	return <TextOption {...{ label, placeholder, name, required, light, className, value, handleChange, disabled, maxLength: 20 }} formatter={cc_format} />
}

export function ExpiryMonth({ 
		label, 
		placeholder, 
		name, 
		required, 
		light, 
		className, 
		value, 
		onChange, 
		disabled=false
	}) {
    
    const handleChange = e => {
		e.preventDefault()
        e.target.value = e.target.value.replaceAll(' ', '').substring(0,2)
        onChange(e)
    }

	return <TextOption {...{ label, placeholder, name, required, light, className, value, handleChange, disabled, maxLength: 2 }} formatter={cc_ym} />

}

export function ExpiryYear({ 
		label, 
		placeholder, 
		name, 
		required, 
		light, 
		className, 
		value, 
		onChange, 
		disabled=false
	}) {
    
    const handleChange = e => {
		e.preventDefault()
        e.target.value = e.target.value.replaceAll(' ', '').substring(0,2)
        onChange(e)
    }

	return <TextOption {...{ label, placeholder, name, required, light, className, value, handleChange, disabled, maxLength: 2 }} formatter={cc_ym} />

}

export function CCV({ 
		label, 
		placeholder, 
		name, 
		required, 
		light, 
		className, 
		value, 
		onChange, 
		disabled=false
	}) {

	const handleChange = e => {
		e.preventDefault()
        e.target.value = e.target.value.replaceAll(' ', '').substring(0,3)
        onChange(e)
    }

	return <TextOption {...{ label, placeholder, name, required, light, className, value, handleChange, disabled, maxLength: 3 }} formatter={cc_ccv} />

}

function TextOption({ label, placeholder, name, required, light, className, value, handleChange, disabled=false, formatter, maxLength }) {
	const [hasFocus, setHasFocus] = useState(!!placeholder || !!value)

	useEffect(() => {
		if(!!placeholder || !!value) {
			setHasFocus(true)
		}
	}, [placeholder, value])

	return <label className={cn(
		'block relative', 
		light ? 'md:bg-white' : 'md:bg-grey-100',
		className
	)}> 
		<span className={cn('text-black cursor-text leading-1 md:absolute md:leading-vw-1 md:transition-all md:duration-500', 
			hasFocus ? 'top-vw-0.5 left-vw-0.5 text-base md:text-vw-xs' : 'top-vw-1 left-vw-1 text-base md:text-vw-base'
		)}>
			{label}
			{required && '*'}
		</span>

		<input 
            type="text" 
			name={name} 
			value={formatter(value)} 
			onChange={handleChange}
			maxLength={maxLength}
			onFocus={() => !placeholder && setHasFocus(true)} 
			onBlur={e => !placeholder && setHasFocus(!!e.target.value)} 
			required={!!required} 
			disabled={disabled} 
			placeholder={placeholder} 
			className={cn(
				'block w-full p-3 mb-3 text-base leading-1 rounded md:mb-0 md:p-vw-0.5 md:pt-vw-1.5 md:text-vw-sm md:leading-vw-1',
				light ? 'bg-white' : 'bg-grey-100'
			)} 
		/>
	</label>
}