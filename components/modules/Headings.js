import cn from 'classnames'

export function H1({ className, children }) { 
	return <h1 className={cn('font-serif font-bold tracking-tight relative text-6xl md:text-vw-7xl', className)}>{children}</h1> 
}

export function H2({ className, children }) { 
	return <h2 className={cn('font-serif font-bold tracking-tight text-4xl md:text-vw-5xl', className)}>{children}</h2> 
}

export function H3({ className, children }) { 
	return <h3 className={cn('font-serif font-bold tracking-tight text-3xl md:text-vw-3xl', className)}>{children}</h3> 
}

export function H4({ className, children }) { 
	return <h4 className={cn('font-serif font-bold tracking-tight text-2xl md:text-vw-2xl', className)}>{children}</h4> 
}

export function H5({ className, children }) { 
	return <h5 className={cn('font-sans font-bold uppercase', className)}>{children}</h5> 
}

export function H6({ className, children }) { 
	return <h6 className={cn('font-sans uppercase text-sm md:text-vw-sm', className)}>{children}</h6> 
}