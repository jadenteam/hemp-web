import { useEffect } from 'react'

import { useRouter } from 'next/router'

import cn from 'classnames'
import useModal from '../../hooks/useModal'
import { IconButton } from './Button'

/**
 * Shop homepage hero banner 
 */
export default function ShopMobileFilters({ menu, sorters, sortedByLabel, onChangeSort }) {
	const { setModal, closeModal } = useModal()
	const router = useRouter()

	// close the modal on first load
	useEffect(() => {
		closeModal()
	}, [])

	function handleSortChange(newSort) {
		closeModal()
		onChangeSort(newSort)
	}

	function handleSubcategoryChange(link) {
		closeModal()
		router.push(link)
	}

	function handleOpen() {
		setModal({ 
			content: <FilterModal {...{ closeModal, menu, sorters, sortedByLabel, handleSortChange, handleSubcategoryChange }} />, 
			style: 'drawer' 
		})
	}

	return <div className="fixed bottom-8 right-8 md:hidden">
		<IconButton variant="white" icon="filter" className="shadow rounded-full p-2 bg-white" onClick={handleOpen} />
	</div>
}


/**
 * Display the quick buy info and payment form 
 */
function FilterModal({ closeModal, menu = [], sorters, sortedByLabel, handleSortChange, handleSubcategoryChange }) {
	return <div className="px-0 md:px-vw-3 pb-vw-2 flex flex-col h-full bg-grey-700 text-white">
		<section className="flex items-center text-2xl md:text-vw-2xl py-5">
			<IconButton variant="clear" onClick={closeModal} icon="back" className="" />
			<span className="ml-4 md:ml-vw-1 uppercase">Product filters</span>
		</section>
		<section className="px-8">
			<h2 className="mb-2">Select category</h2>
			<ul>
				{menu.map(item => <li key={item.id} className='mr-6 py-1 pl-5'>
					<button onClick={() => handleSubcategoryChange(item.link)} className={cn('uppercase', 
						item.active && 'text-grey-400')}>
						{item.label}
					</button>
				</li>)}
			</ul>
		</section>
		<section className="pt-8 px-8">
			<h2 className="mb-2">Sort by</h2>
			<ul>
				{sorters.map((item, i) => (
					<li key={i} className='mr-6 py-1 pl-5'>
						<button href={item.value} onClick={() => handleSortChange(item.value)}>
							<a className={cn('uppercase', item.value === sortedByLabel && 'text-grey-400')}>{item.label}</a>
						</button>
					</li>
				))}
			</ul>
		</section>
	</div>
}