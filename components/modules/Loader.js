import cn from 'classnames'

export default function Loader({ inline, loading, className, children }) {
	return <div className={cn(inline ? 'inline-grid' : 'grid', className)}>
		<span className={cn(
			'col-span-full row-span-full transition-all overflow-hidden', 
			loading && 'bg-grey-300 animate-pulse', 
			inline && 'rounded'
		)}>
			<span className={cn('transition-opacity', loading && 'opacity-0')}>
				{children}
			</span>
		</span>
	</div>
}