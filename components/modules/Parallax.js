import { Tween, Reveal, ScrollTrigger } from 'react-gsap'
import useBreakpoints from '../../hooks/useBreakpoints'

/** Slide left title lines on scroll */
export function ShopTitle({ line1, line2 }) {
	const { isMobile, isDesktop } = useBreakpoints()

	if(isMobile) {
		return <ScrollTrigger trigger="body" start="top top" end="400px top" scrub={0.5}>
			<Tween to={{ x: '-120%', ease: 'power1.inOut' }}>{line1}</Tween>
			<Tween to={{ x: '-160%', ease: 'power1.inOut' }}>{line2}</Tween>
		</ScrollTrigger>
	}

	if(isDesktop) {
		return <ScrollTrigger trigger="body" endTrigger="#shop-home-content" start="top top" end="top top" scrub={0.5}>
			<Tween to={{ x: '-110%', ease: 'power1.out' }}>{line1}</Tween>
			<Tween to={{ x: '-150%', ease: 'power1.out' }}>{line2}</Tween>
		</ScrollTrigger>
	}

	return false
}

/** Fade in element on scroll over */
export function FadeIn({ as, children, className }) { 
	const Element = as ? as : `div`

	return <Reveal threshold={0.2}>
		<Tween
			from={{
				y: '30px',
				opacity: 0
			}}
			to={{
				y: '0',
				opacity: 1,
			}}
			ease="power4.inOut"
			duration={1}
			stagger={0.2}
		>
			<Element className={className}>
				{children}
			</Element>
		</Tween>
	</Reveal>
}

