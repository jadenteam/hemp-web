import { useRouter } from 'next/router'

import { IconButton } from './Button'
import { NumberOption, SelectOption } from './Form'
import useProductOptions from '../../hooks/useProductOptions'
import Icon from './Icons'
import { ResponsiveImage } from './Image'

import { currency } from "../../utility/format";
import ManufacturerLink from './ManufacturerLink'
import ProductStockLabel from './ProductStock'

export default function ModalProduct({ cartID, id, product, initialQuantity, initialOptions, quickbuy, update, remove, loading }) {
	const { image, manufacturer, name, url_key, price, options, inStock, only_x_left_in_stock } = product
	const { isConfigurable, meta, quantity, selectedOptions } = useProductOptions(url_key, options, initialOptions, initialQuantity)

	const Element = (quickbuy ? `section` : `li`) // render element based on passed props

	const regular_price = currency.format(price.regular);

	return (
		<Element className="relative flex bg-grey-100 mt-5 md:mt-vw-2.5 mx-0">
			<div className="w-1/2 md:w-vw-15 md:h-vw-15 flex items-center">
				<div className="h-52 md:h-32 w-full" style={{ background: "url('" + image.url + "') center/contain no-repeat" }} />
			</div>
			<div className="flex flex-col p-4 md:p-vw-1 py-10">
				<span className="text-xs md:text-vw-sm leading-snug text-grey-500">
					<ManufacturerLink {...manufacturer} />
				</span>
				<div className="line-clamp-1 text-base md:text-vw-sm leading-snug">{name}</div>
				<div className="leading-snug">
					{price.isDiscounted && <span className="text-grey-500 line-through mr-1 md:mr-vw-0.3">{isConfigurable && !!meta.price ? currency.format(meta.price.regular) : regular_price}</span>}
					<span>
						{isConfigurable && !!meta.price ? 
							(meta.price.isDiscounted ? currency.format(meta.price.discounted) : currency.format(meta.price.regular))
						:
							(price.isDiscounted ? currency.format(price.discounted) : regular_price)
						}
					</span>
				</div>
				<div className="text-base md:text-vw-xs">
					<ProductStockLabel 
						inStock={isConfigurable ? meta.inStock : inStock} 
						only_x_left_in_stock={isConfigurable ? meta.only_x_left_in_stock : only_x_left_in_stock} 
					/>
				</div>
				<div className="text-base md:text-vw-xs flex flex-grow flex-col space-x-vw-1 mt-5">
					<label className="w-full  text-grey-400 uppercase md:self-end flex flex-col">
						<span className="text-xs">Quantity:</span>
						{loading ?
							<Icon type="loading" className="pt-vw-1 self-center h-vw-2" />
							:
							<NumberOption name="quantity" label="Quantity" value={quantity} setValue={value => update(id, product, 'quantity', value)} max={isConfigurable ? (meta.only_x_left_in_stock || 99) : (only_x_left_in_stock ? only_x_left_in_stock : 99)} />
						}
					</label>
					{options && options.map((data, i) => <label key={i} className="mt-2.5 w-full text-grey-400 uppercase self-end flex flex-col">
						<span className="text-xs">{data.label}:</span>
						{loading ?
							<Icon type="loading" className="pt-vw-1 self-center h-vw-2" />
							:
							<SelectOption
								name={data.attribute_code}
								values={data.values}
								selected={selectedOptions[data.attribute_code]}
								setSelected={e => update(id, product, 'size', e.target.value)}
							/>
						}
					</label>)}
				</div>
			</div>
			{!quickbuy && <IconButton onClick={() => remove(id, product)} icon="close" variant="white" className="button__removeFromCart absolute -top-vw-1 -right-vw-1 border rounded-full border-grey-400" />}
		</Element>
	)
}