import cn from 'classnames'
import Link from 'next/link'

import { H1 } from './Headings'
import Pips from './Pips'
import { ShopTitle } from './Parallax'

import useCarousel from '../../hooks/useCarousel'

/**
 * Shop homepage hero banner 
 */
export default function ShopHero({ carousel, indentTitle, banner, bannerActive }) {
	const { carouselIndex, carouselActive, goTo } = useCarousel(carousel.data, 'hero')

	return <section className="">
		<H1 className="md:text-vw-13xl mt-5">
			<ShopTitle 
				line1={
					<span 
						id="shop-title-trigger" 
						className={cn(
							'block fixed top-28 -z-1 whitespace-nowrap text-6xl md:text-vw-13xl',
							banner?.field_banner_active && bannerActive ? 'top-48 md:top-vw-11' : 'md:top-vw-7',
							indentTitle ? 'left-1/4 md:left-vw-20' : 'left-4 md:left-vw-4'
						)
					}>
						{carousel.title_line_1}
					</span>
				} 
				line2={
					<span className={cn(
						'block fixed z-10 top-48 whitespace-nowrap text-6xl md:text-vw-13xl',
						banner?.field_banner_active && bannerActive ? 'top-64 md:top-vw-28' : 'md:top-vw-23',
						indentTitle ? 'left-1/3 md:left-vw-40' : 'left-1/3 md:left-vw-25'
					)}>
						{carousel.title_line_2}
					</span>
				} 
			/>
		</H1>

		<div className="grid">
			{carousel.data.map((item, i) => <div key={i} className={cn(
				'col-span-full row-span-full transition-opacity duration-500',
				carouselIndex === i ? 'opacity-100' : 'opacity-0 -z-1'
			)}>
				<SlideImage image={item.image} />
				<SlideText text={item.description} />
				<SlideLink {...item.link} />
			</div>)}
		</div>

		<div className="absolute left-5 right-5 bottom-6 md:bottom-vw-16 md:left-vw-65 md:right-vw-7">
			<Pips active={carouselActive} total={carousel.data && carousel.data.length} counter={carouselIndex} goTo={goTo} showNumbers dark className="mt-3 md:mt-vw-1" />
		</div>

	</section>
}

export function SlideImage({ image }) {
	const bgImage = {backgroundImage: 'url(' + image.src + ')'}
	return <div className="absolute h-2/3 md:h-full top-11 md:top-vw-12 md:bottom-0 left-0 md:right-vw-35 right-vw-5 bg-cover bg-center" style={bgImage}></div>
}

export function SlideText({ text }) {
	return <div className="absolute bottom-vw-20 left-vw-65 right-vw-7 hidden md:block z-20">
		<p className="line-clamp-5">{text}</p>
	</div>
}

export function SlideLink({ title, url, description, subtext }) {
	return <Link href={url}>
		<a className="block absolute bottom-1/3 md:bottom-vw-14 right-5 left-5 md:left-vw-3 w-10/12 md:w-vw-25 bg-white px-5 md:px-vw-2 py-3 md:py-vw-1.5 z-20">
			<div className="font-black text-xl md:text-vw-xl">{title ? title : 'Find out more'}</div>
			<div className="text-base md:text-vw-sm whitespace-nowrap overflow-hidden overflow-ellipsis">{description}</div>
			<div className="text-base md:text-vw-sm">{subtext}</div>
		</a>
	</Link>
}