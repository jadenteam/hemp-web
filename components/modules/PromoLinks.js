import cn from 'classnames'
import Link from 'next/link'

import { H4 } from './Headings'
import { Button, IconButton } from './Button'
import Modal from './Modal'
import { ResponsiveImage } from './Image'

import useModal from '../../hooks/useModal'

export default function PromoLinks({ title, links }) {
	return <>
		<h2 className="text-2xl md:text-vw-lg uppercase text-center mb-10 md:mb-vw-3">{title}</h2>
		<ul className="grid md:grid-cols-3 gap-x-vw-2">
			{links.map((link, i) => <PromoLink key={i} {...link} />)}
		</ul>
	</>
}

export function PromoLink({ title, text, image, link }) {
	return <Link href={link.url}>
		<a>
			<li className="flex flex-col bg-grey-100 text-center overflow-hidden mb-10 md:mb-0">
				<H4 className="pt-5 md:pt-vw-3 px-vw-2 dark:text-black">{title}</H4>
				<div className="py-5 px-3 md:pb-vw-3 md:px-vw-2 text-grey-500 text-xm md:text-vw-sm">{text}</div>
				<div className="relative md:h-vw-25 h-vw-80">
					<ResponsiveImage {...{image}} objectFit="cover" layout="fill" />
				</div>
			</li>
		</a>
	</Link>
}