import cn from 'classnames'
import Link from 'next/link'
import { useState } from 'react'

import { Button } from '../../components/modules/Button'
import useBreakpoints from '../../hooks/useBreakpoints'
import setProductReview from '../../repository/setProductReview'
import { LabelledField } from './Form'
import { StarEmpty, StarFull, StarHalf } from './Icons'

/**
 * Display a products reviews
 */
export default function ProductReviews({ sku, reviews }) {
	const [isWriting, setIsWriting] = useState(false)
	const [submitted, setSubmitted] = useState(false)
	const [errors, setErrors] = useState([])
	const { isDesktop } = useBreakpoints()

	return <section>
		<div className="flex flex-col md:flex-row">
			<div className="flex-grow">
				<h2 className="text-xl md:text-vw-xl font-bold">Customer Reviews</h2>
			</div>
			<div className="py-5 md:p-0">
				{submitted ? 
					<div className="font-bold text-sm text-success py-4 md:py-vw-1">Thanks for submitting your review. It will be published shortly.</div>
				:
					<Button type="submit" variant={isDesktop && 'white'} size="small" onClick={() => setIsWriting(!isWriting)}>{isWriting ? 'Cancel' : 'Write a review'}</Button>
				}
			</div>
		</div>
		{isWriting && !submitted ?
			<div className="mx-auto w-11/12 pt-4 md:pt-vw-3">
				<h2 className="mb-4 md:mb-vw-2 text-lg md:text-vw-lg">Write a review</h2>
				<Form {...{sku, setErrors, setSubmitted}} />
				<div className="text-error">{errors.map((err, i) => <span key={i}>{err.message}</span>)}</div>
			</div>
		: 
			!!reviews.count ? 
				<>
					<Rating rating={reviews.average} label={`Based on ${reviews.count} Reviews`} />
					<ul>
						{reviews.items.map((review, i) => <Review key={i} {...review} />)}
					</ul>
				</> 
			: 
				<span>No reviews yet</span>
		}
	</section>
}

export function Rating({ rating, label, className }) {
	return <div className={cn('flex items-center text-sm', className)}>
		<span><Stars {...{rating}} /></span>
		<span className="ml-3 md:ml-vw-1">{label}</span>
	</div>
}

export function Review({ nickname, summary, text, created_at, average_rating }) {
	return <article className="border-t border-grey-300 mt-8 pt-8 md:mt-vw-2.5 md:pt-vw-2.5">
		<Rating rating={average_rating} />
		<h3 className="text-lg md:text-vw-lg mb-2 md:mb-vw-1">{summary}</h3>
		<section className="text-sm text-grey-500 mb-1 md:mb-vw-0.5">{nickname} on {created_at}</section>
		<p>{text}</p>
	</article>
}

export function Stars({ rating }) {
	const max = 5
	const score = rating / (100/max)
	const diff = max - score

	const full = (rating) => Array.from(Array(Math.floor(rating)))
	const half = (rating) => rating % 1 !== 0
	const empty = (rating) => Array.from(Array(Math.floor(rating)))

	return <div className="flex justify-center items-center space-x-1">
		{full(score).map((star, i) => <StarFull key={i} />)}
		{half(score) && <StarHalf />}
		{empty(diff).map((star, i) => <StarEmpty key={i} />)}
	</div>
}

export function Form({ sku, setErrors, setSubmitted }) {
	const [values, setValues] = useState({
		name: '',
		rating: -1, // default no stars selected
		title: '',
		body: ''
	})

	function handleChange(e) {
		setValues(prevState => ({
			...prevState,
			[e.target.name]: e.target.value
		}))
	}

	async function handleSubmit(e) {
		e.preventDefault()
		console.log('handleSubmit', values)

		const response = await setProductReview(sku, values)
		console.log('setProductReview', response)

		if(response.errors) {
			setErrors(response.errors)
		} else {
			setSubmitted(true)
		}
	}

	return <form onSubmit={handleSubmit}>
		<LabelledField className="mb-4 md:mb-vw-2" type="text" label="Name" placeholder="enter your name" name="name" value={values.name} onChange={handleChange} required />
		<div className="mb-4 md:mb-vw-2 ml-2 md:ml-vw-0.5">
			<h3 className="text-xs md:text-vw-xs">Rating</h3>
			<div className="flex">
				{[...new Array(5)].map((star, i) => <label key={i} className="flex cursor-pointer mr-2 md:mr-vw-0.5" onClick={() => setValues(prevState => ({ ...prevState, rating: i + 1 }))}>
					<input className="fixed opacity-0 pointer-events-none" type="radio" name="rating" value={i} />
					{values.rating > i ? <StarFull /> : <StarEmpty />}
				</label>)}
			</div>
		</div>
		<LabelledField className="mb-4 md:mb-vw-2" type="text" label="Review title" placeholder="give this review a title" name="title" value={values.title} onChange={handleChange} required />
		<LabelledField className="mb-4 md:mb-vw-2" type="textarea" label="Review text" placeholder="write your comments here..." name="body" value={values.body} onChange={handleChange} required />
		<Button type="submit" variant="black">Submit</Button>
	</form>
}