import Link from 'next/link'

/**
 * Display a link to a manufacturer list page 
 */
export default function ManufacturerLink({ label }) {
	return <Link href={getManufacturerPageURL(label)}>
		<a>{label}</a>
	</Link>
}

export const getManufacturerPageURL = label => `/shop/brand/${label}`