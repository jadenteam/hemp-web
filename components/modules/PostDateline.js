import cn from 'classnames'
import Link from 'next/link'
import parse from 'html-react-parser'

import { format } from '../../utility/dates'

/**
 * Display the post categories and date 
 */
export default function PostDateline({ className, categories='', date='' }) {

	return <div className={cn('mt-3 mb-1 text-sm md:mt-vw-1 md:mb-vw-0.5 md:text-vw-sm', className)}>
		{!!categories.length && <CategoryLine {...{categories}} />}
		{!!categories.length && date && ' / '}
		{date && <span className="font-bold">{format(date)}</span>}
	</div>
}

/**
 * Display comma-separated list of categories
 */
export function CategoryLine({ categories }) {
	return <ul className="inline-flex">
		{categories.map((cat, i, arr) => <li key={i}>
			<Link href={`/blog/category/${cat.slug}`}>
				<a dangerouslySetInnerHTML={{ __html: cat.name }} />
			</Link>
			{arr.length - 1 !== i && <span>,&nbsp;</span>}
		</li>)}
	</ul>
}