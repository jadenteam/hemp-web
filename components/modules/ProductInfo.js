import cn from 'classnames'
import { useRouter } from 'next/router'
import { Tween } from 'react-gsap'

import { Button } from './Button'
import { Rating } from './ProductReviews'
import { NumberOption, SelectOption } from './Form'
import QuickBuy from './QuickBuy'
import Cart from './Cart'

import useModal from '../../hooks/useModal'
import useElementDimensions from '../../hooks/useElementDimensions'
import useBreakpoints from '../../hooks/useBreakpoints'
import useRouterRefresh from '../../hooks/useRouterRefresh'

import { addToCart } from '../../repository/getCart'
import useProductOptions from '../../hooks/useProductOptions'
import { useCartContext } from '../../context/cart'

import { currency, stringCurrency } from "../../utility/format";
import ManufacturerLink from './ManufacturerLink'
import SocialShare from './SocialShare'
import ProductStockLabel from './ProductStock'
import { useDataLayer } from '../../hooks/useDataLayer'

/**
 * Display single product item
 */
export default function ProductInfo({ product, className, style, initialisedOption }) {
	const { sku, manufacturer, url_key, name, image, inStock, salableQuantity, only_x_left_in_stock, price, short_description, reviews, options, colours, configurable_options } = product
	const { cart, setCart } = useCartContext()

	const router = useRouter()

	const refresh = useRouterRefresh()
	const { isMobile, isDesktop } = useBreakpoints()
	const { setModal, closeModal } = useModal()
	const { isConfigurable, meta, quantity, setQuantity, selectedOptions, handleOptionChange } = useProductOptions(url_key, options, initialisedOption, 1, product)

	const [changeDataLayer] = useDataLayer()

	/** Open the QuickBuy drawer modal */
	function openModal() {
		setModal({
			content: <QuickBuy {...{ product, closeModal }} initialOptions={selectedOptions} initialQuantity={quantity} />,
			style: 'drawer'
		})
	}

	function handleAddToCart(e) {
		e.preventDefault()
		addToCart(cart.id, sku, quantity, selectedOptions)
			.then(updatedCart => {
				const { name, price, manufacturer } = product
				setCart(updatedCart)
				setModal({
					content: <Cart {...{ closeModal }} />,
					style: 'drawer'
				})
				try{
					changeDataLayer({
						dataLayer: {
							event: "add_to_cart",
							ecommerce: {
								items: [{
									item_id: sku,
									item_name: name,
									item_brand: manufacturer.label,
									affiliation: process.env.SITE_KEY,
									price: stringCurrency(price.regular)
								}]
							}
						}
					})
				} catch(err) {
					// Failed to process event - ignore
				}
			})
			.catch(err => console.error('CATCH', err))
			.finally(() => refresh())
	}

	const { ref, height } = useElementDimensions()

	// Prevent double processing of format
	const regular_price = currency.format(price.regular || price.discounted);

	return <article className={cn('', className)} style={style}>
		<Tween
			progress={isMobile && 0} // disable animation on mobile
			to={{
				opacity: 0,
				scrollTrigger: {
					trigger: '#product-info-1',
					start: '-100 top',
					end: 'top top',
					scrub: true,
				}
			}}
		>
			<div id="product-info-1" className="flex">
				{!!reviews.count && <a href="#reviews">
					<Rating className="uppercase" rating={reviews.average} label={`${reviews.count} Reviews`} />
				</a>}
				<div className="flex-grow flex justify-end space-x-4">
					<SocialShare productUrlKey={url_key} shareOptions={['facebook', 'twitter', 'email']} />
				</div>
			</div>
			<div className="uppercase tracking-wider text-sm mt-10 md:text-vw-sm md:mt-vw-3"><ManufacturerLink {...manufacturer} /></div>
		</Tween>

		<Tween
			progress={isMobile && 0} // disable animation on mobile
			to={{
				y: isDesktop ? height : 0, // disable animation on mobile
				scrollTrigger: {
					trigger: '#product-info-1',
					start: 'top top',
					end: '100 top',
					scrub: true,
				}
			}}
		>
			<h1 className="font-bold text-4xl md:text-vw-3xl">{name}</h1>
			<div className="mt-5 md:mt-vw-1.5">
				<span className="text-lg md:text-vw-lg">
					{isConfigurable && !!meta.price ? 
						(meta.price.isDiscounted ? currency.format(meta.price.discounted) : currency.format(meta.price.regular || price.discounted))
					:
						(price.isDiscounted ? currency.format(price.discounted) : (regular_price || price.discounted))
					}
				</span>
				{meta.isDiscounted && <span className="text-grey-500 line-through ml-3 md:ml-vw-1">{isConfigurable && !!meta.price ? currency.format(meta.price.regular || price.discounted) : regular_price}</span>}
			</div>
		</Tween>

		<Tween
			progress={isMobile && 0} // disable animation on mobile
			to={{
				opacity: 0,
				scrollTrigger: {
					trigger: '#product-info-1',
					start: '-100 top',
					end: 'top top',
					scrub: true
				}
			}}
		>
			<div ref={ref} id="product-info-3">
				<div className="text-sm md:text-vw-sm">
					<ProductStockLabel
						inStock={isConfigurable ? meta.salableQuantity > 0 : salableQuantity > 0} 
						only_x_left_in_stock={isConfigurable ? meta.only_x_left_in_stock : only_x_left_in_stock} 
					/>
				</div>
				{!!short_description && <p className="text-sm md:text-vw-sm line-clamp-3 mt-5 md:mt-vw-1.5" dangerouslySetInnerHTML={{ __html: short_description }} />}
			</div>
		</Tween>

		<hr className="border-grey-300 mt-5 md:mt-vw-1.5" />

		{/* Even if not in stock, we still want to have some sort of usability with this product */}
		{/* We will feed the inStock flag down into some components and disable them rather than hide this entire section */}
		{<form className="mt-5 md:mt-vw-1">
			<div className="flex flex-wrap justify-between">
				<label className="w-1/3 text-grey-400 uppercase">
					<span className="text-base md:text-vw-sm">Quantity:</span>
					<NumberOption name="quantity" label="Quantity" value={quantity} setValue={setQuantity} max={isConfigurable ? meta.salableQuantity : salableQuantity} />
				</label>
				{!!colours.length && <label className="w-1/3 text-grey-400 uppercase">
					<span className="text-base md:text-vw-sm">Colour:</span>
					<SelectOption
						name="colour"
						values={colours}
						selected={url_key}
						setSelected={e => router.push(`/shop/product/${e.target.value}`)}
					/>
				</label>}
				{options && options.map((data, i) => <label key={i} className="w-1/3 text-grey-400 uppercase self-end">
					<span className="text-base md:text-vw-sm">{data.label}:</span>
					<SelectOption
						name={data.attribute_code}
						values={data.values}
						selected={selectedOptions[data.attribute_code]}
						setSelected={handleOptionChange}
					/>
				</label>)}
			</div>
			<div className="mt-8 md:mt-vw-2 flex md:flex-row flex-col md:space-x-vw-2">
				<Button onClick={handleAddToCart} type="submit" disabled={isConfigurable ? (meta.salableQuantity === 0 || !meta.salableQuantity) : (salableQuantity === 0 || !salableQuantity)} className="button__addToCart mt-5 md:mt-0">Add to Cart</Button>
			</div>
		</form>}
	</article>
}