import Link from "next/link";
import cn from 'classnames'

import { H1 } from './Headings'
import { Button, IconButton } from './Button'
import Pips from './Pips'
import { ResponsiveImage } from './Image'

import useCarousel from '../../hooks/useCarousel'
import useBreakpoints from '../../hooks/useBreakpoints'

export default function Promo({ data, className, reverse }) {
	const carousel = useCarousel(data, 'hero')
	const { isMobile, isDesktop } = useBreakpoints()

	return <div className="relative">
		<div className="grid">
			{data.map((promo, i) => <Slide key={i} counter={i} {...promo} {...{className, carousel, reverse}} />)}
		</div>

		{isMobile && <div className="px-5 md:px-0 mt-5">
			<Pips active={carousel.carouselActive} total={data.length} counter={carousel.carouselIndex} goTo={carousel.goTo} showNumbers dark />
		</div>}

		{isDesktop && <div className={cn(
			'w-full md:w-vw-20 absolute bottom-vw-6',
			reverse ? 'right-0 md:pl-vw-2' : 'left-0 md:pr-vw-2'
		)}>
			<Pips active={carousel.carouselActive} total={data.length} counter={carousel.carouselIndex} goTo={carousel.goTo} showNumbers dark />
		</div>}
	</div>
}

export function Slide({ counter, carousel, className, reverse, tagline, title, text, video, link, image }) {
	const { isMobile, isDesktop } = useBreakpoints()

	return <div className={cn(
		'col-span-full row-span-full flex flex-col transition-opacity duration-400 overflow-hidden md:overflow-visible', 
		reverse ? 'md:flex-row-reverse' : 'md:flex-row',
		counter === carousel.carouselIndex ? 'opacity-100' : 'opacity-0 -z-1',
		className
	)}>
		<section className={cn(
			'w-full md:w-vw-20',
			reverse ? 'md:pl-vw-2' : 'md:pr-vw-2'
		)}>
			<p className={cn(
				'text-grey-500 text-sm mb-vw-2 px-5 md:px-0',
				reverse ? 'text-right' : 'text-left'
			)}>{tagline}</p>
			<H1 className={cn(
				'relative z-10 whitespace-nowrap md:text-vw-10xl px-5 md:px-0',
				(reverse && isDesktop) && 'rtl'
			)}>{title}</H1>

			{isMobile && <ResponsiveImage image={image} alt="" className="-mt-6 md:mt-0" />}

			<p className="line-clamp-4 my-5 md:my-0 px-5 md:px-0">{text}</p>
			<div className="px-5 md:px-0">
				{video && <IconButton variant="black" className="my-4 md:mt-vw-2" onClick={openModal} icon="play">{link.title}</IconButton>}
				{!!link &&
					<Link href={link.url}>
						<a target={link.target}>
							<Button variant="black" className="mt-vw-2">{link.title}</Button>
						</a>
					</Link>
				}
			</div>
		</section>
		{isDesktop && <ResponsiveImage image={image} alt="" className="flex-grow inline-block" />}
	</div>
}