import cn from 'classnames'

import { H1 } from './Headings'
import { Button, IconButton } from './Button'
import Pips from './Pips'
import ScreenTint from './ScreenTint'

import useBreakpoints from '../../hooks/useBreakpoints'
import useCarousel from '../../hooks/useCarousel'

/**
 * Homepage hero banner with carousel 
 */
export default function Hero({ carousel }) {
	const { isMobile, isDesktop } = useBreakpoints()
	const { carouselIndex, carouselActive, goTo, goToNext, goToPrev, animateFlick } = useCarousel(carousel, 'hero')

	return <section className="absolute inset-0">

		{/* background images */}
		{carousel && carousel.map((slide, i) => <SlideBackground key={i} counter={i} carouselIndex={carouselIndex} image={slide.image} /> )}

		{/* tint over background image */}
		<ScreenTint />

		{/* slides */}
		{carousel && carousel.map((slide, i) => <Slide key={i} {...slide} {...{isDesktop}}
			isAnimating={carouselActive}
			counter={i}
			animate={animateFlick} 
		/>)}

		{/* navigation buttons */}
		{carouselActive && 
			<div className="hidden md:flex px-5 md:absolute md:px-0 md:top-vw-11 md:left-vw-7">
				<IconButton variant="white" onClick={goToPrev} icon="prev" />
				<IconButton variant="white" onClick={goToNext} className="ml-2 md:ml-vw-0.5" icon="next" />
			</div>
		}

		{/* call to action and pips  */}
		<div className="inline-block absolute bottom-5 left-3 right-3 md:bottom-vw-3 md:left-vw-7 md:right-auto md:w-vw-30">
			<Button variant="white" href={carousel && carousel[carouselIndex] && carousel[carouselIndex].link.url} target={carousel[carouselIndex].link.target} className="w-full justify-center">{carousel[carouselIndex].link.title}</Button>
			<Pips active={carouselActive} total={carousel && carousel.length} counter={carouselIndex} goTo={goTo} showNumbers className="mt-3 md:mt-vw-2" />
		</div>

	</section>
}

/**
 * Display single carousel slide 
 */
export function Slide({ isDesktop, title, title2, heading, description, subtext, counter, isAnimating, animate }) {
	return <>
		<div className="h-full absolute inset-0">
			<div className="absolute top-44 bottom-0 text-white px-5 w-full md:top-vw-14 md:pt-vw-15 md:px-vw-7 md:pb-vw-6 md:w-3/4">
				<H1 className="md:absolute top-0">
					{isDesktop ? 
						<>
							<TitleLine {...{isAnimating}} className={animate('', counter)}>{title}</TitleLine>
							<TitleLine {...{isAnimating}} className={animate('wait-100', counter)}>{title2}</TitleLine>
						</>
					: <span className={animate('text-7xl', counter)}>{title} {title2}</span>}
				</H1>
				{/* TODO: add link around this box  */}
				<div className={animate('mt-6 wait-200 md:mt-0 md:wait-200', counter)}>
					<p className="text-xl md:text-vw-xl font-black">{heading}</p>
					<p className="text-lg md:w-1/2 md:text-vw-lg">{description}</p>
					<p className="text-lg md:text-vw-lg">{subtext}</p>
				</div>
			</div>
		</div>
	</>
}

/**
 * Display single carousel slide background. Separated from Slide component in order to apply different animation. 
 */
export function SlideBackground({ counter, carouselIndex, image }) {
	const bgImage = {backgroundImage: 'url(' + image.src + ')'}
	return <div className={cn('absolute inset-0 bg-cover bg-center transition-opacity duration-500', 
		carouselIndex >= counter ? 'opacity-100' : 'opacity-0 -z-1' 
	)} style={bgImage}></div>
}

/**
 * Single carousel slide title line 
 */
export function TitleLine({ className, children, isAnimating }) {
	return <span className="block whitespace-nowrap">
		<span className={cn(isAnimating && 'relative top-7 md:top-vw-7', className)}>{children}</span>
	</span>
}