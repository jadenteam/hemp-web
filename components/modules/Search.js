import { useState } from 'react'
import Link from 'next/link'
import cn from 'classnames'

import { IconButton } from './Button'
import SearchForm from './SearchForm'
import { getURLByID } from '../../repository/getStores'
import ProductStockLabel from './ProductStock'
import ManufacturerLink from './ManufacturerLink'
import { currency } from "../../utility/format"

/**
 * Display the search form and results
 */
export default function Search({ closeModal }) {
	const [results, setResults] = useState([])

	return <article className="p-vw-4 flex flex-col h-full">
		<section className="flex items-center text-2xl md:text-vw-2xl py-5">
			<IconButton variant="white" onClick={closeModal} icon="back" className="" />
			<span className="ml-4 md:ml-vw-1 uppercase">Product search</span>
		</section>
		<SearchForm {...{setResults}} />
		<div className="overflow-y-scroll pl-5 md:pl-0 pr-7 scrollbar-thin scrollbar-thumb-grey-300 scrollbar-track-transparent scrollbar-thumb-rounded-full">
			<Results products={results} />
		</div>
	</article>
}

/**
 * Display the search results
 */
function Results({ products }) {
	return products.map(({ id, name, url_key, image, category, manufacturer, price }, i) => {
		const url = getURLByID(category)
		const regular_price = currency.format(price.regular);

		return <Link href={`${url}/shop/product/${url_key}`} key={id}>
			<a key={i} className="relative flex bg-grey-100 mb-5 md:mb-vw-1.5 mx-0">
				<div className="w-1/3 flex">
					<img src={image.url} alt={image.label} className="block w-full object-cover" />
				</div>
				<div className="w-2/3 flex flex-col p-4 md:p-vw-1 py-10">
					<span className="text-xs md:text-vw-sm leading-snug text-grey-500">
						<ManufacturerLink {...manufacturer} />
					</span>
					<div className="line-clamp-1 text-base md:text-vw-sm leading-snug">{name}</div>
					<div className="leading-snug">
						{price.hasFromPrice && <span className="mr-1 md:mr-vw-0.3">from</span>}
						{price.isDiscounted && <span className="text-grey-500 line-through mr-1 md:mr-vw-0.3 text-xs md:text-vw-base">{regular_price}</span>}
						<span className="text-xs md:text-vw-base">{price.isDiscounted ? currency.format(price.discounted) : regular_price}</span>
					</div>
				</div>
			</a>
		</Link>
	})
}