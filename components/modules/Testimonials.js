import cn from 'classnames'

import useCarousel from '../../hooks/useCarousel'

import { H2, H3 } from './Headings'
import { IconButton } from './Button'
import Pips from './Pips'
import { ResponsiveImage } from './Image'

import useBreakpoints from '../../hooks/useBreakpoints'

/**
 * Display testimonial carousel section
 */
export default function Testimonials({ title, description, data, className }) {
	const carousel = useCarousel(data, 'hero')
	const { isMobile, isDesktop } = useBreakpoints()

	return <section className={cn('my-20 md:my-vw-8', className)}>
		<H2>{title}</H2>
		<p>{description}</p>

		<div className="block relative mt-vw-1">
			{(carousel.carouselActive && isDesktop) && <div className="absolute top-2 right-0 flex z-20 md:top-vw-1">
				<IconButton variant="black" onClick={carousel.goToPrev} icon="prev" />
				<IconButton className="ml-2 md:ml-vw-0.5" variant="black" onClick={carousel.goToNext} icon="next" />
			</div>}
			<ul className="grid">
				{data.map((testimonial, i) => <Quote key={i} counter={i} {...testimonial} {...{carousel}} />)}
			</ul>
			<Pips active={carousel.carouselActive} total={data.length} counter={carousel.carouselIndex} goTo={carousel.goTo} dark extend className={'md:absolute bottom-0 right-0 md:w-2/3 pl-vw-2.5'} />
		</div>
	</section>
}

/**
 * A single slide in the testimonial carousel 
 */
export function Quote({ counter, carousel, title, quote, author, image, logo }) {
	return <li className="col-span-full row-span-full w-full flex flex-col md:flex-row md:space-x-vw-4 overflow-hidden">
		<div className={cn('relative z-20 w-full md:w-1/3 bg-white transition-opacity duration-400 overflow-hidden',
			counter === carousel.carouselIndex ? 'opacity-100' : 'opacity-0'
		)}>
			<ResponsiveImage image={image} alt={author} layout="fill" objectFit="cover" className="h-72 md:h-vw-30" />
		</div>
		<div className={carousel.animateSlide('w-full md:w-2/3 py-5 border-t-2 border-grey-200 md:border-t-vw-2 flex items-center', counter)}>
			<div>
				<H3 className="mb-vw-0.5">{title}</H3>
				<div dangerouslySetInnerHTML={{ __html: quote }} className="w-full text-sm md:text-vw-xl leading-relaxed" />
				<div className="my-vw-1 text-xs md:text-vw-sm">&mdash;&mdash; {author}</div>
				<ResponsiveImage image={logo} alt="" className="mt-3 w-1/3 md:w-vw-8" />
			</div>
		</div>
	</li>
}