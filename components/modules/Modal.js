import { useEffect, useRef } from 'react'

import ScreenTint from './ScreenTint'
import useOutsideClickTrigger from '../../hooks/useOutsideClickTrigger'
import { Close } from './Icons'

/**
 * Display a modal window with fixed tinted background
 * Used via ModalProvider, not to be imported into components  
 */
export default function Modal({ content, style = 'popup', close }) {

	const modalRef = useRef(null)
	useOutsideClickTrigger(modalRef, close)

	useEffect(async () => {
		const bind = async e => {
			if(e.keyCode !== 27) return
			if(document.activeElement && ['INPUT', 'SELECT'].includes(document.activeElement.tagName)) return
			close()
		}
	
		document.addEventListener('keyup', bind)
		return () => document.removeEventListener('keyup', bind)
	}, [content, close])

	// modal style depending on style prop
	const modalStyles = {
		'drawer' : Drawer,
		'popup' : Popup,
		'fullscreen' : Fullscreen
	}
	const ModalElement = modalStyles[style]

	return <ScreenTint fixed dark className="z-50 flex justify-center items-center cursor-pointer">
		<ModalElement {...{modalRef, content, close}} />
	</ScreenTint>
}

/**
 * Pop-up box style modal
 */
export function Popup({ modalRef, content, close }) {
	return <>
		{/* close button - redundant due to useOutsideClickTrigger but we'll keep it for looks */}
		<div className="absolute top-0 right-0 text-white z-10" onClick={close}>
			<Close className="w-4 md:w-vw-1 h-4 md:h-vw-1 m-3 md:m-vw-2" light />
		</div>

		{/* modal */}
		<div ref={modalRef} className="animate-modal-popup-in bg-white max-w-full">{content}</div>
	</>
}

/**
 * Fullscreen style modal
 */
export function Fullscreen({ modalRef, content, close }) {
	return <div ref={modalRef} className="w-screen h-screen animate-tint-in bg-white max-w-full">
		<div className="absolute top-5 md:top-vw-2 right-5 md:right-vw-2 bg-grey-100 rounded-full text-white z-10" onClick={close}>
			<Close className="w-4 md:w-vw-1 h-4 md:h-vw-1 m-3 md:m-vw-2" />
		</div>
		{content}
	</div>
}

/**
 * Pull-out drawer style modal
 */
export function Drawer({ modalRef, content }) {
	return (
		<div ref={modalRef} className="animate-modal-drawer-in w-full md:w-vw-40 bg-white absolute top-0 bottom-0 right-0 cursor-auto">
			{content}
		</div>
	)
}