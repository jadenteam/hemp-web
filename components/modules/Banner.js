export default function Banner({ banner, bannerActive, closeBanner }) {

    if(bannerActive && banner?.field_banner_active) {
        return (
            <div className="relative">
                <div className="h-20 md:h-12 p-vw-0.5 w-full flex flex-col md:flex-row justify-center items-center fixed z-20" style={{ backgroundColor: banner.field_banner_colour_bg }}>
                    <span style={{ color: banner.field_banner_text_colour }} className="mr-vw-1 uppercase">{banner.field_banner_text}</span>
                    <a href={banner.field_banner_button_link.url} style={{ backgroundColor: banner.field_banner_button_colour_bg }} className="rounded-full px-vw-3 py-1.5 text-xs uppercase strong flex align-center">
                        <button>{banner.field_banner_button_text}</button>
                    </a>
                    <img 
                        src="/images/close-light.svg" 
                        className="absolute right-vw-3 md:right-vw-1 cursor-pointer w-vw-3 h-vw-3 md:w-vw-1.5 md:h-vw-1.5"
                        onClick={closeBanner}
                    />
                </div>
                <div className="h-20 md:h-12 relative z-10" />
            </div>
        )
    } else {
        return <div />
    }
}