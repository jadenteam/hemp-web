import cn from "classnames"

export default function Icon({ type, light, className }) {
	switch(type) {
		case 'close' : 
			return <Close {...{className}} />
		case 'back' : 
			return <Back {...{light, className}} />
		case 'next' : 
			return <Next {...{light, className}} />
		case 'prev' : 
			return <Prev {...{light, className}} />
		case 'play' : 
			return <Play {...{className}} />
		case 'add' : 
			return <Add {...{className}} />
		case 'remove' : 
			return <Remove {...{className}} />
		case 'down' : 
			return <Down {...{className}} />
		case 'filter':
			return <Filter {...{className}} />
		case 'loading' : 
			return <Loading {...{light, className}} />
		case 'facebook':
			return <Facebook {...{light, className}} />
		case 'instagram':
			return <Instagram {...{light, className}} />
		case 'youtube':
			return <Youtube {...{light, className}} />
		case 'twitter':
			return <Twitter {...{light, className}} />
		case 'email':
			return <Email {...{light, className}} />
		case 'hamburger':
			return <Hamburger {...{light, className}} />
		case 'shopbag':
			return <ShopBag {...{light, className}} />
		case 'search':
			return <Search {...{light, className}} />
		default :
			return false
	}
}

export const Play 		= ({ className }) => <img className={className} src="/images/play-button-dark.svg" alt="Play" />
export const Down 		= ({ className }) => <img className={className} src="/images/option-dropdown.svg" alt="Open" />
export const Add 		= ({ className }) => <img className={className} src="/images/option-plus.svg" alt="Add" />
export const Remove 	= ({ className }) => <img className={className} src="/images/option-minus.svg" alt="Remove" />
export const StarFull 	= ({ className }) => <img className={className} src="/images/star-full.svg" alt="*" />
export const StarHalf 	= ({ className }) => <img className={className} src="/images/star-half.svg" alt="1/2" />
export const StarEmpty 	= ({ className }) => <img className={className} src="/images/star-empty.svg" alt="" />
export const Filter 	= ({ className }) => <img className={className} src="/images/filter.svg" alt="" />
export const Loading 	= ({ light, className }) => <img className={cn('w-4 md:w-vw-1', className)} src={`/images/loading-${light ? 'light' : 'dark'}.svg`} alt="Loading..." />
export const Close 		= ({ light, className }) => <img className={className} src={`/images/close${light ? '-light' : ''}.svg`} alt="Close" />
export const Back 		= ({ light, className }) => <img className={className} src={`/images/arrow-big-left.svg`} alt="Back" />
export const Next 		= ({ light, className }) => <img className={className} src={`/images/arrow-right-${light ? 'light' : 'dark'}.svg`} alt="Next" />
export const Prev 		= ({ light, className }) => <img className={className} src={`/images/arrow-left-${light ? 'light' : 'dark'}.svg`} alt="Previous" />
export const Facebook 	= ({ light, className }) => <img className={className} src={`/images/icon-facebook${light ? '-light' : ''}.svg`} alt="Facebook" />
export const Instagram 	= ({ light, className }) => <img className={className} src={`/images/icon-instagram${light ? '-light' : ''}.svg`} alt="Instagram" />
export const Youtube 	= ({ light, className }) => <img className={className} src={`/images/icon-youtube${light ? '-light' : ''}.svg`} alt="YouTube" />
export const Twitter 	= ({ light, className }) => <img className={className} src={`/images/icon-twitter${light ? '-light' : ''}.svg`} alt="Twitter" />
export const Email 		= ({ light, className }) => <img className={className} src={`/images/icon-email${light ? '-light' : ''}.svg`} alt="Email" />
export const Hamburger 	= ({ light, className }) => <img className={className} src={`/images/icon-hamburger${light ? '-light' : ''}.svg`} alt="Menu" />
export const ShopBag 	= ({ light, className }) => <img className={className} src={`/images/icon-shop-${light ? 'light' : 'dark'}.svg`} alt="Shop" />
export const Search 	= ({ light, className }) => <img className={className} src={`/images/search-${light ? 'light' : 'dark'}.svg`} alt="Search" />