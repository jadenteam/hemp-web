import { useEffect, useState } from 'react'
import cn from 'classnames'
import { IconButton } from './Button'
import Icon from './Icons'

export function LabelledField({ type, label, placeholder, name, required, light, className, value, options=[], onChange, disabled=false, error=null }) {
	const [hasFocus, setHasFocus] = useState(!!placeholder || !!value)

	// listen for changes to placeholder/value and update focus 
	useEffect(() => {
		if(!!placeholder || !!value) {
			setHasFocus(true)
		}
	}, [placeholder, value])

	return <label className={cn(
		'block relative', 
		light ? 'md:bg-white' : 'md:bg-grey-100',
		className
	)}> 
		<span className={cn('text-black cursor-text leading-1 md:absolute md:leading-vw-1 md:transition-all md:duration-500', 
			hasFocus ? 'top-vw-0.5 left-vw-0.5 text-base md:text-vw-xs' : 'top-vw-1 left-vw-1 text-base md:text-vw-base'
		)}>
			{label}
			{required && '*'}
		</span>

		{type === 'textarea' ? 
			<textarea name={name} 
				rows="6"
				value={value}
				onFocus={() => !placeholder && setHasFocus(true)} 
				onBlur={e => !placeholder && setHasFocus(!!e.target.value)} 
				onChange={onChange}
				required={!!required} 
				disabled={disabled} 
				placeholder={placeholder} 
				className={cn(
					'block w-full p-3 mb-3 text-base leading-1 md:mb-0 md:p-vw-0.5 md:pt-vw-1.5 md:text-vw-sm md:leading-vw-1', 
					light ? 'bg-white' : 'bg-grey-100'
				)}></textarea> 
		: type === 'select' ? 
			<>
				<select 
					name={name}
					value={value}
					onChange={e => onChange(e, e.target)} 
					className={cn(
						'block w-full p-3 mb-3 text-base leading-1 md:mb-0 md:p-vw-0.5 md:pt-vw-1.5 md:pr-vw-2 md:text-vw-sm md:leading-vw-1 cursor-pointer appearance-none', 
						light ? 'bg-white' : 'bg-grey-100'
				)}>
					{options && options.map(opt => <option key={opt.id} value={opt.code} data-id={opt.id}>{opt.name}</option>)}
				</select>
				<Icon className="absolute top-12 right-3 md:top-1/2 md:right-vw-0.5 w-2 md:w-vw-0.7 cursor-pointer" type="down" />
			</>
		:
			<input type={type} 
				name={name} 
				value={value} 
				onChange={onChange} 
				onFocus={() => !placeholder && setHasFocus(true)} 
				onBlur={e => !placeholder && setHasFocus(!!e.target.value)} 
				required={!!required} 
				disabled={disabled} 
				placeholder={placeholder} 
				className={cn(
					'block w-full p-3 mb-3 text-base leading-1 rounded md:mb-0 md:p-vw-0.5 md:pt-vw-1.5 md:text-vw-sm md:leading-vw-1',
					light ? 'bg-white' : 'bg-grey-100'
				)} />
		}
		{error && <span className="text-error">{error}</span>}
	</label>
}

export function SelectOption({ name, values, selected, setSelected }) {
	return <div className="relative text-black normal-case h-10 md:h-vw-2 text-xs md:text-vw-sm flex">
		<select onChange={setSelected} value={selected} className="appearance-none cursor-pointer bg-transparent pr-6" name={name}>
			{values.map((val, i) => <option key={i} className="p-0" value={val.uid}>{val.label}</option>)}
		</select>
		<Icon className="relative right-4 -z-1 w-2 md:w-vw-0.7 cursor-pointer" type="down" />
	</div>
}

export function NumberOption({ name, value=1, setValue, min=1, max=99 }) {
	function handleChange(e, newValue) {
		e.preventDefault()

		// prevent value lower than min
		if(newValue < min) {
			return setValue(min)
		}

		// prevent value higher than max 
		if(newValue > max) {
			return setValue(max)
		}

		// check if newValue is a valid number (int or string format)
		if(newValue >>> 0 === parseFloat(newValue)) {
			return setValue(newValue) 
		}
	}

	return <div className="flex items-center text-black h-vw-2">
		<IconButton className="w-7 h-5" onClick={e => handleChange(e, value - 1)} icon="remove" variant="transparent" disabled={value <= min} />
		<input name={name} 
			type="text" // should be number but can't hide up/down arrows in all browsers 
			{...{min, max}} 
			value={value} 
			className="w-5 h-5 md:w-vw-2 text-base md:text-vw-sm appearance-none bg-transparent text-center" 
			onChange={e => handleChange(e, e.target.value)} 
			autoComplete="off" 
		/>
		<IconButton className="w-7 h-5" onClick={e => handleChange(e, value + 1)} icon="add" variant="transparent" disabled={value >= max} />
	</div>
}