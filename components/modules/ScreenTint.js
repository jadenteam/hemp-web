import cn from 'classnames'

/**
 * Display a full screen covering element with black tint and optional onClick method
 * 
 * @example 
 * <ScreenTint fixed dark onClick={doSomethingMethod} />
 */
export default function ScreenTint({ fixed, dark, onClick, className, children }) {
	return <div onClick={onClick} className={cn('animate-modal-tint-in inset-0 pointer-events-auto', 
		dark ? 'bg-black-trans-dark' : 'bg-black-trans-light',
		fixed ? 'fixed' : 'absolute',
		!!onClick && 'cursor-pointer',
		className
	)}>
		{children}
	</div>
}