import cn from 'classnames'
import Link from 'next/link'

import { Button, IconButton } from './Button'
import { H1 } from './Headings'
import ProductGallery from './ProductGallery'
import Pips from './Pips'

import useCarousel from '../../hooks/useCarousel'
import useBreakpoints from '../../hooks/useBreakpoints'

import { currency } from "../../utility/format";
import ManufacturerLink, { getManufacturerPageURL } from './ManufacturerLink'
import getStores, { getURLByID } from '../../repository/getStores'

/**
 * Display a featured product carousel with related products
 * Related products do not appear on smaller screen widths (mobile)
 */
export default function ProductShowcase({ products, className, hideRelated=false, isHome=false }) {	
	const { isDesktop } = useBreakpoints()

	const { carouselIndex, carouselActive, goTo, goToNext, goToPrev, animateCycle } = useCarousel(products, 'showcase')

	const selectedProduct = products[carouselIndex]
	const category = selectedProduct.categories?.[0]?.id

	return !!products[carouselIndex] && <div className={cn('', className)}>
		<div className="relative text-black bg-grey-200 md:px-4 h-px-624 md:pl-vw-6 md:pr-vw-4 md:h-vw-30 overflow-hidden md:overflow-visible">

			{/* counter */}
			<div className="absolute top-5 left-5 md:top-vw-1 md:left-vw-1">
				<span className="text-2xl md:text-vw-2xl">{carouselIndex + 1}</span>
				<span className="relative -top-1 left-1 md:-top-vw-0.6 md:left-vw-0.1">/{products.length}</span>
			</div>

			{/* navigation arrows */}
			<div className="absolute top-5 right-5 md:top-vw-1 md:right-vw-1 flex">
				<IconButton variant="black" onClick={goToPrev} icon="prev" />
				<IconButton className="ml-2 md:ml-vw-0.5" variant="black" onClick={goToNext} icon="next" />
			</div>

			{/* product images */}
			<div className="absolute top-20 left-5 right-5 mx-auto h-72 md:mx-0 md:-top-vw-2.5 md:left-vw-30 md:w-vw-35 md:h-vw-35">
				{products.map((product, i) => <img key={i} className={animateCycle('absolute top-0 left-0 w-full h-full object-cover', i)} src={product.image.url} alt={product.image.label} />)}
			</div>

			{/* product info */}
			{products.map((product, i) => <Product 
					key={i} 
					counter={i} 
					carouselIndex={carouselIndex}
					product={product}
					category={category}
					isHome={isHome}
				/>
			)}

		</div>

		{/* related products title */}
		{isDesktop && <div className="flex items-center my-1 md:my-vw-1">
			{!hideRelated && <span className="uppercase">Related products</span>}
			<span className="flex-grow text-right text-sm md:text-vw-sm">
				<Link href={getManufacturerPageURL(products[carouselIndex].manufacturer.label)} passHref>
					<a>
						View all <span className="uppercase font-bold">{products[carouselIndex].manufacturer.label}</span> products
					</a>
				</Link>
			</span>
		</div>}

		{/* related products gallery */}
		{isDesktop && !hideRelated && <ProductGallery products={products[carouselIndex].related_products} defaultCategory={category} />}

		{/* navigation pips */}
		{isDesktop && !hideRelated && <Pips active={carouselActive} total={products.length} counter={carouselIndex} goTo={goTo} className="justify-end" dark />}
	</div>
}

/**
 * Single product information slide. To be used inside showcase carousel
 */
export function Product({ carouselIndex, counter, isHome, product, category }) {

	const { manufacturer, name, price, url_key, short_description } = product

	// Prevent double processing of format
	const regular_price = currency.format(price.regular)
	
	const url = getURLByID(category)

	return <div className={cn('absolute top-96 left-5 right-5 inset-y-0 transition-opacity duration-500 md:top-auto md:left-vw-6 md:h-full md:w-vw-20 md:pt-vw-4',
		carouselIndex === counter ? 'opacity-100 z-10' : 'opacity-0 z-5',
	)}>
		<p className="uppercase"><ManufacturerLink {...manufacturer} /></p>
		<p className="mt-1 mb-1 text-lg uppercase font-black md:text-vw-lg md:mt-vw-0.5 md:mb-vw-1">{name}</p>
		<p className="line-clamp-3">{short_description}</p>
		{price.isDiscounted && <p className="text-sm text-grey-500 line-through md:text-vw-sm">{regular_price}</p>}
		<p className="text-lg md:text-vw-lg">
			{price.hasFromPrice && <span className="mr-1 md:mr-vw-0.3">from</span>}
			<span>{price.isDiscounted ? currency.format(price.discounted) : regular_price}</span>
		</p>
		<Button
			target={isHome ? "_blank" : null}
			href={`${url}/shop/product/${url_key}`} 
			className="w-full text-center mt-2 md:mt-vw-2 absolute bottom-4 md:bottom-vw-4">
				Learn more
		</Button>
	</div>
}