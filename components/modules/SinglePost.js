import { useEffect, useState } from 'react'

import Layout from '../../components/Layout'
import { H3 } from '../../components/modules/Headings'
import PostDateline from '../../components/modules/PostDateline'
import Subscribe from '../../components/modules/Subscribe'
import PostSidebar from '../../components/modules/PostSidebar'
import { ResponsiveImage } from '../../components/modules/Image'

import WPShortcodes from "../../utility/shortCodeProcessor"
import getProduct from "../../repository/getProduct"
import ManufacturerLink from '../../components/modules/ManufacturerLink'

export const SinglePost = ({ data: { tags, categories, date, title, excerpt, image, body, subscribe, slug, seo, error = null }, menuFeaturedProducts }) => {
    if (error) {

        return (
            <Layout {...{ seo }} title={title} className="flex flex-col-reverse md:flex-row" menuFeaturedProducts={menuFeaturedProducts}>
                <aside className="md:relative md:w-vw-15"></aside>
                <article className="flex-1 md:ml-vw-4 md:mr-vw-15">
                    <H3>Whoops</H3>
                    <p className="mt-1 text-base md:text-vw-base md:mt-vw-1">Looks like we're having trouble connecting to our server. Please try again later!</p>
                    <p className="mt-1 text-error text-xs md:text-vw-xs md:mt-vw-1">{error}</p>
                </article>
            </Layout>
        )

    } else {

        // parsebody to find shortcode
        const [pBody, setPBody] = useState(body);

        // if shortcode get product data and replace with component
        useEffect(async () => {

            // Define function to replace instances with loader initially
            let inline_product = async (attr) => {
                return `<img style="width: 2rem;" src="/images/loading-dark.svg" alt="Loading..." />`
            };

            // Parse body and replace instances with inline_product function, return plain body if error in processing
            try {
                processedBody = await WPShortcodes(pBody, { inline_product });
                setPBody(processedBody.markup);
            } catch (err) {
                return pBody;
            }

            // Re-define function to replace instances with product tab
            inline_product = async (attr) => {

                // Match slug and get value
                let slug = attr.filter(obj => {
                    return obj.name === "slug";
                })[0].value;

                // Get product data using matched slug
                try {
                    const product = await getProduct(slug);
                    const manufacturerLink = <ManufacturerLink {...product.manufacturer} />

                    return `
						<div className="bg-grey-100 p-vw-3 py-1.5">
							<div className="flex items-center">
								<div className="w-1/2">
									<div className="uppercase text-vw-sm">
										<span className="">${manufacturerLink}</span>
									</div>
									<div className="mb-vw-2 text-vw-lg font-bold uppercase">${product.name}</div>
									<div className="mb-vw-1 text-vw-base line-clamp-3">${product.short_description}</div>
									<div className="mb-vw-2"><span>${product.price.regular} AUD</span></div>
									<a href="/shop/product/${product.url_key}">	
										<button className="nline-block font-bold uppercase w-full text-center text-sm py-4 border md:text-vw-sm bg-black text-white">Learn More</button>
									</a>
								</div>
								<div className="w-1/2">
									<img src="${product.image.url}" alt="${product.name}" className="block object-contain w-full">
								</div>
							</div>
						</div>
					`
                } catch (err) {
                    return `<div className="bg-grey-100 p-vw-1">Failed to get product</div>`
                }

            };

            // Re-parse body for product data
            let processedBody;
            try {
                processedBody = await WPShortcodes(pBody, { inline_product });
                setPBody(processedBody.markup);
            } catch (err) {
                console.error(err);
            }

        }, []);

        return <Layout {...{ seo }} title={title} className="flex flex-col-reverse md:flex-row" menuFeaturedProducts={menuFeaturedProducts}>
            <aside className="md:relative md:w-vw-15">
                <PostSidebar {...{ slug, tags }} />
            </aside>
            <article className="flex-1 md:ml-vw-4 md:mr-vw-15">
                <PostDateline className="mt-3 md:mt-vw-3" categories={categories} date={date} />
                <H3>{title}</H3>
                <p className="mt-1 font-bold text-lg md:text-vw-lg md:mt-vw-1">{excerpt}</p>
                <ResponsiveImage className="w-full mt-2 md:mt-vw-2" image={image} alt={title} objectFit="contain" />

                <div id="prestyled" className="my-3 md:my-vw-3" dangerouslySetInnerHTML={{ __html: pBody }} />

                {/* TODO: style small version */}
                <Subscribe data={subscribe} />
            </article>
        </Layout>

    }
}