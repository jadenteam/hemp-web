import { useEffect, useState } from 'react'
import cn from 'classnames'
import { Tween } from 'react-gsap'
import { ProductJsonLd } from 'next-seo'
import parse from 'html-react-parser'

import Layout from '../../components/Layout'
import Header from '../../components/Header'
import ProductInfo from '../../components/modules/ProductInfo'
// import ProductReviews from '../../../components/modules/ProductReviews'
import ProductImages from '../../components/modules/ProductImages'

import useBreakpoints from '../../hooks/useBreakpoints'
import { H3 } from '../../components/modules/Headings'
import ProductGallery from '../../components/modules/ProductGallery'
import PostItem from '../../components/modules/PostItem'
import { getProductURLByCategoryID } from '../../repository/getStores'
import { useBanner } from '../../hooks/useBanner'
import { useDataLayer } from '../../hooks/useDataLayer'
import { stringCurrency } from '../../utility/format'

import { getSalableQuantity } from '../../repository/getSalableQuantity';

export const SingleProduct = ({ product: _product, related_articles, menuFeaturedProducts, banner, initialisedOption }) => {
    const [product, setProduct] = useState(_product)

    useEffect(async () => {
        const response = await getSalableQuantity(product.sku)
        setProduct(prev => ({
            ...prev,
            salableQuantity: response
        }))
    }, [])

    const { isMobile } = useBreakpoints()
    const { name, media_gallery, short_description, manufacturer, image, url_key, price, price_range, description, sku, audience } = product

    const [bannerActive, closeBanner] = useBanner()
    const [changeDataLayer] = useDataLayer({
        dataLayer: {
            event: "view_item",
            ecommerce: {
                items: [{
                    item_id: sku,
                    item_name: name,
                    item_brand: manufacturer.label, // - gender category (men/women/unisex)
                    affiliation: process.env.SITE_KEY,
                    price: stringCurrency(price.regular)
                }]
            }
        }
    })

    const formatPrice = (price) => price.toFixed(2)

    const options = {
        replace: ({ children }) => {
            const img = children?.filter(child => child.name === 'img')[0]
            if (img) {
                const { alt, src } = img.attribs
                return <img src={src} alt={alt} />
            }
        }
    }

    return (
        <Layout
            noheader
            title={`${product.name} | Hemp Forever`}
            menuFeaturedProducts={menuFeaturedProducts}
            headContents={
                <>
                    <meta property="og:title" content={name} />
                    <meta property="og:description" content={`${product.short_description && product.short_description.slice(0, 63)}`} />
                    <meta property="description" content={`${product.short_description && product.short_description.slice(0, 63)}`} />
                    <meta property="og:url" content={getProductURLByCategoryID(process.env.SHOP_CATEGORY_ID, url_key)} />
                    <meta property="og:type" content="website" />
                    <meta property="og:site_name" content="Hemp Forever" />
                    <meta property="og:image" content={image} />
                    <meta property="og:image:width" content="1200" />
                    <meta property="og:image:height" content="630" />
                </>
            }
        >
            <div className={cn(
                'bg-grey-100',
                {
                    'md:h-vw-60': bannerActive
                })}
            >
                <Header
                    menuFeaturedProducts={menuFeaturedProducts}
                    banner={banner}
                    bannerActive={bannerActive}
                    closeBanner={closeBanner}
                />

                <ProductJsonLd
                    productName={name}
                    images={media_gallery.map(m => m.url)}
                    description={short_description}
                    brand="ACME"
                    manufacturerName={manufacturer.label}
                    price={formatPrice(price.discounted || price.regular)}
                    lowPrice={formatPrice(price_range.minimum_price.final_price.value)}
                    highPrice={formatPrice(price_range.maximum_price.final_price.value)}
                    priceCurrency='AUD'
                />

                <div className="flex flex-col-reverse md:flex-row p-3 pt-8 md:pt-vw-2 md:px-vw-7">

                    {/* TODO: Disabled doesn't work -- resize big to small broken display:none as workaround */}
                    <div className={isMobile ? "hidden" : ""}>
                        <Tween
                            progress={isMobile && 0} // disable animation on mobile
                            to={{
                                y: '0',
                                scrollTrigger: {
                                    trigger: '#product-info',
                                    start: '-100 top',
                                    endTrigger: '#scroll-end',
                                    end: 'top bottom',
                                    scrub: true,
                                    pin: true
                                }
                            }}
                        >
                            <div id="product-info" className={cn('md:w-vw-30 md:absolute z-10')}>
                                <ProductInfo {...{ product }} initialisedOption={initialisedOption} className="pt-6 md:pt-0" />
                            </div>
                        </Tween>
                    </div>

                    {isMobile &&
                        <div id="product-info" className={cn('md:w-vw-30 md:absolute z-10')}>
                            <ProductInfo {...{ product }} initialisedOption={initialisedOption} className="pt-6 md:pt-0" />
                        </div>
                    }


                    <div className="pl-0 md:pl-vw-40">
                        <div className="h-vh-40 md:h-vw-40 w-full md:w-vw-40">
                            <ProductImages images={product.media_gallery} />
                        </div>
                    </div>
                </div>
            </div>

            <div className="p-3 pt-8 md:pt-vw-2 md:px-vw-7">
                <div id="prestyle-product" className="product__details mt-20 md:mt-vw-6 md:pl-vw-35">
                    {parse(description, options)}
                </div>
                <div id="scroll-end" />

                {/* <section id="reviews" className="border p-10 md:p-vw-3 my-40 md:my-vw-12">
					<ProductReviews sku={product.sku} reviews={product.reviews} />
				</section> */}

                {related_articles && !!related_articles.length &&
                    <section className="my-40 md:my-vw-12">{/* TODO: when reactivating the reviews section above, also remove this line */}
                        {/* <section> */}
                        <H3>Related posts from the blog</H3>

                        <div className="flex flex-col flex-wrap mt-6 md:flex-row md:pb-vw-4 md:mt-vw-3 md:-mx-vw-1">
                            {related_articles.map((post, i) => <PostItem key={i} linkToHome={true} {...post} className="mb-10 md:mb-0 md:w-1/2 md:px-vw-1" />)}
                        </div>
                    </section>
                }

                {product.related_products && !!product.related_products.length &&
                    <section>
                        <H3>Related Products</H3>
                        <p className="mb-8 md:mb-vw-2">Here's a few more items that might interest you</p>
                        <ProductGallery products={product.related_products} />
                    </section>
                }
            </div>
        </Layout>
    )
}