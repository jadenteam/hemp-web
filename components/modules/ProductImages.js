import cn from 'classnames'
import { useCallback, useEffect, useState } from 'react'

import { IconButton } from './Button'

import usePreventScroll from '../../hooks/usePreventScroll'
import useCarousel from '../../hooks/useCarousel'
import useBreakpoints from '../../hooks/useBreakpoints'
import Image from "next/image";


/**
 * Display single product images and popup slideshow
 */
export default function ProductImages({ images }) {
	const [openSlideshow, setOpenSlideshow] = useState(false)
	const { isMobile, isDesktop } = useBreakpoints()
	usePreventScroll(openSlideshow)

	function closeSlideshow() {
		setOpenSlideshow(false)
	}

	const { carouselIndex, carouselActive, goTo, goToNext, goToPrev, animateSwipe, isStart, isEnd  } = useCarousel(images, 'showcase', false)

	return !!images.length && <article className="relative w-full h-full">
		<div className="flex w-full h-full relative top-17">

			<img className="absolute w-5 right-0 md:left-0 md:w-vw-1.5" src="/images/zoom.svg" alt="Zoom" />

			{
				isDesktop ? <div>
					<IconButton disabled={isStart} variant="white" large icon="prev" onClick={goToPrev} className="absolute top-1/2 left-0 z-20" />
					<IconButton disabled={isEnd} variant="white" large icon="next" onClick={goToNext} className="absolute top-1/2 right-0 z-20" />

					<ul className="w-vw-80">
						{images.map((img, i) => <img key={i} onClick={() => setOpenSlideshow(true)} src={img.src} alt={img.label} className={animateSwipe('w-vw-40 absolute inset-0 h-full object-contain overflow-hidden cursor-pointer', i)} />
					)}</ul>
				</div> 
				: <div>
					<Image src={images[0].src} alt={images[0].label} layout="fill" objectFit="contain" />
				</div>
				
			}

		</div>
		<Slideshow {...{images, isStart, isEnd}} active={openSlideshow} close={closeSlideshow} />
	</article>
}

export function Slideshow({ images, active, close, isStart, isEnd }) {
	const { carouselIndex, goToNext, goToPrev} = useCarousel(images, 'hero')

	// close on ESC key press
	const closeOnEsc = useCallback((event) => event.keyCode === 27 && close())
	useEffect(() => {
		document.addEventListener('keydown', closeOnEsc, false)
	
		return () => {
			document.removeEventListener('keydown', closeOnEsc, false)
		}
	}, [])

	return active && <div className="fixed inset-0 bg-white z-50 p-12 md:p-vw-3">
		<IconButton variant="grey" large icon="close" onClick={close} className="absolute top-8 right-8 z-10 md:top-vw-2 md:right-vw-2" />
		<IconButton disabled={isEnd} variant="grey" large icon="next" onClick={goToNext} className="absolute top-1/2 right-8 z-10 md:right-vw-2" />
		<IconButton disabled={isStart} variant="grey" large icon="prev" onClick={goToPrev} className="absolute top-1/2 left-8 z-10 md:left-vw-2" />

		<ul className="">
			{images.map((img, i) => <img key={i} src={img.url} alt={img.label} className={cn(
				'absolute inset-0 w-full h-full object-contain transition-opacity duration-400 overflow-hidden', 
				i === carouselIndex ? 'opacity-100' : 'opacity-0'
			)} />)}
		</ul>
	</div>
}