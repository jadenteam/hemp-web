import Icon from '../../components/modules/Icons'
import { useState, useRef } from 'react'
import useOutsideClickTrigger from '../../hooks/useOutsideClickTrigger'
import cn from 'classnames'

const socials = {
    facebook: "https://www.facebook.com/sharer.php?u=",
    twitter: "https://twitter.com/intent/tweet?&url=",
    email: "mailto:?subject=Hemp Forever&body=Hey, I found this and thought you might enjoy seeing it too! "
}

export default function socialShare({ productUrlKey, shareOptions }) {

    const [active, setActive] = useState(false)

    const dropdownRef = useRef(null)
    useOutsideClickTrigger(dropdownRef, () => setActive(false));

    const shareUrl = `${process.env.SITE_URL}shop/product/${productUrlKey}`

    return (

        <div className="w-full" ref={dropdownRef}>
            <div onClick={() => setActive(!active)} className="float-right flex cursor-pointer">
                <img className="mr-2 md:w-vw-1 md:mr-vw-0.5" src="/images/share.svg" alt="" />
                <span className="text-vw-sm">SHARE</span>
            </div>
            <table className={cn(
                'shadow md:p-vw-1 absolute -right-vw-9 bg-grey-200',
                !active && 'hidden'
            )}>
                <tbody>
                    {buildOptions(shareUrl, shareOptions)}
                </tbody>
            </table>
        </div>

    )

}

function buildOptions(shareUrl, shareOptions) {

    let options = []

    for (let key in socials) {

        shareOptions.includes(key) && options.push(
            <tr className="md:p-vw-0.5 flex cursor-pointer" key={key} onClick={() => { window.open(socials[key] + shareUrl, '_blank', 'noopener,noreferrer') }}>
                <td><Icon className="w-vw-7 md:w-vw-1.5 md:mr-vw-1" type={key} /></td>
                <td className="capitalize text-vw-sm">{key}</td>
            </tr>
        )

    }

    return options

}