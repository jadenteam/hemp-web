import Image from 'next/image'

/**
 * Display a responsive image
 */
 export function ResponsiveImage({ className, image, alt, layout='responsive', objectFit='cover' }) {
	// wrapping div required because next/image doesn't support styling its generated wrappers (yet)

	return image ? <div className={className}> 
		<Image 
			src={image.src} 
			width={layout !== 'fill' && image.width}
			height={layout !== 'fill'  && image.height}
			alt={alt} 
			layout={layout}
			objectFit={objectFit}
			unoptimized={true} // memory issues with live server if optimzation is turned on
		/>
		</div>
	: false
}

/**
 * Display a placeholder image
 */
export function PlaceholderImage({ className, alt }) {
	return <Image src={`/images/placeholder.png`} width={600} height={345} alt={alt} className={className} />
}