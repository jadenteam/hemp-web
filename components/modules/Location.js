import { H1 } from '../../components/modules/Headings'
import { ResponsiveImage } from '../../components/modules/Image'
import { IconButton } from '../../components/modules/Button'
import useBreakpoints from '../../hooks/useBreakpoints'

export default function Location({ title, description, link, image, background }) {
	const { isDesktop } = useBreakpoints()
	const bgImage = isDesktop && background ? {backgroundImage: 'url(' + background.src + ')'} : {}

	return (
		<section className="md:bg-left-bottom bg-no-repeat" style={bgImage}>
			<H1 className="mb-2 md:mb-vw-2">{title}</H1>
			<div className="md:flex w-full">
				<div dangerouslySetInnerHTML={{ __html: description }} className="mb-10 md:mb-0 md:w-2/5 md:pr-vw-4" />
				<ResponsiveImage image={image} alt="" className="md:w-3/5" />
			</div>
			<div className="pt-vw-6 flex items-end">
				<IconButton variant="black" href={link} target="_blank" icon="next">Get directions</IconButton>
			</div>
		</section>
	)
}