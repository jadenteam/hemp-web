import cn from 'classnames'
import parse, { attributesToProps } from 'html-react-parser'

import { H1, H2 } from './Headings'
import { IconButton } from './Button'
import Modal from './Modal'
import useModal from '../../hooks/useModal'
import { ResponsiveImage } from './Image'
import useBreakpoints from '../../hooks/useBreakpoints'

export default function Mission({ title, text, tagline, image, video, className }) {
	const { setModal } = useModal()
	const { isMobile } = useBreakpoints()

	/** Open the video modal */
	function openModal() {
		setModal({
			content: <div>{parse(video, {
				replace: domNode => {
					// on mobile, remove dimensions from iframe 
					if (isMobile && domNode.attribs && domNode.name === 'iframe') {
						const props = attributesToProps(domNode.attribs)

						// remove height/width 
						delete props.width
						delete props.height

						// recreate iframe 
						return <iframe {...props} />
					}
				}
			})}</div>,
			style: 'popup'
		})
	}

	return <>
		<article className={cn('relative flex-grow text-right', className)}>
			<section className="text-left md:absolute md:mt-vw-5 md:w-2/3 z-20">
				<p className="text-grey-500 md:w-1/2 md:pr-vw-2">{tagline}</p>
				<H1 className="mb-4 md:mb-0 z-10">{title}</H1>
				<div dangerouslySetInnerHTML={{ __html: text }} className="md:w-1/2 md:pr-vw-2" />
				<br />
				{/* <div className="flex items-center" onClick={openModal}>
					<IconButton large variant="clear" className="mr-4 cursor-pointer" icon="play"/>
					<span className="inline-block">Play video</span>
				</div> */}
			</section>
			<div className="relative z-0">
				<ResponsiveImage image={image} alt="" className="inline-block md:w-2/3" />
			</div>
		</article>
	</>
}