import { useState } from 'react'
import cn from 'classnames'

import { LiveButton } from './Button'
import getSearch from '../../repository/getSearch'

export default function SearchForm({ setResults }) {
	const [value, setValue] = useState('')
	const [loading, setLoading] = useState(false)
	const [message, setMessage] = useState(null)

	async function handleSubmit() {
		try {
			// add mailchimp subscriber and attach a discount code
			const response = await getSearch(value)

			console.log('search response', response)

			// throw error if no products found
			if(response.items && response.items.length === 0) {
				throw 'No products matched your search terms'
			}

			// set results to state 
			setResults(response.items)
		} catch (err) {
			setMessage({
				type: "error",
				text: err
			});
			setLoading(false)
			setResults([])
			return
		} finally {
			setLoading(false)
		}
	}

	function validate(e) {
		e.preventDefault()
		setLoading(true)
		setMessage(null)
		handleSubmit()
	}

	return (
		<form onSubmit={validate}>
			<div className="flex mb-1 md:mb-vw-1 border-b md:border-b-vw-1 border-grey-900 dark:border-grey-600">
				<input className="flex-grow w-full py-1 text-sm md:py-vw-1 md:text-vw-sm dark:bg-black"
					type="text"
					placeholder="enter search terms..."
					value={value}
					onChange={e => setValue(e.target.value)}
					required
				/>
				<LiveButton {...{ loading }} size="small" variant="white" type="submit">
					Search
				</LiveButton>
			</div>
			<p className={cn(
				'text-sm md:text-vw-sm mt-vw-1.5',
				message ? message.type === "success" && 'text-primary' : "",
				message ? message.type === "error" && 'text-error' : ""
			)}>
				{message && message.text}
			</p>
		</form>
	);
}