import React from 'react'
import cn from 'classnames'
import { H2, H6 } from './Headings'
import FormEmail from './FormEmail'

/** Newletter subscribe */
export default function Subscribe({ variant, className, data = {}, noBg = false }) {

	const { title, subheading, description } = data;

	return <section className={cn('md:flex space-x-vw-5 w-full bg-no-repeat bg-center bg-cover dark:bg-black py-20 md:py-vw-4',
		noBg && 'bg-grey-100',
		variant === 'home' ? 'px-5 md:px-vw-15' : 'px-5 border-t-8 border-primary md:px-vw-6 md:border-t-vw-10',
		className
	)}
		style={!noBg ? { backgroundImage: 'url(/images/hemp-bg.svg)' } : {}}>
		<div className="md:w-2/3">
			<H6 className="mb-1 md:mb-vw-1">Newsletter</H6>
			<H2>{title}</H2>
			<p className="mb-4 md:mb-vw-2">{subheading}</p>
			<FormEmail variant="bordered" description={description} GAClass="form__subscribe" />
		</div>
		<div className="hidden md:block md:w-1/3 pt-5 pb-5">
			<img className="w-full" src="/images/newsletter-home.svg" alt="" />
		</div>
	</section>
}

/** Mini subscribe form */
export function SubscribeMini({ className }) {
	return <section className={cn('w-full py-8 md:py-vw-4', className)}>
		<div className="text-center p-vw-3">
			<img className="inline-block mb-3 md:mb-vw-1" src="/images/newsletter.svg" alt="" />
			<div className="uppercase font-bold text-sm mb-1 md:mb-vw-1">Newsletter</div>
			<p className="font-normal font-serif text-vw-2xl mb-8 md:mb-vw-2">Sign up for the latest hemp products, offers &amp; news</p>
			<FormEmail GAClass="form__subscribe" variant="basic" />
		</div>
	</section>
}