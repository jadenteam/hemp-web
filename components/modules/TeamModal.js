import React from 'react'

import { ResponsiveImage } from '../../components/modules/Image'

export const TeamModal = ({ name, role, image, bio }) => {
    return <div className="w-screen h-screen md:flex">
        <div className="md:w-1/2 w-full flex items-center flex-grow-0">
            <ResponsiveImage image={image} alt={name} className="w-full" />
        </div>
        <div className="md:w-1/2 w-full p-5 flex items-center flex-grow-0">
            <div>
                <h2 className="font-serif font-bold tracking-tight text-4xl md:text-vw-5xl mb-4 md:mb-0">{name}</h2>
                <p className="text-sm md:text-vw-sm opacity-50">{role}</p>
                <p className="text-sm md:text-vw-sm pt-2">{bio}</p>
            </div>
        </div>
    </div>
} 