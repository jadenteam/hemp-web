
import React, { useEffect, useState, useRef } from 'react'
import cn from 'classnames'

import { H3 } from '../components/modules/Headings'
import { Button } from '../components/modules/Button'

import useOutsideClickTrigger from '../hooks/useOutsideClickTrigger'
import getSocial from '../repository/getSocial';
import Icon from './modules/Icons'
import { currency } from '../utility/format'
import ManufacturerLink from './modules/ManufacturerLink'

import menu from '../repository/getShopMenu'

/**
 * Desktop-only header fixed to top of screen. Active logic is handled by the parent
 */
export default function HeaderFixed({ active, stores }) {
	const [dropdownOpen, setDropdownOpen] = useState(false)

	useEffect(() => {
		setDropdownOpen(false)
	}, [active])

	// TODO: Fix header peeking out when hidden on wide screens, then remove false conditional here
	return <header className={cn('hidden md:flex fixed z-50 transition-positions duration-500 bg-black text-white w-full content-center px-vw-7 py-vw-0.25',
		active ? 'top-0' : '-top-vw-10'
	)}>
		<a className="flex items-center" href="/">
			<img className="mr-1 h-vw-3" src="/images/logo-long-light.svg" alt="Hemp Forever" />
		</a>
		<nav className="flex items-center">
			<ul className="flex px-vw-1">
				<li className="relative p-vw-1">
					<Icon className="inline-block mr-vw-0.5 h-vw-1" type="shopbag" light />
					<span className="text-vw-sm font-bold cursor-pointer" onClick={() => setDropdownOpen(prevState => !prevState)}>Shop</span>
					{dropdownOpen && active && <Dropdown setOpen={setDropdownOpen} {...{ stores }} />}
				</li>
				{!!menu[process.env.SITE_KEY] && menu[process.env.SITE_KEY].map((page, i) => <NavItem key={i} {...page} active={i === 1} />)}
			</ul>
		</nav>
		<div className="flex-grow" />
		<div className="flex justify-end space-x-4 items-center w-vw-10">
			{getSocial.map(obj => <a key={obj.label} target="_blank" href={obj.link}>
				<Icon light className="w-vw-2" type={obj.icon} /></a>)}
		</div>
	</header>
}

/**
 * Single navigation item in main menu 
 */
export function NavItem({ link, label }) {
	return <li className="relative p-vw-1">
		<a href={link} className="inline-block text-vw-sm font-bold">
			{label}
		</a>
	</li>
}

/**
 * Dropdown mega menu to display stores and featured products
 */
export function Dropdown({ setOpen, stores }) {
	const [activeStore, setActiveStore] = useState(0)
	const dropdownRef = useRef(null)
	useOutsideClickTrigger(dropdownRef, () => setOpen(false));

	function handleFocus(id) {
		setActiveStore(id)
	}

	return <div ref={dropdownRef} className="absolute top-vw-3 -left-vw-4 w-vw-70 bg-white shadow-lg text-black flex px-vw-5 py-vw-3">
		<ul className="border-grey-300 border-r-vw-1 pr-vw-4">
			{stores.map((store, i) => <DropdownNavItem key={i} index={i} {...store} callback={handleFocus} />)}
		</ul>
		<FeaturedProducts current={activeStore} products={stores.map(store => ({ store_link: store.link, ...store.featured }))} />
	</div>
}

/**
 * Single navigation item in first-level dropdown menu 
 */
export function DropdownNavItem({ index, link, label, description, callback }) {
	return <li className="pb-vw-2 last:pb-0">
		<a href={link} className="inline-block"
			onFocus={() => callback(index)}
			onMouseEnter={() => callback(index)}
		>
			<span className="block text-vw-xl font-black leading-none whitespace-nowrap">{label}</span>
			<span className="block text-vw-sm whitespace-nowrap">{description}</span>
		</a>
	</li>
}

/**
 * Product data and link shown in dropdown menu 
 */
export function FeaturedProducts({ current, products }) {
	return <div className="pl-vw-4 flex flex-col">
		<H3 className="mr-vw-10 whitespace-nowrap">Featured Product</H3>
		<div className="relative flex-grow">
			{products.map((product, i) => {
				let url = `${product.store_link}/shop/product/${product.url_key}`
				return (<React.Fragment key={i}>
					<div className={cn('absolute inset-0 transition-opacity duration-200',
						current === i ? 'opacity-100 z-10' : 'opacity-0 z-0'
					)}>
						<p className="w-1/2 mt-vw-2"><ManufacturerLink {...product.manufacturer} /></p>
						<p className="w-1/2 mt-vw-0.5 mb-vw-1 uppercase font-black text-vw-lg">{product.name}</p>
						{product.price?.isDiscounted && <p className="text-vw-sm text-grey-500 line-through">{currency.format(product.price?.regular)}</p>}
						<p className="text-vw-lg">{product.price?.isDiscounted ? currency.format(product.price?.discounted) : currency.format(product.price?.regular)}</p>
						<a href={url} target="_blank">
							<Button className="absolute bottom-0">View</Button>
						</a>
					</div>
					<img className={cn('absolute bottom-0 right-0 h-full', current === i ? 'opacity-100' : 'opacity-0')} src={product.image?.src} alt={product.image?.title} />
				</React.Fragment>)
			})}
		</div>
	</div>
}