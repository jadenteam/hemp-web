import FormEmail from './modules/FormEmail'
import Link from 'next/link'
import getSocial from '../repository/getSocial'

export default function Footer() {
	const shopMenu = [
		{ link: 'https://bodycare.hempforever.store/', label: 'Hemp Bodycare' },
		{ link: 'https://food.hempforever.store/', label: 'Hemp Food' },
		{ link: 'https://lifestyle.hempforever.store/', label: 'Hemp Lifestyle' },
		{ link: 'https://clothing.hempforever.store/', label: 'Hemp Clothing' },
		// { link: 'https://growhouse.hempforever.store/', label: 'Extraction & Growing' },
		// { link: 'https://greenlight.hempforever.store/', label: 'Green Light District (+18)' },
	]

	const infoMenu = [
		{ link: '/shipping', label: 'Shipping & Returns' },
		{ link: '/faqs', label: 'FAQs' },
		{ link: '/contact', label: 'Contact' },
		{ link: '/about', label: 'About Us' },
	]

	const footerMenu = [
		{ link: '/terms', label: 'Terms of service' },
		{ link: '/privacy', label: 'Privacy policy' },
	]

	return <footer className="px-5 pt-16 pb-6 text-sm md:px-vw-7 md:py-vw-8 md:text-vw-xs">
		<div className="block md:flex">
			<div className="flex flex-grow flex-col md:flex-row">
				<ul className="md:mb-0 mb-10">
					<MenuTitle text="Our shops" />
					{shopMenu.map((item, i) => <MenuItem key={i} {...item} />)}
				</ul>
				<ul className="md:mb-0 mb-10">
					<MenuTitle text="About" />
					{infoMenu.map((item, i) => <MenuItem key={i} {...item} />)}
				</ul>
				<ul className="md:mb-0 mb-10">
					<MenuTitle text="Get social" />
					{getSocial.map((item, i) => <MenuItem key={i} {...item} />)}
				</ul>
			</div>
			<div className="md:w-2/5 py-10 md:py-0 md:mt-0 md:ml-20">
				<Newsletter />
			</div>
		</div>
		<nav className="md:mt-vw-1">
			<ul className="md:flex items-center">
				<li className="py-2 md:py-0 md:mr-vw-5">All rights reserved {(new Date().getFullYear())} &copy; Hemp Forever</li>
				{footerMenu.map((item, i) => <MenuItem key={i} {...item} />)}
				<li className="mt-10 md:mt-0 flex justify-end flex-grow">
					<img className="w-full md:w-vw-35" src="/images/footer-logo.svg" alt="Revolution for a greener future" />
				</li>
			</ul>
			<div>
			</div>
		</nav>
		<div className="mt-4 md:mt-vw-2 pt-4 md:pt-vw-1.5 border-t border-grey-300">
			<p className="leading-snug">We acknowledge the traditional custodians of this land throughout Australia and recognise their
				continuing connection to land, waters, communities and culture.</p>
			<p className="leading-snug">We pay our respect to Elders past and present and to all Aboriginal and Torress Straight Islanders.</p>
		</div>
	</footer>
}

export function MenuTitle({ text }) {
	return <li className="uppercase font-bold mb-2 md:mb-vw-1">{text}</li>
}

export function MenuItem({ link, label, external = false }) {
	return link ?
		<li className="py-2 mr-5 md:mr-vw-5 md:py-vw-0.1">
			<Link href={link}>
				<a target={external ? "_blank" : "_self"}>{label}</a>
			</Link>
		</li>
		:
		<li>{label}</li>
}

export function Newsletter() {
	return <>
		<h3 className="uppercase font-bold mb-5 md:mb-vw-3">Sign up to the newsletter &amp; get 10% off your first purchase</h3>
		<FormEmail variant="basic" />
	</>
}